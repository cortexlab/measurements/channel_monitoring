/*
 * Copyright 2024 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, channel_monitoring, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_channel_monitoring_strobe_random_seq = R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_strobe_random_seq_0 =
    R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_strobe_random_seq_1 =
    R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_make = R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_set_modulo = R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_modulo = R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_set_length = R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_set_period = R"doc()doc";


static const char* __doc_gr_channel_monitoring_strobe_random_seq_period = R"doc()doc";

/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, channel_monitoring, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */



 static const char *__doc_gr_channel_monitoring_header_packet_ofdm_id = R"doc()doc";


 static const char *__doc_gr_channel_monitoring_header_packet_ofdm_id_header_packet_ofdm_id = R"doc()doc";


 static const char *__doc_gr_channel_monitoring_header_packet_ofdm_id_make = R"doc()doc";

  

/*
 * Copyright 2020 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#include <pybind11/pybind11.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

namespace py = pybind11;

// Headers for binding functions
/**************************************/
// The following comment block is used for
// gr_modtool to insert function prototypes
// Please do not delete
/**************************************/
// BINDING_FUNCTION_PROTOTYPES(
// void bind_header_packet_ofdm_id(py::module& m);
void bind_packet_header_ofdm_id(py::module& m);
    void bind_freq_offset_tagger(py::module& m);
    // void bind_ampl_stats(py::module& m);
    void bind_strobe_random_seq(py::module& m);
    void bind_divide_random_seq(py::module& m);
    void bind_insert_burst_time(py::module& m);
// ) END BINDING_FUNCTION_PROTOTYPES


// We need this hack because import_array() returns NULL
// for newer Python versions.
// This function is also necessary because it ensures access to the C API
// and removes a warning.
void* init_numpy()
{
    import_array();
    return NULL;
}

PYBIND11_MODULE(channel_monitoring_python, m)
{
    // Initialize the numpy C API
    // (otherwise we will see segmentation faults)
    init_numpy();

    // Allow access to base block methods
    py::module::import("gnuradio.gr");
    py::module::import("gnuradio.digital");

    /**************************************/
    // The following comment block is used for
    // gr_modtool to insert binding function calls
    // Please do not delete
    /**************************************/
    // BINDING_FUNCTION_CALLS(
    // bind_header_packet_ofdm_id(m);
    bind_packet_header_ofdm_id(m);
    bind_freq_offset_tagger(m);
    // bind_ampl_stats(m);
    bind_strobe_random_seq(m);
    bind_divide_random_seq(m);
    bind_insert_burst_time(m);
    // ) END BINDING_FUNCTION_CALLS
}
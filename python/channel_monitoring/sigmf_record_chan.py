#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2023 Cyrille Morin.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import datetime
import time
import threading
# import atexit
import numpy
from gnuradio import gr
import pmt
import sigmf
from sigmf import SigMFFile


class sigmf_record_chan(gr.sync_block):
    """
    Record metadata in SigMF file for channel state information
    """

    def __init__(
        self,
        filename="./channel",
        samp_rate=300000,
        fft_len=64,
        center_freq=200000,
        occupied_carriers=[-1, 2, 44],
        time_tag_key="rx_time",
        frac_offset_tag_key="frac_freq_offset",
        carrier_offset_tag_key="ofdm_sync_carr_offset",
        rx_node=1,
        tx_node=2,
        author_email="insert.email@address.here",
        file_description="Channel recording info",
    ):
        gr.sync_block.__init__(
            self,
            name="Record Channel SigMF",
            in_sig=[
                numpy.complex64,
            ],
            out_sig=None,
        )

        self.time_tag_key = time_tag_key
        self.frac_offset_tag_key = frac_offset_tag_key
        self.carrier_offset_tag_key = carrier_offset_tag_key
        self.filename = filename
        self.samp_rate = samp_rate
        self.fft_len = fft_len
        self.center_freq = center_freq
        self.occupied_carriers = occupied_carriers
        self.rx_node = rx_node
        self.tx_node = tx_node
        self.author_email = author_email
        self.file_description = file_description

        # self.freq_change_time = numpy.inf
        # self.next_freq = 0

        self.freq_change_sched = []

        self.my_log = gr.logger(self.alias())

        self.meta = SigMFFile(
            global_info={
                SigMFFile.VERSION_KEY: sigmf.__version__,
                SigMFFile.DATATYPE_KEY: "cf32_le",
                SigMFFile.AUTHOR_KEY: self.author_email,
                SigMFFile.DESCRIPTION_KEY: self.file_description,
                SigMFFile.SAMPLE_RATE_KEY: self.samp_rate,
                "ofdm:fft_len": fft_len,
                "ofdm:occupied_carriers": self.occupied_carriers,
                "cxlb:rx_node": self.rx_node,
            },
        )

        self.meta.tofile(filename)
        # atexit.register(self.meta.tofile, filename)

    def timestamp_to_date(self, timestamp: float) -> str:
        """
        Converts a float timestamp to a readable date string

        Parameters
        ----------
        timestamp : float
            Timestamp (float value of second)

        Returns
        -------
        str
           Readable string with date and time up to micro-second
        """

        return datetime.datetime.fromtimestamp(timestamp).strftime(
            "%Y-%m-%dT%H:%M:%S.%f"
        )
    
    def record_current_data(self)-> None:
        """
        A call to this function triggers the recording of the current metadata to file
        """
        def rec():
            # rec_start_time = time.perf_counter()
            # self.meta.tofile(self.filename,skip_validate=False)
            # self.my_log.info(f"Just recorded to file. This process took {time.perf_counter()-rec_start_time}")
            rec_start_time = time.perf_counter()
            self.meta.tofile(self.filename,skip_validate=True)
            self.my_log.info(f"Just recorded to file without validating. This process took {time.perf_counter()-rec_start_time}")

        rec_thread = threading.Thread(target=rec)
        rec_thread.daemon = True
        rec_thread.start()

    def set_freq_change(self, new_freq: float, change_time: float) -> None:
        """
        Inform of an upcoming frequency change

        Parameters
        ----------
        new_freq : float | int
            New frequency to change to
        change_time : float
            Expected time for the frequency change
        """

        # self.next_freq = new_freq
        # self.freq_change_time = change_time
        self.freq_change_sched.append((change_time, new_freq))

        self.my_log.info(
            f"Got info of an upcoming frequency change, from {self.center_freq}, to {self.freq_change_sched[-1][1]}, at timestamp {self.timestamp_to_date(self.freq_change_sched[-1][0])}"
        )

    def work(self, input_items, output_items):
        in0 = input_items[0]
        # <+signal processing here+>
        tags = self.get_tags_in_window(0, 0, len(in0))
        for tag in tags:
            if pmt.to_python(tag.key) == "tx_id":
                self.tx_node = pmt.to_python(tag.value)
                rx_date = None
                # rx_freq = None
                freq_offset = 0
                packet_num = -1
                local_tags = self.get_tags_in_range(0, tag.offset, tag.offset + 1)
                for local_tag in local_tags:
                    if pmt.to_python(local_tag.key) == self.carrier_offset_tag_key:
                        freq_offset += pmt.to_python(local_tag.value) * (
                            self.samp_rate / self.fft_len
                        )
                    if pmt.to_python(local_tag.key) == self.frac_offset_tag_key:
                        freq_offset += pmt.to_python(local_tag.value)
                    if pmt.to_python(local_tag.key) == "packet_num":
                        packet_num = pmt.to_python(local_tag.value)
                    if pmt.to_python(local_tag.key) == "packet_len":
                        packet_len = pmt.to_python(local_tag.value)

                    if pmt.to_python(local_tag.key) == "rx_freq":
                        if pmt.to_python(local_tag.value) is None:
                            self.my_log.warn("rx_freq recieved is None")
                        else:
                            self.my_log.warn("rx_freq recieved. This should not happen!!")
                            # self.center_freq = pmt.to_python(local_tag.value)

                    if pmt.to_python(local_tag.key) == self.time_tag_key:
                        rx_time = pmt.to_python(
                            local_tag.value
                        )  # Outputs a tuple with (seconds, fractions)
                        rx_time = (
                            rx_time[0] + rx_time[1]
                        )  # Combine into a float seconds
                        rx_date = self.timestamp_to_date(rx_time)
                        changed_freq = False
                        while (
                            self.freq_change_sched
                            and rx_time > self.freq_change_sched[0][0]
                        ):
                            _, next_freq = self.freq_change_sched.pop(0)
                            # if rx_time > self.freq_change_time:
                            self.my_log.info(
                                f"Time for frequency change, from {self.center_freq}, to {next_freq}, at timestamp {rx_date}"
                            )
                            self.center_freq = next_freq
                            changed_freq = True
                        if changed_freq:
                            self.record_current_data()


                        # self.my_log.info(rx_date)

                if rx_date is None:
                    self.my_log.error("Packet recieved without a time")
                if packet_num < 0:
                    self.my_log.error("Packet recieved without a number")
                if self.tx_node > 40:
                    self.my_log.error("Packet recieved from an impossible ID")
                    # continue

                # if rx_freq is None:
                #     self.my_log.warn("rx_freq not recieved with packet, defaulting to base value")
                #     rx_freq = self.center_freq
                self.meta._metadata[SigMFFile.CAPTURE_KEY] += [{
                        SigMFFile.START_INDEX_KEY: tag.offset,
                        SigMFFile.FREQUENCY_KEY: self.center_freq,
                        SigMFFile.DATETIME_KEY: rx_date,
                        "cxlb:tx_node": self.tx_node,
                        "cxlb:packet_num": packet_num,
                        "cxlb:packet_len": packet_len,
                        "ofdm:freq_offset": freq_offset,
                    }]
                # self.meta.add_capture(
                #     tag.offset,
                #     metadata={
                #         SigMFFile.FREQUENCY_KEY: self.center_freq,
                #         SigMFFile.DATETIME_KEY: rx_date,
                #         "cxlb:tx_node": self.tx_node,
                #         "cxlb:packet_num": packet_num,
                #         "cxlb:packet_len": packet_len,
                #         "ofdm:freq_offset": freq_offset,
                #     },
                # )

                self.my_log.warn(
                    f"Packet num {packet_num} recieved from {self.tx_node} and freq {self.center_freq}, at {rx_date}, with {freq_offset:.0f}Hz offset"
                )

                # self.meta.tofile(self.filename)
        return len(input_items[0])

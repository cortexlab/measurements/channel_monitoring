/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_STROBE_RANDOM_SEQ_IMPL_H
#define INCLUDED_CHANNEL_MONITORING_STROBE_RANDOM_SEQ_IMPL_H

#include <gnuradio/channel_monitoring/strobe_random_seq.h>
#include <gnuradio/random.h>
#include <atomic>

namespace gr {
namespace channel_monitoring {

class strobe_random_seq_impl : public strobe_random_seq
{
private:
    gr::thread::thread d_thread;
    std::atomic<bool> d_finished;
    unsigned int d_length;
    unsigned int d_msg_num;
    unsigned int d_modulo;
    long d_period_ms;
    const pmt::pmt_t d_port;
    gr::random d_rng;

    void run();

public:
    strobe_random_seq_impl(unsigned int length, long period_ms, unsigned int modulo);
    ~strobe_random_seq_impl();

    void set_modulo(unsigned int modulo) override { d_modulo = modulo; }
    unsigned int modulo() const override { return d_modulo; }
    void set_length(unsigned int length) override { d_length = length; }
    unsigned int length() const override { return d_length; }
    void set_period(long period_ms) override { d_period_ms = period_ms; }
    long period() const override { return d_period_ms; }

    // Overloading these to start and stop the internal thread that
    // periodically produces the message.
    bool start() override;
    bool stop() override;
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_STROBE_RANDOM_SEQ_IMPL_H */

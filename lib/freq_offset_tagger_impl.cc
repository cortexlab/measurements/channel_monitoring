/* -*- c++ -*- */
/*
 * Copyright 2023 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "freq_offset_tagger_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace channel_monitoring {

using input_type1 = float;
using input_type2 = uint8_t;
using output_type = float;
freq_offset_tagger::sptr freq_offset_tagger::make(float norm_factor, const std::string& tag_key)
{
    return gnuradio::make_block_sptr<freq_offset_tagger_impl>(norm_factor, tag_key);
}


/*
 * The private constructor
 */
freq_offset_tagger_impl::freq_offset_tagger_impl(float norm_factor, const std::string& tag_key)
    : gr::sync_block(
          "freq_offset_tagger",
          gr::io_signature::make2(2, 2, sizeof(input_type1), sizeof(input_type2)),
          gr::io_signature::make(1, 1, sizeof(output_type))),
      d_norm_factor(norm_factor)
{
    d_tag_key = pmt::intern(tag_key);
}

/*
 * Our virtual destructor.
 */
freq_offset_tagger_impl::~freq_offset_tagger_impl() {}

int freq_offset_tagger_impl::work(int noutput_items,
                                  gr_vector_const_void_star& input_items,
                                  gr_vector_void_star& output_items)
{
    auto in0 = static_cast<const input_type1*>(input_items[0]);
    auto in1 = static_cast<const input_type2*>(input_items[1]);
    auto out = static_cast<output_type*>(output_items[0]);

    // Do <+signal processing+>

    memcpy(out, in0, noutput_items * input_signature()->sizeof_stream_item(0));

    // Detect when in1 is non zero, and tag corresponding output sample

    for (int i = 0; i < noutput_items; i++) {
        if (in1[i] != 0) {
            // Tag output
            add_item_tag(0,
                         nitems_written(0) + i,
                         d_tag_key,
                         pmt::mp(in0[i] * d_norm_factor),
                         pmt::intern("Freq offset tagger"));
        }
    }


    // Tell runtime system how many output items we produced.
    return noutput_items;
}

} /* namespace channel_monitoring */
} /* namespace gr */

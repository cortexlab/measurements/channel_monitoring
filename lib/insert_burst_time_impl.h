/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_INSERT_BURST_TIME_IMPL_H
#define INCLUDED_CHANNEL_MONITORING_INSERT_BURST_TIME_IMPL_H

#include <gnuradio/channel_monitoring/insert_burst_time.h>

namespace gr {
namespace channel_monitoring {

class insert_burst_time_impl : public insert_burst_time
{
private:
    // Nothing to declare in this block.
    float d_tx_period;
    float d_start_margin;
    const std::string d_frame_tag_key;
    unsigned long d_epoch_ref;
    double d_fractional_ref;
    unsigned long d_frame_counter;

public:
    insert_burst_time_impl(float tx_period,
                           float start_margin,
                           const std::string& frame_tag_key);
    ~insert_burst_time_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);

    void set_ref_time(unsigned long epoch_time, double fractional_time) override
    {
        d_epoch_ref = epoch_time;
        d_fractional_ref = fractional_time;
        d_frame_counter = 0;
    };
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_INSERT_BURST_TIME_IMPL_H */

/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "insert_burst_time_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace channel_monitoring {

using input_type = gr_complex;
using output_type = gr_complex;
insert_burst_time::sptr insert_burst_time::make(float tx_period,
                                                float start_margin,
                                                const std::string& frame_tag_key)
{
    return gnuradio::make_block_sptr<insert_burst_time_impl>(
        tx_period, start_margin, frame_tag_key);
}

/*
 * The private constructor
 */
insert_burst_time_impl::insert_burst_time_impl(float tx_period,
                                               float start_margin,
                                               const std::string& frame_tag_key)
    : gr::sync_block("insert_burst_time",
                     gr::io_signature::make(1 , 1, sizeof(input_type)),
                     gr::io_signature::make(1 , 1, sizeof(output_type))),
                     d_tx_period(tx_period),
                     d_start_margin(start_margin),
                     d_frame_tag_key(frame_tag_key),
                     d_epoch_ref(0),
                     d_fractional_ref(-1.0),
                     d_frame_counter(0)
{

}

/*
 * Our virtual destructor.
 */
insert_burst_time_impl::~insert_burst_time_impl() {}


int insert_burst_time_impl::work(int noutput_items,
                                 gr_vector_const_void_star& input_items,
                                 gr_vector_void_star& output_items)
{
    auto in = static_cast<const input_type*>(input_items[0]);
    auto out = static_cast<output_type*>(output_items[0]);
    // Simply copy input to output
    memcpy(out, in, noutput_items * input_signature()->sizeof_stream_item(0));

    std::vector<tag_t> tags;
    get_tags_in_window(tags, 0, 0, noutput_items, pmt::intern(d_frame_tag_key));
    for (size_t i = 0; i < tags.size(); i++) {
        unsigned long tx_epoch_time = d_epoch_ref + (unsigned long)(d_start_margin+d_fractional_ref+d_tx_period*d_frame_counter);
        double tx_fraq_time = fmod(d_start_margin+d_fractional_ref+d_tx_period*(double)d_frame_counter, 1.0);

        add_item_tag(0,
                         tags[i].offset,
                         pmt::intern("tx_time"),
                         pmt::make_tuple(pmt::from_uint64(tx_epoch_time), pmt::from_double(tx_fraq_time)),
                         pmt::intern("Burst Time Inserter"));
        ++d_frame_counter;
    }

    // Tell runtime system how many output items we produced.
    return noutput_items;
}

} /* namespace channel_monitoring */
} /* namespace gr */

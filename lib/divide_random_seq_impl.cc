/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "divide_random_seq_impl.h"
#include <gnuradio/expj.h>
#include <gnuradio/io_signature.h>
#include <math.h>

namespace gr {
namespace channel_monitoring {

using input_type = gr_complex;
using output_type = gr_complex;
divide_random_seq::sptr divide_random_seq::make(const std::string& len_tag_key,
                                                const std::string& num_tag_key)
{
    return gnuradio::make_block_sptr<divide_random_seq_impl>(len_tag_key, num_tag_key);
}


/*
 * The private constructor
 */
divide_random_seq_impl::divide_random_seq_impl(const std::string& len_tag_key,
                                               const std::string& num_tag_key)
    : gr::tagged_stream_block("divide_random_seq",
                              gr::io_signature::make(1, 1, sizeof(input_type)),
                              gr::io_signature::make(1, 1, sizeof(output_type)),
                              len_tag_key),
      d_rng(0),
      d_len_tag_key(len_tag_key),
      d_num_tag_key(num_tag_key)
{
    set_relative_rate(1.0);
    set_tag_propagation_policy(TPP_ONE_TO_ONE);
}

/*
 * Our virtual destructor.
 */
divide_random_seq_impl::~divide_random_seq_impl() {}

int divide_random_seq_impl::calculate_output_stream_length(
    const gr_vector_int& ninput_items)
{
    int noutput_items = ninput_items[0];
    return noutput_items;
}

int divide_random_seq_impl::work(int noutput_items,
                                 gr_vector_int& ninput_items,
                                 gr_vector_const_void_star& input_items,
                                 gr_vector_void_star& output_items)
{
    auto in = static_cast<const input_type*>(input_items[0]);
    auto out = static_cast<output_type*>(output_items[0]);


    // Do <+signal processing+>
    std::vector<tag_t> tags;
    get_tags_in_window(tags, 0, 0, 1, pmt::intern(d_num_tag_key));
    unsigned long msg_num = 0;
    for (size_t i = 0; i < tags.size(); i++) {
        msg_num = pmt::to_uint64(tags[i].value);
    }

    d_rng.reseed(msg_num + 1);
    for (int i = 0; i < ninput_items[0]; i++) {
        gr_complex randval = gr_expj((d_rng.ran1() - 0.5) * (2 * M_PI));
        out[i] = in[i] / randval;
    }
    // Tell runtime system how many output items we produced.
    return ninput_items[0];
}

} /* namespace channel_monitoring */
} /* namespace gr */

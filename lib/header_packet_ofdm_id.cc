/* -*- c++ -*- */
/*
 * Copyright 2023 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/channel_monitoring/header_packet_ofdm_id.h>
#include <gnuradio/digital/header_buffer.h>
#include <gnuradio/digital/lfsr.h>
#include <volk/volk_alloc.hh>
#include <cstring>

namespace gr {
namespace channel_monitoring {

header_packet_ofdm_id::sptr
header_packet_ofdm_id::make(const std::vector<std::vector<int>>& occupied_carriers,
                            int n_syms,
                            const std::string& len_key_name,
                            const std::string& frame_key_name,
                            const std::string& num_key_name,
                            int bits_per_header_sym,
                            int bits_per_payload_sym,
                            bool scramble_header,
                            int tx_id)
{
    return header_packet_ofdm_id::sptr(new header_packet_ofdm_id(occupied_carriers,
                                                                 n_syms,
                                                                 len_key_name,
                                                                 frame_key_name,
                                                                 num_key_name,
                                                                 bits_per_header_sym,
                                                                 bits_per_payload_sym,
                                                                 scramble_header,
                                                                 tx_id));
}

header_packet_ofdm_id::header_packet_ofdm_id(
    const std::vector<std::vector<int>>& occupied_carriers,
    int n_syms,
    const std::string& len_key_name,
    const std::string& frame_key_name,
    const std::string& num_key_name,
    int bits_per_header_sym,
    int bits_per_payload_sym,
    bool scramble_header,
    int tx_id)
    : header_format_base(),
      d_header_number(0),
      d_header_len(0),
      d_crc_impl(
          8, 0x07, 0xFF, 0x00, false, false), // TODO Check for maybe change LSB booleans
      d_len_key_name(pmt::intern(len_key_name)),
      d_frame_key_name(pmt::intern(frame_key_name)),
      d_num_key_name(pmt::intern(num_key_name)),
      d_occupied_carriers(occupied_carriers),
      d_bits_per_payload_sym(bits_per_payload_sym)

{
    // Compute number of symbols in header from number of OFDM symbols and occupied
    // carriers per OFDM symbol
    for (int i = 0; i < n_syms; i++) {
        d_header_len += occupied_carriers[i].size();
    }
    // TODO: Log warning when header len is too small

    // Compute number of symbols containted in a full rotation. (Sums occupied carriers
    // per version)
    d_syms_per_set = 0;
    for (unsigned i = 0; i < d_occupied_carriers.size(); i++) {
        d_syms_per_set += d_occupied_carriers[i].size();
    }

    // Init scrambler mask
    d_scramble_mask = std::vector<uint8_t>(header_nbits(), 0);
    if (scramble_header) {
        // These are just random values which already have OK PAPR:
        gr::digital::lfsr shift_reg(0x8a, 0x6f, 7);
        for (size_t i = 0; i < header_nbytes(); i++) {
            for (int k = 0; k < bits_per_header_sym; k++) {
                d_scramble_mask[i] ^= shift_reg.next_bit() << k;
            }
        }
    }
}

header_packet_ofdm_id::~header_packet_ofdm_id() {}

bool header_packet_ofdm_id::format(int nbytes_in,
                                   const unsigned char* input,
                                   pmt::pmt_t& output,
                                   pmt::pmt_t& info)
{
    // Creating the output pmt copies data; free our own here when done.
    volk::vector<uint8_t> bytes_out(header_nbytes());

    // Should this throw instead of mask if the payload is too big
    // for 12-bit representation?
    nbytes_in &= 0x0FFF;

    // unsigned char buffer[] = { (unsigned char)(nbytes_in & 0xFF),
    //                            (unsigned char)(nbytes_in >> 8),
    //                            (unsigned char)(d_header_number & 0xFF),
    //                            (unsigned char)(d_header_number >> 8) };
    // uint8_t crc = d_crc_impl.compute(buffer, sizeof(buffer));

    // // Form 2 12-bit items into 1 2-byte item
    // uint32_t concat = 0;
    // concat = (d_header_number << 12) | (nbytes_in);

    digital::header_buffer header(bytes_out.data());
    header.add_field16(nbytes_in, 12, true);
    header.add_field16(d_header_number, 12, true);
    header.add_field8(d_tx_id);
    uint8_t crc = d_crc_impl.compute(bytes_out.data(), 4);
    header.add_field8(crc);

    // header.add_field32(concat, 24, true);
    // header.add_field8(crc);

    d_header_number++;
    d_header_number &= 0x0FFF;


    // Scramble
    size_t len;
    // uint8_t* out = pmt::u8vector_writable_elements(output, len);
    for (size_t i = 0; i < len; i++) {
        bytes_out[i] ^= d_scramble_mask[i];
    }

    // Package output data into a PMT vector
    output = pmt::init_u8vector(header_nbytes(), bytes_out.data());

    return true;
}

bool header_packet_ofdm_id::header_ok()
{
    uint16_t pktlen = d_hdr_reg.extract_field16(0, 12, false, true);    // Problème: on utilise pas ce qui arrive de mon fork :/
    uint16_t pktnum = d_hdr_reg.extract_field16(12, 12, false, true);
    uint8_t ti_is = d_hdr_reg.extract_field8(24, 8, false, true);
    uint8_t crc_rcvd = d_hdr_reg.extract_field8(32, 8, false, true);
    // Check CRC8
    unsigned char buffer[] = { (unsigned char)(pktlen & 0xFF),
                               (unsigned char)(pktlen >> 8),
                               (unsigned char)(pktnum & 0xFF),
                               (unsigned char)(pktnum >> 8) };
    uint8_t crc_clcd = d_crc_impl.compute(buffer, sizeof(buffer));

    return (crc_rcvd == crc_clcd);
}

bool header_packet_ofdm_id::parse(int nbits_in,
                               const unsigned char* input,
                               std::vector<pmt::pmt_t>& info,
                               int& nbits_processed)
{
    while (nbits_processed <= nbits_in) {
        // Remove scrambing and fill up header buffer. Scramble mask has 8 significant
        // bits per byte, while input has only one
        d_hdr_reg.insert_bit(
            ((d_scramble_mask[nbits_processed / 8] >> nbits_processed % 8) & 0x01) ^
            input[nbits_processed]);
        nbits_processed++;
        if (d_hdr_reg.length() == header_nbits()) {
            if (header_ok()) {
                int payload_len = header_payload();
                enter_have_header(payload_len);
                info.push_back(d_info);
                d_hdr_reg.clear();
                return true;
            } else {
                d_hdr_reg.clear();
                return false;
            }
            break;
        }
    }

    return true;
}

size_t header_packet_ofdm_id::header_nbits() const { return d_header_len; }

int header_packet_ofdm_id::header_payload()
{
    uint16_t pktlen = d_hdr_reg.extract_field16(0, 12, false, true);
    uint16_t pktnum = d_hdr_reg.extract_field16(12, 12, false, true);

    // Convert num bytes to num complex symbols in payload
    pktlen *= 8;
    uint16_t pldlen = pktlen / d_bits_per_payload_sym;
    if (pktlen % d_bits_per_payload_sym) {
        pldlen++;
    }

    // frame_len = # of OFDM symbols in this frame
    int framelen = pldlen / d_syms_per_set;
    int k = 0;
    int i = framelen * d_syms_per_set;
    while (i < pldlen) {
        framelen++;
        // i += d_occupied_carriers[k++].size();
        i += d_occupied_carriers[k].size();
    }

    d_info = pmt::make_dict();
    d_info = pmt::dict_add(d_info, d_len_key_name, pmt::from_long(pldlen));
    d_info = pmt::dict_add(d_info, d_num_key_name, pmt::from_long(pktnum));
    d_info = pmt::dict_add(d_info, d_frame_key_name, pmt::from_long(framelen));
    return static_cast<int>(pldlen);
}

} /* namespace channel_monitoring */
} /* namespace gr */

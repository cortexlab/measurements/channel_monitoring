/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "strobe_random_seq_impl.h"
#include <gnuradio/expj.h>
#include <gnuradio/io_signature.h>
#include <math.h>
#include <chrono>
#include <thread>

namespace gr {
namespace channel_monitoring {

strobe_random_seq::sptr strobe_random_seq::make(unsigned int length, long period_ms, unsigned int modulo)
{
    return gnuradio::make_block_sptr<strobe_random_seq_impl>(length, period_ms, modulo);
}


/*
 * The private constructor
 */
strobe_random_seq_impl::strobe_random_seq_impl(unsigned int length, long period_ms, unsigned int modulo)
    : gr::block("strobe_random_seq",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      d_finished(false),
      d_length(length),
      d_msg_num(0),
      d_modulo(modulo),
      d_period_ms(period_ms),
      d_port(pmt::mp("strobe")),
      d_rng(d_msg_num)
{
    message_port_register_out(d_port);
}

/*
 * Our virtual destructor.
 */
strobe_random_seq_impl::~strobe_random_seq_impl() {}

bool strobe_random_seq_impl::start()
{
    d_finished = false;
    d_thread = gr::thread::thread([this] { run(); });

    return block::start();
}

bool strobe_random_seq_impl::stop()
{
    // Shut down the thread
    d_finished = true;
    d_thread.interrupt();
    d_thread.join();

    return block::stop();
}

void strobe_random_seq_impl::run()
{
    while (!d_finished) {
        std::this_thread::sleep_for(
            std::chrono::milliseconds(static_cast<long>(d_period_ms)));
        if (d_finished) {
            return;
        }
        d_rng.reseed(++d_msg_num);

        pmt::pmt_t vector = pmt::make_c32vector(d_length, gr_complex(0));
        for (size_t i = 0; i < d_length; i++) {
            gr_complex randval = gr_expj((d_rng.ran1() - 0.5) * (2 * M_PI));
            pmt::c32vector_set(vector, i, randval);
        }
        pmt::pmt_t msg = pmt::cons(pmt::make_dict(), vector);

        message_port_pub(d_port, msg);
        d_msg_num = d_msg_num % d_modulo;
    }
}

} /* namespace channel_monitoring */
} /* namespace gr */

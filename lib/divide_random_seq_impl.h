/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_DIVIDE_RANDOM_SEQ_IMPL_H
#define INCLUDED_CHANNEL_MONITORING_DIVIDE_RANDOM_SEQ_IMPL_H

#include <gnuradio/channel_monitoring/divide_random_seq.h>
#include <gnuradio/random.h>

namespace gr {
namespace channel_monitoring {

class divide_random_seq_impl : public divide_random_seq
{
private:
    // Nothing to declare in this block.
    gr::random d_rng;
    const std::string d_len_tag_key;
    const std::string d_num_tag_key;

protected:
    int calculate_output_stream_length(const gr_vector_int& ninput_items);

public:
    divide_random_seq_impl(const std::string& len_tag_key,
                           const std::string& num_tag_key);
    ~divide_random_seq_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_int& ninput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_DIVIDE_RANDOM_SEQ_IMPL_H */

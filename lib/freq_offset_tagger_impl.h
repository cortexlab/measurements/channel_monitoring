/* -*- c++ -*- */
/*
 * Copyright 2023 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_FREQ_OFFSET_TAGGER_IMPL_H
#define INCLUDED_CHANNEL_MONITORING_FREQ_OFFSET_TAGGER_IMPL_H

#include <gnuradio/channel_monitoring/freq_offset_tagger.h>

namespace gr {
namespace channel_monitoring {

class freq_offset_tagger_impl : public freq_offset_tagger
{
private:
    // Nothing to declare in this block.
    float d_norm_factor;
    pmt::pmt_t d_tag_key;

public:
    freq_offset_tagger_impl(float norm_factor, const std::string& tag_key);
    ~freq_offset_tagger_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);

    
    void set_norm_factor(float norm_factor) { d_norm_factor = norm_factor; };
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_FREQ_OFFSET_TAGGER_IMPL_H */

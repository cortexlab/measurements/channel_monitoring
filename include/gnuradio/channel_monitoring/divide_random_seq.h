/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_DIVIDE_RANDOM_SEQ_H
#define INCLUDED_CHANNEL_MONITORING_DIVIDE_RANDOM_SEQ_H

#include <gnuradio/channel_monitoring/api.h>
#include <gnuradio/tagged_stream_block.h>

namespace gr {
namespace channel_monitoring {

/*!
 * \brief <+description of block+>
 * \ingroup channel_monitoring
 *
 */
class CHANNEL_MONITORING_API divide_random_seq : virtual public gr::tagged_stream_block
{
public:
    typedef std::shared_ptr<divide_random_seq> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of
     * channel_monitoring::divide_random_seq.
     *
     * To avoid accidental use of raw pointers, channel_monitoring::divide_random_seq's
     * constructor is in a private implementation
     * class. channel_monitoring::divide_random_seq::make is the public interface for
     * creating new instances.
     */
    static sptr make(const std::string& len_tag_key, const std::string& num_tag_key);
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_DIVIDE_RANDOM_SEQ_H */

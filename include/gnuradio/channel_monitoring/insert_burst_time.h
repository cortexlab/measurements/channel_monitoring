/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_INSERT_BURST_TIME_H
#define INCLUDED_CHANNEL_MONITORING_INSERT_BURST_TIME_H

#include <gnuradio/channel_monitoring/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace channel_monitoring {

/*!
 * \brief This block adds a new tag of key tx_time on each sample with a tag of key
 * frame_tag_key.
 * \ingroup channel_monitoring
 * \details This tx_time tag contains a uhd formatted time that increases by
 * tx_period each emitted tag and starts from a reference time (that can be reset with
 * set_ref_time), with an added start_margin
 */
class CHANNEL_MONITORING_API insert_burst_time : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<insert_burst_time> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of
     * channel_monitoring::insert_burst_time.
     *
     * To avoid accidental use of raw pointers, channel_monitoring::insert_burst_time's
     * constructor is in a private implementation
     * class. channel_monitoring::insert_burst_time::make is the public interface for
     * creating new instances.
     * \param tx_period Time period between each tag: Amount (of seconds) to increment the
     * emitted tag value at each time
     * \param start_margin Time (in s) to add to reference. Can be used to compensate for
     * delays on digital and analog processing chains
     * \param frame_tag_key Key (name) of the tag to look for
     */
    static sptr
    make(float tx_period, float start_margin, const std::string& frame_tag_key);

    /*!
     * \brief Give to block a new time reference to base countings
     * \param epoch_time Number of integer seconds since epoch
     * \param fractional_time Fractions of seconds since epochs
     */
    virtual void set_ref_time(unsigned long epoch_time, double fractional_time) = 0;
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_INSERT_BURST_TIME_H */

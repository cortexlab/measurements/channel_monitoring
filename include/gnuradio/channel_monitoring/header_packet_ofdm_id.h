/* -*- c++ -*- */
/*
 * Copyright 2023 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_HEADER_PACKET_OFDM_ID_H
#define INCLUDED_CHANNEL_MONITORING_HEADER_PACKET_OFDM_ID_H

#include <gnuradio/channel_monitoring/api.h>
#include <gnuradio/digital/crc.h>
#include <gnuradio/digital/header_format_base.h>
#include <gnuradio/digital/header_format_default.h>
#include <pmt/pmt.h>

namespace gr {
namespace channel_monitoring {

/*!
 * \brief Header formatter that includes the payload length,
 * packet number, transmitter id, and a CRC check on the header.
 * \ingroup packet_operators_blk
 *
 * \details
 *
 * Child class of header_format_base. This version's header
 * format looks like:
 *
 * \li length (12 bits): length of the payload
 * \li number (12 bits): packet number
 * \li tx id  (8 bits):  ID of transmitter
 * \li CRC8 (8 bits): A CRC8 check on the header contents
 *
 * Instead of duplicating the payload length, we only add it once
 * and use the CRC8 to make sure it's correctly received.
 *
 * \verbatim
     |  0 -- 11 | 12 -- 23 | 24 -- 31 | 32 -- 39 |
     |    len   | pkt len  |  tx  id  |   CRC8   |
   \endverbatim
 *
 * Reimplements packet_header_default in the style of the
 * header_format_base.
 */
class CHANNEL_MONITORING_API header_packet_ofdm_id : public digital::header_format_base
{
public:
    header_packet_ofdm_id(const std::vector<std::vector<int>>& occupied_carriers,
                          int n_syms,
                          const std::string& len_key_name = "packet_len",
                          const std::string& frame_key_name = "frame_len",
                          const std::string& num_key_name = "packet_num",
                          int bits_per_header_sym = 1,
                          int bits_per_payload_sym = 1,
                          bool scramble_header = false,
                          int tx_id = 1);
    ~header_packet_ofdm_id() override;

    /*!
     * \brief Encodes the header information in the given tags into
     * bits and places them into \p out.
     *
     * \details
     * Uses the following header format:
     *  - Bits 0-11: The packet length (what was stored in the tag with key \p
     * len_tag_key)
     *  - Bits 12-23: The header number (counts up everytime this function is called)
     *  - Bit 24-31: ID of transmitter
     *  - Bit 32-39: 8-Bit CRC
     */
    bool format(int nbytes_in,
                const unsigned char* input,
                pmt::pmt_t& output,
                pmt::pmt_t& info) override;

    bool parse(int nbits_in,
               const unsigned char* input,
               std::vector<pmt::pmt_t>& info,
               int& nbits_processed) override;

    /*!
     * Returns the length of the formatted header in bits.
     */
    size_t header_nbits() const override;

    void set_tx_id(uint8_t id){d_tx_id = id;};

    /*!
     * Factory to create an async packet header formatter; returns
     * an sptr to the object.
     */
    static sptr make(const std::vector<std::vector<int>>& occupied_carriers,
                     int n_syms,
                     const std::string& len_key_name = "packet_len",
                     const std::string& frame_key_name = "frame_len",
                     const std::string& num_key_name = "packet_num",
                     int bits_per_header_sym = 1,
                     int bits_per_payload_sym = 1,
                     bool scramble_header = false,
                     int tx_id = 1);

protected:
    pmt::pmt_t d_frame_key_name; //!< Tag key of the additional frame length tag
    const std::vector<std::vector<int>>
        d_occupied_carriers; //!< Which carriers/symbols carry data
    int d_syms_per_set;      //!< Helper variable: Total number of elements in
                             //!< d_occupied_carriers
    int d_bits_per_payload_sym;
    std::vector<uint8_t> d_scramble_mask; //!< Bits are xor'd with this before tx'ing
    size_t d_header_len;
    uint16_t d_header_number;
    pmt::pmt_t d_len_key_name;
    pmt::pmt_t d_num_key_name;
    uint8_t d_tx_id;
    digital::crc d_crc_impl;

    //! Verify that the header is valid
    bool header_ok() override;

    /*! Get info from the header; return payload length and package
     *  rest of data in d_info dictionary.
     */
    int header_payload() override;
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_HEADER_PACKET_OFDM_ID_H */

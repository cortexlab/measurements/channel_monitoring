/* -*- c++ -*- */
/*
 * Copyright 2023 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_FREQ_OFFSET_TAGGER_H
#define INCLUDED_CHANNEL_MONITORING_FREQ_OFFSET_TAGGER_H

#include <gnuradio/channel_monitoring/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace channel_monitoring {

/*!
 * \brief Tags stream with value corresponding to non-zero value at input 1
 * \ingroup channel_monitoring
 *
 * \details This block has two inputs, both consumed at the same rate, and one output
 * The first (float) input is directly forwarded without modification (simple copy)
 * The second (byte) is used as a trigger: When a non-zero sample is detected, the value
 * of the corresponding sample in the first input is read.
 * That value is then multiplied by the norm_factor and written as a tag to the output
 * (on the same sample)
 */
class CHANNEL_MONITORING_API freq_offset_tagger : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<freq_offset_tagger> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of
     * channel_monitoring::freq_offset_tagger.
     *
     * To avoid accidental use of raw pointers, channel_monitoring::freq_offset_tagger's
     * constructor is in a private implementation
     * class. channel_monitoring::freq_offset_tagger::make is the public interface for
     * creating new instances.
     */
    static sptr make(float norm_factor, const std::string& tag_key = "frac_freq_offset");

    /*!
     * \brief Set new normalisation factor
     */
    virtual void set_norm_factor(float norm_factor) = 0;
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_FREQ_OFFSET_TAGGER_H */

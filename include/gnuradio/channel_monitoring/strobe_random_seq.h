/* -*- c++ -*- */
/*
 * Copyright 2024 Cyrille Morin.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CHANNEL_MONITORING_STROBE_RANDOM_SEQ_H
#define INCLUDED_CHANNEL_MONITORING_STROBE_RANDOM_SEQ_H

#include <gnuradio/block.h>
#include <gnuradio/channel_monitoring/api.h>

namespace gr {
namespace channel_monitoring {

/*!
 * \brief Sends a random message at define interval
 * \ingroup channel_monitoring
 * \details
 * Sends a PMT message of length \p length every \p period_ms milliseconds.
 * Messages are complex vectors of unitary magnitude and random phase
 * Seed of random vector is changed each message to the message number modulo \p modulo
 */
class CHANNEL_MONITORING_API strobe_random_seq : virtual public gr::block
{
public:
    typedef std::shared_ptr<strobe_random_seq> sptr;

    /*!
     * \brief Make a block to send random phase messages every \p period_ms millisecond
     *
     * \param length The length of each message
     * \param period_ms the time period in milliseconds in which to send messages
     * \param modulo Maximum value of message number (rolls back to 0 when reaching it)
     */
    static sptr make(unsigned int length, long period_ms, unsigned int modulo = 4096);

    /*!
     * Reset the modulo value.
     * \param modulo Maximum value of message number (rolls back to 0 when reaching it)
     */
    virtual void set_modulo(unsigned int modulo) = 0;

    /*!
     * Get the value of the modulo parameter
     */
    virtual unsigned int modulo() const = 0;

    /*!
     * Reset the message being sent.
     * \param length The random message length to send
     */
    virtual void set_length(unsigned int length) = 0;

    /*!
     * Get the value of the message length being sent.
     */
    virtual unsigned int length() const = 0;

    /*!
     * Reset the sending interval.
     * \param period_ms the time period in milliseconds.
     */
    virtual void set_period(long period_ms) = 0;

    /*!
     * Get the time interval of the strobe.
     */
    virtual long period() const = 0;
};

} // namespace channel_monitoring
} // namespace gr

#endif /* INCLUDED_CHANNEL_MONITORING_STROBE_RANDOM_SEQ_H */

#import "@preview/polylux:0.3.1": *

// Typic theme
//
// Originally contributed by Pol Dellaiera - https://github.com/drupol
//
// Please feel free to improve this theme
// by submitting a PR in https://github.com/andreasKroepelin/typst-slides

#let uni-colors = state("uni-colors", (:))
#let uni-short-title = state("uni-short-title", none)
#let uni-short-author = state("uni-short-author", none)
#let uni-short-date = state("uni-short-date", none)
#let uni-progress-bar = state("uni-progress-bar", true)

#let inria_red = rgb("#db5d47")
#let inria_gblue = rgb("#384257")
#let inria_orange = rgb("#f07e26")
#let inria_yellow = rgb("#ffcd1c")
#let inria_lilas = rgb("#9b004f")
#let inria_purple = rgb("#6561a9")
#let inria_blue = rgb("#1488ca")
#let inria_lblue = rgb("#89ccca")
#let inria_green = rgb("#95c11f")
#let inria_lgreen = rgb("#c7d64f")
#let inria_white = rgb("#ffffff")

#let typic-theme(
  aspect-ratio: "16-9",
  short-title: none,
  short-author: none,
  short-date: none,
  inria_gblue: inria_gblue,
  inria_red: inria_red,
  inria_white: inria_white,
  progress-bar: true,
  body,
) = {


  set page(
    paper: "presentation-" + aspect-ratio,
    margin: 0em,
    // footer: footer-layout(),
    // footer-descent: 0em,
    // header: none,
    // footer: none,
  )
  // set align(left)
  set text(font: "Inria Sans")
  set text(size: 22pt)
  show footnote.entry: set text(size: .6em)

  uni-progress-bar.update(progress-bar)
  uni-colors.update((a: inria_gblue, b: inria_red, c: inria_white))

  uni-short-title.update(short-title)
  uni-short-author.update(short-author)
  uni-short-date.update(short-date)
  
  body

}

#let progress-barline = locate(
  loc => {
    if uni-progress-bar.at(loc) {
      let cell = block.with(width: 100%, height: 100%, above: 0pt, below: 0pt, breakable: false)
      // let colors = uni-colors.at(loc)

      utils.polylux-progress(ratio => {
        grid(
          rows: 4pt,
          columns: (ratio * 100%, 1fr),
          cell(fill: inria_gblue),
          cell(fill: inria_red),
        )
      })
    } else { [] }
  },
)

#let header-text-layout(title: none, header: none, new-section: none) = {
  if header != none {
    header
  } else if title != none {
    if new-section != none {
      utils.register-section(new-section)
    }
    locate(
      loc => {
        // let colors = uni-colors.at(loc)
        block(
          fill: inria_white,
          inset: (x: .5em),
          grid(
            columns: (60%, 40%),
            align(top + left, heading(level: 2, text(fill: inria_gblue, title))),
            align(top + right, text(fill: inria_gblue.lighten(65%), utils.current-section)),
          ),
        )
      },
    )
  } else { [] }
}

#let header-layout(header-text) = {
  set align(top)
  grid(rows: (auto, auto), row-gutter: 3mm, progress-barline, header-text)
}

#let footer-layout(footer: none) = {
  set text(size: 10pt)
  set align(center + bottom)
  let cell(fill: none,text-color: white, it) = rect(
    width: 100%,
    height: 100%,
    inset: 1mm,
    outset: 0mm,
    fill: fill,
    stroke: none,
    align(horizon, text(fill: text-color, it)),
  )
  if footer != none {
    footer
  } else {
    locate(loc => {
      // let colors = uni-colors.at(loc)

      show: block.with(width: 100%, height: auto, fill: inria_red)
      grid(
        columns: (25%, 1fr, 15%, 10%),
        rows: (1.5em, auto),
        cell(fill: inria_gblue, uni-short-author.get()),
        cell(text-color: black, uni-short-title.get()),
        cell(fill: inria_gblue, uni-short-date.get()),
        cell(
          fill: inria_gblue,
          logic.logical-slide.display() + [~/~] + utils.last-slide-number,
        ),
      )
    })
  }
}

#let title-slide(
  title: [],
  subtitle: none,
  authors: (),
  institution-name: "University",
  date: none,
  logo: (),
) = {

  set page(
    margin: (top: 1em, bottom: 1em, x: 0em),
    footer: footer-layout(),
    footer-descent: 0em,)
  // let background-color = inria_gblue
  let background-color = inria_red
  let authors = if type(authors) == "array" { authors } else { (authors,) }

    
  let content = locate(loc => {
    // let colors = uni-colors.at(loc)

    align(center + horizon, {
      block(
        width: 80%,
        fill: background-color,
        spacing: 1pt,
        // stroke: inria_green,
        inset: 0.8em,
        breakable: false,
        {
          text(size: 2em, fill: white, strong(title))
          if subtitle != none {
            parbreak()
            text(size: 1.2em, fill: white, subtitle)
          }
        },
      )
      set text(size: .8em)
      grid(
        columns: (1fr,) * calc.min(authors.len(), 3),
        column-gutter: 1em,
        row-gutter: 1em,
        ..authors.map(author => text(fill: black, author)),
      )
      v(1em)
      if institution-name != none {
        parbreak()
        text(size: .9em, institution-name)
      }
      if date != none {
        parbreak()
        text(size: .8em, date)
      }

      if logo.len() > 0 {
        grid(columns: (1fr,) * logo.len(), align: horizon, ..logo)
      }
    })
  })
  // content
  logic.polylux-slide(content)
}

#let slide(title: none, header: none, footer: none, new-section: none, body) = {
  let body = pad(x: 2em, y: .5em, body)

  set page(
    margin: (top: 2em, bottom: 1em, x: 0em),
    header: header-layout(header-text-layout(title: title, header: header, new-section: new-section)),
    footer: footer-layout(),
    footer-descent: 0em,
    header-ascent: .6em,
  )
  // body
  logic.polylux-slide(body)
}

#let text-block(title: none, title-fill: inria_gblue, body-fill: color.mix((inria_gblue, 30%), (white, 70%)), body) = {
  set align(horizon)
  block(
      breakable: false,
      spacing: 0.5em,[
    #block(
      fill: title-fill,
      spacing: 0em,
      breakable: false,
      width: 100%,
      inset: 10pt,
      text(fill:white, strong(title)),
    )
    #block(
      fill: body-fill,
      above: 0em,
      below: 1em,
      breakable: false,
      width: 100%,
      inset: 20pt,
      body,
    )])
}

#let focus-slide(background-color: none, background-img: none, body) = {
  let background-color = if background-img == none and background-color == none {
    inria_gblue
  } else {
    background-color
  }

  set page(fill: background-color, margin: 1em) if background-color != none
  set page(background: {
    set image(fit: "stretch", width: 100%, height: 100%)
    background-img
  }, margin: 1em) if background-img != none

  set text(fill: white, size: 2em)

  logic.polylux-slide(align(horizon, body))
}

#let matrix-slide(columns: none, rows: none, ..bodies) = {
  let bodies = bodies.pos()

  let columns = if type(columns) == "integer" {
    (1fr,) * columns
  } else if columns == none {
    (1fr,) * bodies.len()
  } else {
    columns
  }
  let num-cols = columns.len()

  let rows = if type(rows) == "integer" {
    (1fr,) * rows
  } else if rows == none {
    let quotient = calc.quo(bodies.len(), num-cols)
    let correction = if calc.rem(bodies.len(), num-cols) == 0 { 0 } else { 1 }
    (1fr,) * (quotient + correction)
  } else {
    rows
  }
  let num-rows = rows.len()

  if num-rows * num-cols < bodies.len() {
    panic(
      "number of rows (" + str(num-rows) + ") * number of columns (" + str(num-cols) + ") must at least be number of content arguments (" + str(bodies.len()) + ")",
    )
  }

  let cart-idx(i) = (calc.quo(i, num-cols), calc.rem(i, num-cols))
  let color-body(idx-body) = {
    let (idx, body) = idx-body
    let (row, col) = cart-idx(idx)
    let color = if calc.even(row + col) { white } else { silver }
    set align(center + horizon)
    rect(inset: .5em, width: 100%, height: 100%, fill: color, body)
  }

  let content = grid(
    columns: columns,
    rows: rows,
    gutter: 0pt,
    ..bodies.enumerate().map(color-body),
  )

  logic.polylux-slide(content)
}

#import "@preview/touying:0.4.2": *
#import "dropic.typ"

// #set text(font: "Inria Sans")
// #set page(fill: inria_gblue)
  // #set page(
  //   // margin: (top: 1em, bottom: 1em, x: 0em),
  //   footer: footer-layout(),
  //   footer-descent: 0em,)
// #let uni-short-title = "Chan Monitoring"
// #let uni-short-author = "C. Morin"
// #let uni-short-date = "31/05/24"

#let s = dropic.register(aspect-ratio: "16-9", navigation: "mini-slides",
  mini-slides: (height: 1.75em, x: 2em, section: true, subsection: false),)
#let s = (s.methods.info)(
  self: s,
  title: [Channel Measurements in CorteXlab],
  short-title: "Chan Monitoring",
  author: [Cyrille Morin],
  date: datetime.today(),
  institution: [Équipe Maracas],
  logo: (image("maracas_logo.png", width: 20mm), image("inr_logo_rouge.svg", width: 40mm), image("maracas_logo.png", width: 20mm))
)

#let (init, slides, touying-outline, alert, speaker-note, text-block) = utils.methods(s)
#show: init

// #show strong: alert
#show figure: set align(horizon)

#let (slide, empty-slide, title-slide, new-section-slide, focus-slide) = utils.slides(s)
#show: slides
// #show: typic-theme.with(
//   short-author: "C. Morin",
//   short-title: "Chan Monitoring",
//   short-date: "31/05/24",
//   // color-a: inria_red,
//   // color-b: inria_gblue,
//   // color-c: inria_purple,
// )


// #title-slide(
//   authors: ("Cyrille Morin"),
//   title: "Channel Measurements in CorteXlab",   
//   // subtitle: "LetSe Gooo", 
//   date: "23 Mai 2024",
//   institution-name: "Équipe Maracas",
//   logo: (image("maracas_logo.png", width: 20mm), image("inr_logo_rouge.svg", width: 40mm), image("maracas_logo.png", width: 20mm))
// ) 
= Intro
// #slide(title: [Plan])[
//   // #polylux-outline(padding: 100pt)
// ]
=== Motivations
  #text-block(title: [Needs from CorteXlab experiment room])[
    Room advertised as reliable, reproducible, stable platform for radio comm experiments

    Lacking data to support these claims
  ]

  #text-block(title: [Needs from Experimental projects])[
  - High sensitivity to channel state and variations (Tx-Id)
  - Channel statistics data (AUD with GAMP)
  - Active use of channel state (Backscattering)
  ]

=== Previous attempts

Path loss measurements (Othmane). 
Doesn't show time variations nor frequency selectivity

Frequency measurement made during an internship (Achille Mouaffo, 2014). 
Doesn't study channel stability, nor wideband frequency responses. (And based on matlab code)

=== Objectives

+ Build a channel measurement system
+ Something easy to redeploy to get new data points.
+ Gather raw frequency responses for freedom in post processing
+ Something flexible to adapt to different needs (frequency coverage, time resolutions, frequency resolution, ...)
+ Get highest possible sample rate or frequency coverage

= Data gathering system


=== High level description
#text-block(title: [Based on GNU Radio OFDM transmission chain])[
- Known data is transmitted in a frame (aka Pilots)
- Modulation in OFDM
- Regular frame broadcast
- Standard demodulator
- Channel state for each subcarrier extracted from known data
- Channel state + metadata recorded for offline processing
]

=== Frame Pattern
#figure(
  image("Frame_pattern.svg", width: 110%), caption: "Frame Pattern"
)

=== Frame Structure
  #figure(
    image("Frame_structure.svg"), caption: "Header and frame structure"
  )

=== Parameters summary
  // A table of all useful parameters (to highlight flexibility)
// #set align(horizon)
#text-block(title: [Base Params (CLI args)])[
#grid(columns: (1fr,1fr,1fr), gutter: 1em, rows: (auto),[
  - Sample Rate
  - Center Frequency],[
  - FFT Length
  - TX/RX Gain],[
  - Frame TX Period
  - Device ID
])]

#text-block(title: [Internal Variables])[
#grid(columns:  (1fr,1fr,1fr),rows:(auto),[
  - Occupied Carriers],[
  - Cyclic Prefix Length],[
  - Symbols per Frame
])] 

#slide(title: [Transmitted information], repeat:3, self => [
  #let (uncover, only, alternatives) = utils.methods(self)
  #text-block(title: [Need to define transmitted sequence])[
    - Simple method: Like real pilots, fill in 1 everywhere
      #uncover("2-")[- Causes a very high PAPR #sym.arrow.r *Unuseable*]
    #only("3")[- Backup plan: Keep amplitude to 1, and vary phase]
  ]

  #only(2)[  
    #figure(
    image("PAPR_pilots_full_ones.jpg", width: 80%), caption: "Frames with pilot sequence full of ones"
  )]

  #only(3)[#figure(
    image("PAPR_pilots_random_phase.jpg", width: 80%), caption: "Frames with pilot sequence random phase and amplitude 1"
  )]
])

=== Recorded elements

#text-block(title: [Raw complex valued CSI for each used subcarrier plus metadata])[
  - Transmitter id
  - Reception time
  - Number of samples (chan coeffs for the frame)
  - Center frequency (of RX)
  - Frequency offset (estimated by Schmidl&Cox algorithm)
]

#text-block(title: [Global recording metadata])[
  #grid(columns:  (1fr,1fr,1fr),rows:(auto),[
    - Sample rate
    - FFT length],[
    - receiver id
    - Author info],[
    - Used subcarriers
    - Description
  ])  
]

=== Performance considerations
  // Mention that we want to measure over highest bw possible, but with limiting hardware.

  #text-block(title: "Two USRP models, two possible bandwidths")[
  - 2932 #sym.arrow.r 25Msps, 20MHz BW
  - 2944 #sym.arrow.r 200Msps, 160MHz BW
  ]

  #text-block(title: "Some software optimisation")[
  - All (but one) blocks in C++
  - Metadata recording only triggered at exp end and not on each recieved packet
  ]

  #text-block(title: [Tradeoff: Bandwidth #sym.arrows.lr Frame Period #sym.arrows.lr Reliability])[
    - More Bandwidth (sps) or less Frame period increases CPU load, risking sample losses
    - Current max: 25Msps and 100 fps without losses
    - Above: some frames still make it (up to 100Msps), allowing for some data points
  ]
  // Currently able to run without samples loss at 25Msps, and 100 frames per second, but not at 200Msps.
  // Losing some samples at the receiver because CPU can not process fast enough means that some frames are lost.
  // For measurement of slow processes, it may not matter.
  // I managed to get some frames across at up to 100Msps. (Many frame losses, but not all of them :))


=== Intra-experiment Frequency Sweep
  // Maximum frequency BW is limited to 25MHz for most available USRPs (100MHz for the others)
  // It would be really useful to have a way to get data on the whole frequency band.

  // Thus, we need to sweep frequencies.
  #text-block(title: [Measurement needs])[
    - Limited max BW. By both hardware and software
    - Interested in whole reachable frequencies by available USRPs
    - Simple solution: One CorteXlab experiment per center frequency
     - Requires restarting containers at each step
     - Loses at least 10s every time #sym.arrow.r *Impractical*
  ]

  #text-block(title: [Intra-experiment frequency sweeper: New constraints])[
  - All participating devices need to change frequency at the same time
  - Need to record when the frequency change happens, to properly annotate records
  #sym.arrow.curve Needs synchronisation between devices (Radios and PCs)
  ]

=== Time Sync Protocol
  

#grid(columns:  (2fr,2fr),rows:(auto), gutter: .5em, //fill: gray,
[

  #text-block(title: [Requirements])[
    - All devices connected to PPS source (e.g. Octoclock)
    - Full network communication over UDP (e.g. Ethernet)
    - TX knows about all participants
  ]

  #text-block(title: [Variables])[
    - Max retry number
    - UDP Port
    - Min margin before next PPS
    - Activate Sync (CLI Parameter)
  ]
  //   So I present the system I build to synchronise these changes
  // Based on octoclock sync (means only connected radios can participate)
  // First back and forth before startup to begin at same second
  // Decentralised commands for freq changes on each radio, based on predetermined time frame.
  // Recorded freq information is based on time data, and not device feedback
  ],
  [
    // #set align(left)
    // #figure(
      #image("Time_sync_protocol.svg", width: 100%)
      // , caption: "Time Sync Protocol")
      ]
    )

=== Sweep Operation

#text-block(title: [Issues])[
    - Latency incrodudced by centralised command
    - Frequency Change not accurately reported by USRP
    - USRP commands run in order #sym.arrow.r Cannot query/poll value to check if setting took place
  ]

#text-block(title: [Operation])[
  - Decentralised operation, fully relying on PPS synchronisation
  - Timed commands to set values on specific USRP clock times
  - Frequency recording relies on knowledge of command timing, not USRP feedback
]

#text-block(title: [Variables])[
    - Starting center frequency (Same param as non sweep operation)
    - Max center frequency, Frequency step (Uses Python range())
    - Step duration
  ]

=== Some first results
#figure(
    image("100M_freq_resp_patterned_w_std.png", width: 85%),
    gap: 0em,
    caption: [100MHz wide frequency responses for 4 USRP 2944.\
    Gathered over 5mn]
  )

=== Some first results
#figure(
    image("25M_freq_resp_patterned_w_std.png", width: 85%),
    gap: 0em,
    caption: [25MHz wide frequency responses for 2 USRP 2944 and one USRP 2932.\
    Gathered between 27/01/24 and 29/01/24 (67h)]
  )
  
=== Some first results
#text-block(title: [First takeways])[
    - 25MHz wide PSD does work (slightly less because edge subcarriers are unused)
    - Different responses with clear fading effect from different positions
    - Responses over one weekend does not vary much
      // #pause
    - Can you see the pattern?
  ]

=== Pilot sequence imprinting

#text-block(title: [What's happening?])[
    - Constant sequence: Exact same transmitted frame every time
    - Even though Amplitude is constant, still imprints on the PSD
  ]

#text-block(title: [Workaround])[
  - Two new blocks. One at TX, One at RX
  - Pilot sequence still random (in phase)
  - Random seed based on frame number (Info available at RX for decoding)
  - Can then average over more sequences ($2^12 = 4096$ possibilities)
]

=== Averaged sequence

#figure(
    image("25M_freq_resp_smoothed_w_std.png", width: 85%),
    gap: 0em,
    caption: [25MHz wide frequency responses for 2 USRP 2944 and one USRP 2932.\
    Gathered between 11/06/24 and 12/06/24 (15h)]
  )

= Extracted Data
=== What to do with it?

  #text-block(title: [Is averaged PSD all we can do?])[
  - Responses above are from averaging of subcarrier responses over the recorded period.
  - Raw data is available
  - We are free to post process to extract other metrics
]
#pause
#text-block(title: [First example: Power Delay Profile (PDP)])[
  - Evolution of recieved power with time, after reception of first component
  - Can show echoes, delays in recieved signal
  - Can be simply extracted as iFFT of complex valued frequency response
]

=== Power Delay Profile

#figure(
    image("25M_pdp_w_std.png", width: 85%),
    gap: 0em,
    caption: [PDP for 2 USRP 2944 and one USRP 2932.
    Same dataset as previous plot]
  )

=== Impulse response

  #text-block(title: [Simple and common metric])[
  - Channels often modeled or measured as impulse responses
  - Can be simply extracted as iFFT of complex valued frequency response
]
  #pause
  #text-block(title: [Not practical as aggregated metric])[
  - Frames can get decoded a couple of samples early or late
  #sym.arrow.curve Reception phase vary significantly\
  #sym.arrow.curve Extracted response impacted\
  #sym.arrow.curve Leads to bad quality averaging
]

=== Time stability

  #text-block(title: [Is the channel stable over time?])[
  - One of the starting questions
  - Need to not study averaged responses, but time variation, from sample to sample, or frame to frame.
]

  #text-block(title: [Chosen first approach])[
  - Set a reference PSD
  - For each frame (averaging symbols), Compute mean square error (MSE) with reference. (comparing subcarrier per subcarrier)
  - This gives one distance number per frame, that we can then plot over time.
]

#text-block(title: [Possible references])[
  - PSD from one actual frame. E.g. First, Last, Middle, ...
  - Averaged PSD (as plotted previously)
]

=== Without movement

#figure(
    image("we_measurement_rx10_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Gathered between 27/01/24 and 29/01/24 (67h)]
  )
=== Without movement
  #figure(
    image("we_measurement_rx11_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Gathered between 27/01/24 and 29/01/24 (67h)]
  )
=== Without movement
  #figure(
    image("night_measurement_rx14_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Gathered between 11/06/24 and 12/06/24 (15h)]
  )

=== With people

    #figure(
    image("arun_walk_rx10_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Gathered while doing a tour of the room with a visitor]
  )

=== With displaced equipment

  #figure(
    image("stool_mvt_rx10_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Successive displacements of a metallic stool (and leaving the room between operations)]
  )
=== With displaced equipment
  #figure(
    image("stool_mvt_rx25_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Successive displacements of a metallic stool (and leaving the room between operations)]
  )

=== With moving robot

  #figure(
    image("uncovered_robot_mv_rx10_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Robot (uncovered) moving around]
  )

  === With moving robot

  #figure(
    image("robotrx_v_avg_wo_outliers_.png", width: 85%),
    gap: 0em,
    caption: [Evolution of frames recieved by the moving robot (Time axis not reliable)]
  )

  === With moving robot

  #figure(
    image("robotrx_freq_response_various_times.png", width: 85%),
    gap: 0em,
    caption: [PSD from various frames recieved by the moving robot over the length of the experiment]
  )

=== Stability conclusion

  #text-block(title: [Takeaways])[
  - Even without movement, there is still some visible small variation in recieved CSI
  - This appears to be related to gain in the hardware
  - Having a random transmitted sequence increase the CSI noise (because we compare several patterns)
  - Movement of people or objects has large effect
]

 #text-block(title: [Metric feedback])[
  - MSE metric appear effective to visualise variations
  - Could there be other useful metrics of interest to users?
]

= Sweep post processing
=== Sweep post processing
  // We can now have frequency and/or time responses for several bands, but it's all disjointed.

  *What if we could merge neighboring measurement to have a wider picture?*

=== Juxtaposed display

 #figure(
    image("juxtaposed_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Combination of PSDs for all center frequencies of a full range frequency sweep]
  )

=== Juxtaposed display

 #figure(
    image("juxtaposed_zoomed_500MHz_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Zoom of previous plot around 500MHz]
  )

=== Juxtaposed display

 #figure(
    image("juxtaposed_zoomed_1500MHz_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Zoom of previous plot around 1.5GHz]
  )

=== Juxtaposed display

  #text-block(title: [First observations])[
  - The filtering is impacting  measurements
  - There is an overall shape of the recieved amplitude (what could it be?)
  - There are some steps in the response
]

=== Filter calibration

#figure(
    image("filter_shaper_per_device.png", width: 85%),
    gap: 0em,
    caption: [Extracted filter shape per USRP device]
  )

=== Filter calibration

#text-block(title: [Calibration process])[
  - Small range sweeps
  - Very close overlap
  - For each frequency bin, compare with PSD whose corresponding bin is closest to center frequency
]
#pause
#text-block(title: [Observations])[
  - Very similar response among same model
  - Shape resembling RC filter
  - We can extract a model from that to compensate filtering effects
]

=== Filter calibrated

#figure(
    image("filter_corrected_juxtaposed_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Combination of PSDs for all center frequencies of a full range frequency sweep, with filter compensation]
  )

=== Filter calibrated

#figure(
    image("filter_corrected_juxtaposed_zoomed_1500MHz_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Zoom of previous plot around 1.5GHz]
  )

=== Gain steps compensation
  The steps in the response are dependant on the actual device model, and can thus be compensated offline

  #figure(
    image("filter&step_corrected_juxtaposed_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Combination of PSDs for all center frequencies of a full range frequency sweep, with filter and gain step compensation]
  )

=== Fully combined PSD



  #figure(
    image("filter_corrected_merged_rx10_wideband_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Fully merged PSD, with filter and gain step compensation]
  )

=== Antenna equivalent surface

*Did you get where the drop in frequency comes from?*

#pause

Friis formula:
$ P_R = (P_T G_T G_R c²) / ((4 pi d f)²)$

#pause

Antenna equivalent surface does down with wavelength, as $lambda² = (c²)/(f²)$

In dB, that results to $40 log_10 (c/f)$. 

#pause

#text-block(title: [Compensation steps])[
  - Correct the post processed gain values with the previous coefficient
  - Increase gain above some center frequency (eg 2GHz) to ensure reception at higher frequencies
  - Remove that known gain increase in post. Just as the other gain steps
]

=== Antenna equivalent surface

  #figure(
    image("filter_corrected_merged_rx10_wideband_eq_corrected_freq_response.png", width: 85%),
    gap: 0em,
    caption: [Fully merged PSD, with compensation for antenna gain reduction]
  )

=== Super resolution time responses


  What if we could do the same with time responses to get crazy high resolutions?
#pause
  #text-block(title: [Impulse response])[
  - Needs complex valued frequency response
  - Previously shown wideband responses are PSD only (no phase)
  - Phase varies a lot #sym.arrow.r Impractical to accuratly merge
  #sym.arrow.curve Not a practical metric to extract
]
#pause

  #text-block(title: [Power Delay Profile])[
  - Only needs PSD
  #sym.arrow.curve We can derive it
]
  // First, the impulse response: It needs the complex freq response to be computed (with phase). But reception phase is not constant, and fluctuate
  // (show a plot of phase variation? Would need to first explain what that it)

  // But the PDP only uses the power information, not the phase. So if we first combine all the frequency responses into one, we could derive it.

  // For that, my choice is, for each frequency bin (that need to be slightly redefined because they don't all fall into the exact same values for each center frequency), we average the available power.
  // From that wideband response, we can extract the PDP. 
=== Super resolution PDP
  
  #figure(
    image("plot_one_file_rx10_wideband_pdp.png", width: 85%),
    gap: 0em,
    caption: [High resolution PDP]
  )

=== Super resolution PDP
  
  #figure(
    image("plot_one_file_rx10_wideband_zoomedpdp.png", width: 85%),
    gap: 0em,
    caption: [Zoomed high resolution PDP]
  )

= Conclusion

#text-block(title: [Built system])[
  - Transmission chain for channel state measurements
  - Many variable parameters for customisation
  - Designed for operation inside CorteXlab, but can be used outside
  - Python post processing suite
]

#text-block(title: [Uses])[
  - Get wideband PSD help node/frequency selection for experiments
  - Evaluate time variations with different conditions
  - Base for other channel processing applications (backscattering)
  - Other ideas?
]
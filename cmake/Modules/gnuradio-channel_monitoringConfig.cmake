find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_CHANNEL_MONITORING gnuradio-channel_monitoring)

FIND_PATH(
    GR_CHANNEL_MONITORING_INCLUDE_DIRS
    NAMES gnuradio/channel_monitoring/api.h
    HINTS $ENV{CHANNEL_MONITORING_DIR}/include
        ${PC_CHANNEL_MONITORING_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_CHANNEL_MONITORING_LIBRARIES
    NAMES gnuradio-channel_monitoring
    HINTS $ENV{CHANNEL_MONITORING_DIR}/lib
        ${PC_CHANNEL_MONITORING_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-channel_monitoringTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_CHANNEL_MONITORING DEFAULT_MSG GR_CHANNEL_MONITORING_LIBRARIES GR_CHANNEL_MONITORING_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_CHANNEL_MONITORING_LIBRARIES GR_CHANNEL_MONITORING_INCLUDE_DIRS)

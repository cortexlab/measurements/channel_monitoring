#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: OFDM Pilot TX/RX
# Author: Cyrille Morin
# GNU Radio version: 3.10.9.2

from PyQt5 import Qt
from gnuradio import qtgui
from PyQt5 import QtCore
from gnuradio import analog
from gnuradio import blocks
from gnuradio import channel_monitoring
from gnuradio import channels
from gnuradio.filter import firdes
from gnuradio import digital
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
import sys
import signal
from PyQt5 import Qt
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import gr, pdu
import numpy as np
import ofdm_pilot_txrx_epy_block_0 as epy_block_0  # embedded python block
import sip



class ofdm_pilot_txrx(gr.top_block, Qt.QWidget):

    def __init__(self, fft_len=128, freq=2500000000, gain=50, samp_rate=100000000, seed=43, tx_gain=80):
        gr.top_block.__init__(self, "OFDM Pilot TX/RX", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("OFDM Pilot TX/RX")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except BaseException as exc:
            print(f"Qt GUI: Could not set Icon: {str(exc)}", file=sys.stderr)
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "ofdm_pilot_txrx")

        try:
            geometry = self.settings.value("geometry")
            if geometry:
                self.restoreGeometry(geometry)
        except BaseException as exc:
            print(f"Qt GUI: Could not restore geometry: {str(exc)}", file=sys.stderr)

        ##################################################
        # Parameters
        ##################################################
        self.fft_len = fft_len
        self.freq = freq
        self.gain = gain
        self.samp_rate = samp_rate
        self.seed = seed
        self.tx_gain = tx_gain

        ##################################################
        # Variables
        ##################################################
        self.symbols_amount = symbols_amount = 12
        self.pilot_carriers = pilot_carriers = ((),)
        self.occupied_carriers = occupied_carriers = (list(range((-fft_len//2)+5, -20)) + list(range(-20, -6)) + list(range(-6, 0)) + list(range(0, 7)) + list(range(7, 21)) + list(range(21, (fft_len//2)-3)),)
        self.qpsk = qpsk = {0: 1+1j, 1: -1+1j, 2:1-1j, 3:-1-1j}
        self.pilot_symbols = pilot_symbols = ((),)
        self.packet_length_tag_key = packet_length_tag_key = "packet_len"
        self.length_tag_key = length_tag_key = "frame_len"
        self.header_mod = header_mod = digital.constellation_bpsk()
        self.frame_len = frame_len = symbols_amount * len(occupied_carriers[0])
        self.cp_len = cp_len = fft_len//16
        self.bpsk_sqrt = bpsk_sqrt = {0: np.sqrt(2), 1: -np.sqrt(2)}
        self.bpsk = bpsk = {0: 1, 1: -1}
        self.active_carriers = active_carriers = list(np.array(list(occupied_carriers[0]) + list(pilot_carriers[0])) % fft_len)
        self.sync_word2 = sync_word2 = np.fft.fftshift([0j] + [bpsk[np.random.randint(2)] if x in active_carriers else 0 for x in range(1, fft_len)]) if np.random.seed(42) is None else 0
        self.sync_word1 = sync_word1 = np.fft.fftshift([bpsk_sqrt[np.random.randint(2)] if x in active_carriers and x % 2 else 0 for x in range(fft_len)]) if np.random.seed(42) is None else 0
        self.sym_scramble_seq = sym_scramble_seq = (1, 1, 1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, -1, 1, 1, -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 1, 1, -1, -1, 1, 1, 1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, 1, -1, -1, 1, 1, 1, 1, 1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1,-1, -1, -1, -1, -1, 1, -1, 1, 1, -1, 1, -1, 1, 1, 1, -1, -1, 1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1)
        self.pilot_seq_rand = pilot_seq_rand = np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(occupied_carriers[0]*symbols_amount))) if np.random.seed(seed) is None else 0
        self.pilot_seq = pilot_seq = [qpsk[np.random.randint(4)] for x in range(len(occupied_carriers[0]*symbols_amount))] if np.random.seed(seed) is None else 0
        self.packet_period_ms = packet_period_ms = 10
        self.header_formatter_id = header_formatter_id = channel_monitoring.packet_header_ofdm_id( occupied_carriers, n_syms=1, len_tag_key=packet_length_tag_key, frame_len_tag_key=length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23,max_allowed_id=40, expected_len=frame_len )
        self.header_equalizer = header_equalizer = digital.ofdm_equalizer_simpledfe(fft_len,header_mod.base(),occupied_carriers,pilot_carriers,pilot_symbols)
        self.freq_offset = freq_offset = 0
        self.add_delay = add_delay = (cp_len)

        ##################################################
        # Blocks
        ##################################################

        self.tab = Qt.QTabWidget()
        self.tab_widget_0 = Qt.QWidget()
        self.tab_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_widget_0)
        self.tab_grid_layout_0 = Qt.QGridLayout()
        self.tab_layout_0.addLayout(self.tab_grid_layout_0)
        self.tab.addTab(self.tab_widget_0, 'TX')
        self.tab_widget_1 = Qt.QWidget()
        self.tab_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_widget_1)
        self.tab_grid_layout_1 = Qt.QGridLayout()
        self.tab_layout_1.addLayout(self.tab_grid_layout_1)
        self.tab.addTab(self.tab_widget_1, 'RX')
        self.tab_widget_2 = Qt.QWidget()
        self.tab_layout_2 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.tab_widget_2)
        self.tab_grid_layout_2 = Qt.QGridLayout()
        self.tab_layout_2.addLayout(self.tab_grid_layout_2)
        self.tab.addTab(self.tab_widget_2, 'Channel')
        self.top_layout.addWidget(self.tab)
        self._freq_offset_range = qtgui.Range((-20000), 20000, 1, 0, 200)
        self._freq_offset_win = qtgui.RangeWidget(self._freq_offset_range, self.set_freq_offset, "Frequency Offset", "counter_slider", int, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._freq_offset_win)
        self._add_delay_range = qtgui.Range(0, (2*cp_len), 1, (cp_len), 200)
        self._add_delay_win = qtgui.RangeWidget(self._add_delay_range, self.set_add_delay, "Sample timing offset", "counter_slider", int, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._add_delay_win)
        self.qtgui_time_sink_x_2_1 = qtgui.time_sink_c(
            1024, #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_2_1.set_update_time(0.10)
        self.qtgui_time_sink_x_2_1.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_2_1.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_2_1.enable_tags(True)
        self.qtgui_time_sink_x_2_1.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "frame_len")
        self.qtgui_time_sink_x_2_1.enable_autoscale(False)
        self.qtgui_time_sink_x_2_1.enable_grid(False)
        self.qtgui_time_sink_x_2_1.enable_axis_labels(True)
        self.qtgui_time_sink_x_2_1.enable_control_panel(False)
        self.qtgui_time_sink_x_2_1.enable_stem_plot(False)


        labels = ['Real', 'Imag', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                if (i % 2 == 0):
                    self.qtgui_time_sink_x_2_1.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_2_1.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_2_1.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_2_1.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_2_1.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_2_1.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_2_1.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_2_1.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_2_1_win = sip.wrapinstance(self.qtgui_time_sink_x_2_1.qwidget(), Qt.QWidget)
        self.tab_layout_0.addWidget(self._qtgui_time_sink_x_2_1_win)
        self.qtgui_time_sink_x_2_0_0 = qtgui.time_sink_f(
            len(occupied_carriers[0]), #size
            samp_rate, #samp_rate
            "PdP", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_2_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_2_0_0.set_y_axis(-1, 60)

        self.qtgui_time_sink_x_2_0_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_2_0_0.enable_tags(True)
        self.qtgui_time_sink_x_2_0_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "rx_time")
        self.qtgui_time_sink_x_2_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_2_0_0.enable_grid(False)
        self.qtgui_time_sink_x_2_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_2_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_2_0_0.enable_stem_plot(False)


        labels = ['Scrambled', 'Scrambled', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_2_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_2_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_2_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_2_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_2_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_2_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_2_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_2_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_2_0_0.qwidget(), Qt.QWidget)
        self.tab_layout_2.addWidget(self._qtgui_time_sink_x_2_0_0_win)
        self.qtgui_time_sink_x_2_0 = qtgui.time_sink_f(
            len(occupied_carriers[0]), #size
            samp_rate, #samp_rate
            "Recieved Header", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_2_0.set_update_time(0.10)
        self.qtgui_time_sink_x_2_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_2_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_2_0.enable_tags(True)
        self.qtgui_time_sink_x_2_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "rx_time")
        self.qtgui_time_sink_x_2_0.enable_autoscale(False)
        self.qtgui_time_sink_x_2_0.enable_grid(False)
        self.qtgui_time_sink_x_2_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_2_0.enable_control_panel(False)
        self.qtgui_time_sink_x_2_0.enable_stem_plot(False)


        labels = ['Scrambled', 'Scrambled', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_2_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_2_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_2_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_2_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_2_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_2_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_2_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_2_0_win = sip.wrapinstance(self.qtgui_time_sink_x_2_0.qwidget(), Qt.QWidget)
        self.tab_layout_2.addWidget(self._qtgui_time_sink_x_2_0_win)
        self.qtgui_time_sink_x_2 = qtgui.time_sink_f(
            1024, #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_2.set_update_time(0.10)
        self.qtgui_time_sink_x_2.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_2.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_2.enable_tags(True)
        self.qtgui_time_sink_x_2.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "frame_len")
        self.qtgui_time_sink_x_2.enable_autoscale(False)
        self.qtgui_time_sink_x_2.enable_grid(False)
        self.qtgui_time_sink_x_2.enable_axis_labels(True)
        self.qtgui_time_sink_x_2.enable_control_panel(False)
        self.qtgui_time_sink_x_2.enable_stem_plot(False)


        labels = ['Normal', 'Scrambled', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_2.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_2.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_2.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_2.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_2.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_2.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_2.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_2_win = sip.wrapinstance(self.qtgui_time_sink_x_2.qwidget(), Qt.QWidget)
        self.tab_layout_0.addWidget(self._qtgui_time_sink_x_2_win)
        self.qtgui_time_sink_x_1_0 = qtgui.time_sink_f(
            (2*(symbols_amount *len(occupied_carriers[0]))), #size
            samp_rate, #samp_rate
            "Estimated Taps", #name
            5, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_1_0.set_update_time(0.10)
        self.qtgui_time_sink_x_1_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_1_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_1_0.enable_tags(False)
        self.qtgui_time_sink_x_1_0.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "packet_len")
        self.qtgui_time_sink_x_1_0.enable_autoscale(False)
        self.qtgui_time_sink_x_1_0.enable_grid(False)
        self.qtgui_time_sink_x_1_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_1_0.enable_control_panel(False)
        self.qtgui_time_sink_x_1_0.enable_stem_plot(False)


        labels = ['Preamble Mag', 'Preamble Phase', 'Payload Mag', 'Payload Phase', 'Phase diff',
            'Phase diff', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'dark blue', 'red', 'dark red', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(5):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_1_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_1_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_1_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_1_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_1_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_1_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_1_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_1_0_win = sip.wrapinstance(self.qtgui_time_sink_x_1_0.qwidget(), Qt.QWidget)
        self.tab_layout_2.addWidget(self._qtgui_time_sink_x_1_0_win)
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_c(
            10240, #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_0.enable_tags(True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(False)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)


        labels = ['Signal 1', 'Signal 2', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                if (i % 2 == 0):
                    self.qtgui_time_sink_x_0_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.qwidget(), Qt.QWidget)
        self.tab_layout_1.addWidget(self._qtgui_time_sink_x_0_0_win)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_c(
            10240, #size
            samp_rate, #samp_rate
            "", #name
            1, #number of inputs
            None # parent
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)


        labels = ['Signal 1', 'Signal 2', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                if (i % 2 == 0):
                    self.qtgui_time_sink_x_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.qwidget(), Qt.QWidget)
        self.tab_layout_0.addWidget(self._qtgui_time_sink_x_0_win)
        self.qtgui_freq_sink_x_0_0 = qtgui.freq_sink_c(
            128, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "", #name
            1,
            None # parent
        )
        self.qtgui_freq_sink_x_0_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0_0.set_y_axis((-140), 10)
        self.qtgui_freq_sink_x_0_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0_0.enable_grid(False)
        self.qtgui_freq_sink_x_0_0.set_fft_average(1.0)
        self.qtgui_freq_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0_0.enable_control_panel(False)
        self.qtgui_freq_sink_x_0_0.set_fft_window_normalized(False)



        labels = ['', '', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
            "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0_0.qwidget(), Qt.QWidget)
        self.tab_layout_1.addWidget(self._qtgui_freq_sink_x_0_0_win)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
            128, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "", #name
            1,
            None # parent
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0.set_y_axis((-140), 10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(1.0)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)
        self.qtgui_freq_sink_x_0.set_fft_window_normalized(False)



        labels = ['', '', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
            "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.qwidget(), Qt.QWidget)
        self.tab_layout_0.addWidget(self._qtgui_freq_sink_x_0_win)
        self.pdu_pdu_to_tagged_stream_0 = pdu.pdu_to_tagged_stream(gr.types.complex_t, length_tag_key)
        self.pdu_pdu_to_tagged_stream_0.set_min_output_buffer((2*frame_len))
        self.fft_vxx_2 = fft.fft_vcc(len(occupied_carriers[0]), False, (), True, 1)
        self.fft_vxx_1 = fft.fft_vcc(fft_len, True, (), True, 1)
        self.fft_vxx_1.set_min_output_buffer((2*(symbols_amount)))
        self.fft_vxx_0_0 = fft.fft_vcc(fft_len, True, (), True, 1)
        self.fft_vxx_0 = fft.fft_vcc(fft_len, False, (), True, 1)
        self.fft_vxx_0.set_min_output_buffer((2*(symbols_amount+3)))
        self.epy_block_0 = epy_block_0.blk(occupied_carriers=occupied_carriers, fft_len=fft_len)
        self.digital_packet_headerparser_b_0 = digital.packet_headerparser_b(header_formatter_id)
        self.digital_packet_headergenerator_bb_0 = digital.packet_headergenerator_bb(header_formatter_id, length_tag_key)
        self.digital_ofdm_sync_sc_cfb_0 = digital.ofdm_sync_sc_cfb(fft_len, cp_len, False, 0.9)
        self.digital_ofdm_serializer_vcc_payload = digital.ofdm_serializer_vcc(fft_len, occupied_carriers, length_tag_key, packet_length_tag_key, 0, 'ofdm_sync_carr_offset', True)
        self.digital_ofdm_serializer_vcc_payload.set_min_output_buffer((2*frame_len))
        self.digital_ofdm_serializer_vcc_header = digital.ofdm_serializer_vcc(fft_len, occupied_carriers, "header_len", '', 0, 'ofdm_sync_carr_offset', True)
        self.digital_ofdm_frame_equalizer_vcvc_0 = digital.ofdm_frame_equalizer_vcvc(header_equalizer.base(), cp_len, "header_len", True, 1)
        self.digital_ofdm_cyclic_prefixer_0 = digital.ofdm_cyclic_prefixer(
            fft_len,
            fft_len + cp_len,
            0,
            length_tag_key)
        self.digital_ofdm_cyclic_prefixer_0.set_min_output_buffer((2*(symbols_amount+3) * (fft_len+cp_len)))
        self.digital_ofdm_chanest_vcvc_0 = digital.ofdm_chanest_vcvc(sync_word1, sync_word2, 1, 0, 3, False)
        self.digital_ofdm_carrier_allocator_cvc_0 = digital.ofdm_carrier_allocator_cvc( fft_len, occupied_carriers, pilot_carriers, pilot_symbols, (sync_word1, sync_word2), length_tag_key, True)
        self.digital_ofdm_carrier_allocator_cvc_0.set_min_output_buffer((2*(symbols_amount+3) * fft_len))
        self.digital_header_payload_demux_0 = digital.header_payload_demux(
            3,
            fft_len,
            cp_len,
            length_tag_key,
            "",
            True,
            gr.sizeof_gr_complex,
            "rx_time",
            int(samp_rate),
            (),
            0)
        self.digital_header_payload_demux_0.set_min_output_buffer((2*(symbols_amount)))
        self.digital_diff_phasor_cc_0 = digital.diff_phasor_cc()
        self.digital_constellation_decoder_cb_0 = digital.constellation_decoder_cb(header_mod.base())
        self.digital_chunks_to_symbols_xx_0 = digital.chunks_to_symbols_bc(header_mod.points(), 1)
        self.channels_channel_model_0 = channels.channel_model(
            noise_voltage=0.0,
            frequency_offset=(freq_offset/samp_rate),
            epsilon=1.0,
            taps=[1.0],
            noise_seed=0,
            block_tags=False)
        self.channel_monitoring_strobe_random_seq_0 = channel_monitoring.strobe_random_seq(frame_len, packet_period_ms, 4096)
        self.channel_monitoring_freq_offset_tagger_0 = channel_monitoring.freq_offset_tagger((samp_rate/fft_len) / np.pi, 'freq_frac_offset')
        self.channel_monitoring_divide_random_seq_0 = channel_monitoring.divide_random_seq(packet_length_tag_key, 'packet_num')
        self.blocks_vector_to_stream_1 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, len(occupied_carriers[0]))
        self.blocks_tagged_stream_mux_0_1 = blocks.tagged_stream_mux(gr.sizeof_gr_complex*1, length_tag_key, 0)
        self.blocks_tagged_stream_mux_0_0 = blocks.tagged_stream_mux(gr.sizeof_gr_complex*1, length_tag_key, 0)
        self.blocks_tagged_stream_mux_0_0.set_min_output_buffer((2*(symbols_amount+1) * len(occupied_carriers[0])))
        self.blocks_tag_gate_0 = blocks.tag_gate(gr.sizeof_gr_complex * 1, False)
        self.blocks_tag_gate_0.set_single_key("")
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, len(occupied_carriers[0]))
        self.blocks_stream_to_tagged_stream_0_0 = blocks.stream_to_tagged_stream(gr.sizeof_gr_complex, 1, (2*fft_len), length_tag_key)
        self.blocks_repack_bits_bb_0_0_0_0 = blocks.repack_bits_bb(1, 1, length_tag_key, False, gr.GR_LSB_FIRST)
        self.blocks_null_source_0_2 = blocks.null_source(gr.sizeof_gr_complex*1)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_const_xx_0 = blocks.multiply_const_cc(0.01, 1)
        self.blocks_multiply_conjugate_cc_0 = blocks.multiply_conjugate_cc(1)
        self.blocks_float_to_char_0 = blocks.float_to_char(1, 1)
        self.blocks_delay_0 = blocks.delay(gr.sizeof_gr_complex*1, (fft_len+cp_len*0+add_delay))
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)
        self.blocks_complex_to_magphase_0_0 = blocks.complex_to_magphase(1)
        self.blocks_complex_to_magphase_0 = blocks.complex_to_magphase(1)
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(1)
        self.blocks_complex_to_arg_0 = blocks.complex_to_arg(1)
        self.blocks_char_to_float_0_0_0 = blocks.char_to_float(1, 1)
        self.blocks_char_to_float_0_0 = blocks.char_to_float(1, 1)
        self.analog_frequency_modulator_fc_0 = analog.frequency_modulator_fc((-2.0/fft_len))


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.channel_monitoring_strobe_random_seq_0, 'strobe'), (self.pdu_pdu_to_tagged_stream_0, 'pdus'))
        self.msg_connect((self.digital_packet_headerparser_b_0, 'header_data'), (self.digital_header_payload_demux_0, 'header_data'))
        self.connect((self.analog_frequency_modulator_fc_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_char_to_float_0_0, 0), (self.qtgui_time_sink_x_2, 0))
        self.connect((self.blocks_char_to_float_0_0_0, 0), (self.qtgui_time_sink_x_2_0, 0))
        self.connect((self.blocks_complex_to_arg_0, 0), (self.qtgui_time_sink_x_1_0, 4))
        self.connect((self.blocks_complex_to_mag_squared_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_complex_to_magphase_0, 1), (self.qtgui_time_sink_x_1_0, 3))
        self.connect((self.blocks_complex_to_magphase_0, 0), (self.qtgui_time_sink_x_1_0, 2))
        self.connect((self.blocks_complex_to_magphase_0_0, 0), (self.qtgui_time_sink_x_1_0, 0))
        self.connect((self.blocks_complex_to_magphase_0_0, 1), (self.qtgui_time_sink_x_1_0, 1))
        self.connect((self.blocks_complex_to_real_0, 0), (self.blocks_float_to_char_0, 0))
        self.connect((self.blocks_delay_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_float_to_char_0, 0), (self.digital_packet_headergenerator_bb_0, 0))
        self.connect((self.blocks_multiply_conjugate_cc_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_multiply_const_xx_0, 0), (self.blocks_tagged_stream_mux_0_1, 1))
        self.connect((self.blocks_multiply_const_xx_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.blocks_multiply_const_xx_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.digital_header_payload_demux_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qtgui_time_sink_x_2_0_0, 0))
        self.connect((self.blocks_null_source_0_2, 0), (self.blocks_stream_to_tagged_stream_0_0, 0))
        self.connect((self.blocks_repack_bits_bb_0_0_0_0, 0), (self.blocks_char_to_float_0_0, 0))
        self.connect((self.blocks_repack_bits_bb_0_0_0_0, 0), (self.digital_chunks_to_symbols_xx_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0_0, 0), (self.blocks_tagged_stream_mux_0_1, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_2, 0))
        self.connect((self.blocks_tag_gate_0, 0), (self.blocks_delay_0, 0))
        self.connect((self.blocks_tag_gate_0, 0), (self.digital_ofdm_sync_sc_cfb_0, 0))
        self.connect((self.blocks_tag_gate_0, 0), (self.qtgui_freq_sink_x_0_0, 0))
        self.connect((self.blocks_tag_gate_0, 0), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0_0, 0), (self.digital_ofdm_carrier_allocator_cvc_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0_0, 0), (self.qtgui_time_sink_x_2_1, 0))
        self.connect((self.blocks_tagged_stream_mux_0_1, 0), (self.channels_channel_model_0, 0))
        self.connect((self.blocks_vector_to_stream_1, 0), (self.blocks_complex_to_mag_squared_0, 0))
        self.connect((self.channel_monitoring_divide_random_seq_0, 0), (self.blocks_complex_to_magphase_0, 0))
        self.connect((self.channel_monitoring_divide_random_seq_0, 0), (self.blocks_multiply_conjugate_cc_0, 0))
        self.connect((self.channel_monitoring_divide_random_seq_0, 0), (self.blocks_multiply_conjugate_cc_0, 1))
        self.connect((self.channel_monitoring_divide_random_seq_0, 0), (self.digital_diff_phasor_cc_0, 0))
        self.connect((self.channel_monitoring_divide_random_seq_0, 0), (self.epy_block_0, 0))
        self.connect((self.channel_monitoring_freq_offset_tagger_0, 0), (self.analog_frequency_modulator_fc_0, 0))
        self.connect((self.channels_channel_model_0, 0), (self.blocks_tag_gate_0, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0, 0), (self.blocks_tagged_stream_mux_0_0, 0))
        self.connect((self.digital_constellation_decoder_cb_0, 0), (self.blocks_char_to_float_0_0_0, 0))
        self.connect((self.digital_constellation_decoder_cb_0, 0), (self.digital_packet_headerparser_b_0, 0))
        self.connect((self.digital_diff_phasor_cc_0, 0), (self.blocks_complex_to_arg_0, 0))
        self.connect((self.digital_header_payload_demux_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.digital_header_payload_demux_0, 1), (self.fft_vxx_1, 0))
        self.connect((self.digital_ofdm_carrier_allocator_cvc_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.digital_ofdm_chanest_vcvc_0, 0), (self.digital_ofdm_frame_equalizer_vcvc_0, 0))
        self.connect((self.digital_ofdm_cyclic_prefixer_0, 0), (self.blocks_multiply_const_xx_0, 0))
        self.connect((self.digital_ofdm_frame_equalizer_vcvc_0, 0), (self.digital_ofdm_serializer_vcc_header, 0))
        self.connect((self.digital_ofdm_serializer_vcc_header, 0), (self.digital_constellation_decoder_cb_0, 0))
        self.connect((self.digital_ofdm_serializer_vcc_payload, 0), (self.channel_monitoring_divide_random_seq_0, 0))
        self.connect((self.digital_ofdm_sync_sc_cfb_0, 0), (self.channel_monitoring_freq_offset_tagger_0, 0))
        self.connect((self.digital_ofdm_sync_sc_cfb_0, 1), (self.channel_monitoring_freq_offset_tagger_0, 1))
        self.connect((self.digital_ofdm_sync_sc_cfb_0, 1), (self.digital_header_payload_demux_0, 1))
        self.connect((self.digital_packet_headergenerator_bb_0, 0), (self.blocks_repack_bits_bb_0_0_0_0, 0))
        self.connect((self.epy_block_0, 0), (self.blocks_complex_to_magphase_0_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.digital_ofdm_cyclic_prefixer_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.digital_ofdm_chanest_vcvc_0, 0))
        self.connect((self.fft_vxx_1, 0), (self.digital_ofdm_serializer_vcc_payload, 0))
        self.connect((self.fft_vxx_2, 0), (self.blocks_vector_to_stream_1, 0))
        self.connect((self.pdu_pdu_to_tagged_stream_0, 0), (self.blocks_complex_to_real_0, 0))
        self.connect((self.pdu_pdu_to_tagged_stream_0, 0), (self.blocks_tagged_stream_mux_0_0, 1))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "ofdm_pilot_txrx")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_fft_len(self):
        return self.fft_len

    def set_fft_len(self, fft_len):
        self.fft_len = fft_len
        self.set_active_carriers(list(np.array(list(self.occupied_carriers[0]) + list(self.pilot_carriers[0])) % self.fft_len))
        self.set_cp_len(self.fft_len//16)
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))
        self.set_occupied_carriers((list(range((-self.fft_len//2)+5, -20)) + list(range(-20, -6)) + list(range(-6, 0)) + list(range(0, 7)) + list(range(7, 21)) + list(range(21, (self.fft_len//2)-3)),))
        self.set_sync_word1(np.fft.fftshift([self.bpsk_sqrt[np.random.randint(2)] if x in self.active_carriers and x % 2 else 0 for x in range(self.fft_len)]) if np.random.seed(42) is None else 0)
        self.set_sync_word2(np.fft.fftshift([0j] + [self.bpsk[np.random.randint(2)] if x in self.active_carriers else 0 for x in range(1, self.fft_len)]) if np.random.seed(42) is None else 0)
        self.analog_frequency_modulator_fc_0.set_sensitivity((-2.0/self.fft_len))
        self.blocks_delay_0.set_dly(int((self.fft_len+self.cp_len*0+self.add_delay)))
        self.blocks_stream_to_tagged_stream_0_0.set_packet_len((2*self.fft_len))
        self.blocks_stream_to_tagged_stream_0_0.set_packet_len_pmt((2*self.fft_len))
        self.epy_block_0.fft_len = self.fft_len

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle2_0.set_sample_rate(self.samp_rate)
        self.channels_channel_model_0.set_frequency_offset((self.freq_offset/self.samp_rate))
        self.qtgui_freq_sink_x_0.set_frequency_range(0, self.samp_rate)
        self.qtgui_freq_sink_x_0_0.set_frequency_range(0, self.samp_rate)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_1_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_2.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_2_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_2_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_time_sink_x_2_1.set_samp_rate(self.samp_rate)

    def get_seed(self):
        return self.seed

    def set_seed(self, seed):
        self.seed = seed
        self.set_pilot_seq([self.qpsk[np.random.randint(4)] for x in range(len(self.occupied_carriers[0]*self.symbols_amount))] if np.random.seed(self.seed) is None else 0)
        self.set_pilot_seq_rand(np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(self.occupied_carriers[0]*self.symbols_amount))) if np.random.seed(self.seed) is None else 0)

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain

    def get_symbols_amount(self):
        return self.symbols_amount

    def set_symbols_amount(self, symbols_amount):
        self.symbols_amount = symbols_amount
        self.set_frame_len(self.symbols_amount * len(self.occupied_carriers[0]))
        self.set_pilot_seq([self.qpsk[np.random.randint(4)] for x in range(len(self.occupied_carriers[0]*self.symbols_amount))] if np.random.seed(self.seed) is None else 0)
        self.set_pilot_seq_rand(np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(self.occupied_carriers[0]*self.symbols_amount))) if np.random.seed(self.seed) is None else 0)

    def get_pilot_carriers(self):
        return self.pilot_carriers

    def set_pilot_carriers(self, pilot_carriers):
        self.pilot_carriers = pilot_carriers
        self.set_active_carriers(list(np.array(list(self.occupied_carriers[0]) + list(self.pilot_carriers[0])) % self.fft_len))
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))

    def get_occupied_carriers(self):
        return self.occupied_carriers

    def set_occupied_carriers(self, occupied_carriers):
        self.occupied_carriers = occupied_carriers
        self.set_active_carriers(list(np.array(list(self.occupied_carriers[0]) + list(self.pilot_carriers[0])) % self.fft_len))
        self.set_frame_len(self.symbols_amount * len(self.occupied_carriers[0]))
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23,max_allowed_id=40, expected_len=self.frame_len ))
        self.set_pilot_seq([self.qpsk[np.random.randint(4)] for x in range(len(self.occupied_carriers[0]*self.symbols_amount))] if np.random.seed(self.seed) is None else 0)
        self.set_pilot_seq_rand(np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(self.occupied_carriers[0]*self.symbols_amount))) if np.random.seed(self.seed) is None else 0)
        self.epy_block_0.occupied_carriers = self.occupied_carriers

    def get_qpsk(self):
        return self.qpsk

    def set_qpsk(self, qpsk):
        self.qpsk = qpsk
        self.set_pilot_seq([self.qpsk[np.random.randint(4)] for x in range(len(self.occupied_carriers[0]*self.symbols_amount))] if np.random.seed(self.seed) is None else 0)

    def get_pilot_symbols(self):
        return self.pilot_symbols

    def set_pilot_symbols(self, pilot_symbols):
        self.pilot_symbols = pilot_symbols
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))

    def get_packet_length_tag_key(self):
        return self.packet_length_tag_key

    def set_packet_length_tag_key(self, packet_length_tag_key):
        self.packet_length_tag_key = packet_length_tag_key
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23,max_allowed_id=40, expected_len=self.frame_len ))

    def get_length_tag_key(self):
        return self.length_tag_key

    def set_length_tag_key(self, length_tag_key):
        self.length_tag_key = length_tag_key
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23,max_allowed_id=40, expected_len=self.frame_len ))

    def get_header_mod(self):
        return self.header_mod

    def set_header_mod(self, header_mod):
        self.header_mod = header_mod

    def get_frame_len(self):
        return self.frame_len

    def set_frame_len(self, frame_len):
        self.frame_len = frame_len
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23,max_allowed_id=40, expected_len=self.frame_len ))

    def get_cp_len(self):
        return self.cp_len

    def set_cp_len(self, cp_len):
        self.cp_len = cp_len
        self.set_add_delay((self.cp_len))
        self.blocks_delay_0.set_dly(int((self.fft_len+self.cp_len*0+self.add_delay)))

    def get_bpsk_sqrt(self):
        return self.bpsk_sqrt

    def set_bpsk_sqrt(self, bpsk_sqrt):
        self.bpsk_sqrt = bpsk_sqrt
        self.set_sync_word1(np.fft.fftshift([self.bpsk_sqrt[np.random.randint(2)] if x in self.active_carriers and x % 2 else 0 for x in range(self.fft_len)]) if np.random.seed(42) is None else 0)

    def get_bpsk(self):
        return self.bpsk

    def set_bpsk(self, bpsk):
        self.bpsk = bpsk
        self.set_sync_word2(np.fft.fftshift([0j] + [self.bpsk[np.random.randint(2)] if x in self.active_carriers else 0 for x in range(1, self.fft_len)]) if np.random.seed(42) is None else 0)

    def get_active_carriers(self):
        return self.active_carriers

    def set_active_carriers(self, active_carriers):
        self.active_carriers = active_carriers
        self.set_sync_word1(np.fft.fftshift([self.bpsk_sqrt[np.random.randint(2)] if x in self.active_carriers and x % 2 else 0 for x in range(self.fft_len)]) if np.random.seed(42) is None else 0)
        self.set_sync_word2(np.fft.fftshift([0j] + [self.bpsk[np.random.randint(2)] if x in self.active_carriers else 0 for x in range(1, self.fft_len)]) if np.random.seed(42) is None else 0)

    def get_sync_word2(self):
        return self.sync_word2

    def set_sync_word2(self, sync_word2):
        self.sync_word2 = sync_word2

    def get_sync_word1(self):
        return self.sync_word1

    def set_sync_word1(self, sync_word1):
        self.sync_word1 = sync_word1

    def get_sym_scramble_seq(self):
        return self.sym_scramble_seq

    def set_sym_scramble_seq(self, sym_scramble_seq):
        self.sym_scramble_seq = sym_scramble_seq

    def get_pilot_seq_rand(self):
        return self.pilot_seq_rand

    def set_pilot_seq_rand(self, pilot_seq_rand):
        self.pilot_seq_rand = pilot_seq_rand

    def get_pilot_seq(self):
        return self.pilot_seq

    def set_pilot_seq(self, pilot_seq):
        self.pilot_seq = pilot_seq

    def get_packet_period_ms(self):
        return self.packet_period_ms

    def set_packet_period_ms(self, packet_period_ms):
        self.packet_period_ms = packet_period_ms

    def get_header_formatter_id(self):
        return self.header_formatter_id

    def set_header_formatter_id(self, header_formatter_id):
        self.header_formatter_id = header_formatter_id
        self.digital_packet_headergenerator_bb_0.set_header_formatter(self.header_formatter_id)

    def get_header_equalizer(self):
        return self.header_equalizer

    def set_header_equalizer(self, header_equalizer):
        self.header_equalizer = header_equalizer

    def get_freq_offset(self):
        return self.freq_offset

    def set_freq_offset(self, freq_offset):
        self.freq_offset = freq_offset
        self.channels_channel_model_0.set_frequency_offset((self.freq_offset/self.samp_rate))

    def get_add_delay(self):
        return self.add_delay

    def set_add_delay(self, add_delay):
        self.add_delay = add_delay
        self.blocks_delay_0.set_dly(int((self.fft_len+self.cp_len*0+self.add_delay)))



def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-l", "--fft-len", dest="fft_len", type=intx, default=128,
        help="Set FFT Length [default=%(default)r]")
    parser.add_argument(
        "-f", "--freq", dest="freq", type=eng_float, default=eng_notation.num_to_str(float(2500000000)),
        help="Set Center frequency [default=%(default)r]")
    parser.add_argument(
        "-g", "--gain", dest="gain", type=eng_float, default=eng_notation.num_to_str(float(50)),
        help="Set RX Gain [default=%(default)r]")
    parser.add_argument(
        "-r", "--samp-rate", dest="samp_rate", type=intx, default=100000000,
        help="Set Sample rate [default=%(default)r]")
    parser.add_argument(
        "--seed", dest="seed", type=intx, default=43,
        help="Set Pilot Sequence Seed [default=%(default)r]")
    parser.add_argument(
        "-t", "--tx-gain", dest="tx_gain", type=eng_float, default=eng_notation.num_to_str(float(80)),
        help="Set TX Gain [default=%(default)r]")
    return parser


def main(top_block_cls=ofdm_pilot_txrx, options=None):
    if options is None:
        options = argument_parser().parse_args()

    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(fft_len=options.fft_len, freq=options.freq, gain=options.gain, samp_rate=options.samp_rate, seed=options.seed, tx_gain=options.tx_gain)

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()

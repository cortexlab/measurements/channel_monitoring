import os
import glob
from argparse import ArgumentParser
import scipy

# from functools import partial
import numpy as np

# from tqdm.auto import tqdm
import utils.load
import utils.math_proc
import utils.plot
import utils.filter_comp
import utils.hardware_info
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import seaborn as sns

sns.set_theme(style="whitegrid")

def merge_filter_estimates(diff_list: list[np.ndarray[float]], rx_list:list[float], bins_list: list[np.ndarray[float]]):
    diff_2944 = []
    diff_2932 = []

    plt.figure(figsize=(10, 5))
    for i, elem in enumerate(diff_list):
        rx_type = utils.hardware_info.NODE_TYPES[rx_list[i]]
        plt.plot(bins_list[0], elem, label=f"Rx{rx_list[i]} ({rx_type})")
        if rx_type == "N2944":
            diff_2944.append(elem)
        if rx_type == "N2932":
            diff_2932.append(elem)

    plt.legend(ncols=2)
    plt.grid("both")
    plt.xlabel("Normalised frequency")
    plt.ylabel("Power (dBFS)")
    plt.ylim(-6,2)
    plt.title(f"Averaged estimated filter shapes for recievers")
    plt.tight_layout()

    
    plt.figure(figsize=(10, 5))
    
    if len(diff_2944) > 0:
        avg_2944 = np.mean(np.array(diff_2944), axis=0)
        plt.plot(bins_list[0], avg_2944, label = "USRP N2944")
        popt, _ = scipy.optimize.curve_fit(utils.filter_comp.error_func, bins_list[0], avg_2944)
        plt.plot(bins_list[0], utils.filter_comp.error_func(bins_list[0], *popt), label = "USRP N2944, Fitted")
        popt, _ = scipy.optimize.curve_fit(utils.filter_comp.error_func_test, bins_list[0], avg_2944)
        plt.plot(bins_list[0], utils.filter_comp.error_func_test(bins_list[0], *popt), label = "USRP N2944, Fitted (Test)")
        popt, _ = scipy.optimize.curve_fit(utils.filter_comp.rc_error_func, bins_list[0], avg_2944, [0.25])
        plt.plot(bins_list[0], utils.filter_comp.rc_error_func(bins_list[0], *popt), label = "USRP N2944, Fitted (RC)")
        plt.plot(bins_list[0], avg_2944-utils.filter_comp.rc_error_func(bins_list[0], *popt), label = "USRP N2944, Corrected (RC)")
        print(f"2944 fitted RollOff parameter: {popt}")
    if len(diff_2932) > 0:
        avg_2932 = np.mean(np.array(diff_2932), axis=0)
        plt.plot(bins_list[0], avg_2932, label = "USRP N2932")
        popt, _ = scipy.optimize.curve_fit(utils.filter_comp.error_func, bins_list[0], avg_2932)
        plt.plot(bins_list[0], utils.filter_comp.error_func(bins_list[0], *popt), label = "USRP N2932, Fitted")
        popt, _ = scipy.optimize.curve_fit(utils.filter_comp.rc_error_func, bins_list[0], avg_2932, [0.25])
        plt.plot(bins_list[0], utils.filter_comp.rc_error_func(bins_list[0], *popt), label = "USRP N2932, Fitted (RC)")
        plt.plot(bins_list[0], avg_2932-utils.filter_comp.rc_error_func(bins_list[0], *popt), label = "USRP N2932, Corrected (RC)")
        print(f"2932 fitted RollOff parameter: {popt}")
        popt, _ = scipy.optimize.curve_fit(utils.filter_comp.error_func_test, bins_list[0], avg_2932)
        plt.plot(bins_list[0], utils.filter_comp.error_func_test(bins_list[0], *popt), label = "USRP N2932, Fitted (Test)")
    plt.legend(ncols=2)
    
    plt.grid("both")
    plt.xlabel("Normalised frequency")
    plt.ylabel("Power (dBFS)")
    plt.ylim(-6,2)
    plt.title(f"Averaged estimated filter shapes for USRP Types")
    plt.tight_layout()

    # Record avg_2944 and avg_2932 to file
    np.savetxt("utils/full_filter_25m_2944.txt", avg_2944)
    np.savetxt("utils/full_filter_25m_2932.txt", avg_2932)

def estimate_filter_unique(
    file_path: str,
    req_rx_id: int = -1,
    req_tx_id: int = -1,
):
    """
    Estimates filter shape for a reciever from one frequency sweep

    The estimation process requires that there is some overlap in the measured bands,
    as much as possible, and expects that filter effect is lowest around center frequency.
    It goes as the following:

    Average the frames and symbols for each band

    For each measured band, compare the corresponding bins from its neighbours.
    For each of these bins, we get an error with the reference, that should be in the shape of the filter
    Merge (average) all of these errors to get a smoothened filter shape
    (Optionnal?) Fit a polynomial function to that shape for more smoothing and smaller storage


    Parameters
    ----------
    file_path : str
        Data file
    req_rx_id : int, optional
        Desired reciever id, by default -1 for anything
    req_tx_id : int, optional
        Desired transmitter id, by default -1 for anything
            
    Returns
    -------
    np.ndarray[float]
        Averaged filter response, None if not found
    np.ndarray[float]
        List of freqzuency bins of the filter response (normalised), None if not found
    int
        Reciever Id. None if not found
    """
    loaded_list, sample_rate = utils.load.load_files(
        file_paths=file_path,
        req_tx_id=req_tx_id,
        req_rx_id=req_rx_id,
        req_fc=-1,
    )

    if (
        (loaded_list is None)
        or (not isinstance(loaded_list, list))
        or (len(loaded_list) == 0)
    ):
        print(f"Nothing was loaded from {file_path}")
        return None, None, None

    rx_id = loaded_list[0][3]

    averages_list = []
    for i, elem in enumerate(loaded_list):
        avg_pow = utils.math_proc.compute_avg_power(elem[2], compute_sdt=False)
        averages_list.append(avg_pow)

    # Merge frequency measurements
    # Use the first center frequency as reference point to draw regular bins
    ref_common_freq = loaded_list[0][0]
    # Get bin width from bin array of first Fc (to avoid needing to forward fft_len)
    bin_width = loaded_list[0][1][1] - loaded_list[0][1][0]

    # Create an array of frequency bins to represent all the data points we have
    corrected_bin_list = []
    for i, elem in enumerate(loaded_list):
        corresp_bin_array = elem[1] + elem[0] - ref_common_freq
        corresp_bin_array = np.round(corresp_bin_array / bin_width) * bin_width
        corrected_bin_list.append(corresp_bin_array)
    corrected_bin_array = np.concatenate(corrected_bin_list, axis=0)
    corrected_bin_array = np.unique(corrected_bin_array)

    closest_dist_array = np.zeros_like(corrected_bin_array) + np.inf
    merged_array = np.zeros_like(corrected_bin_array)

    # # Fill in the best data points we have (closest to a center frequency)
    # for i, elem in enumerate(loaded_list):
    #     corresp_first_bin = elem[1][0] + elem[0] - ref_common_freq
    #     corresp_first_bin = round(corresp_first_bin / bin_width) * bin_width
    #     start_corrected_index = np.nonzero(corrected_bin_array == corresp_first_bin)[0][
    #         0
    #     ]

    #     # (Try to) correct gain differences between RF chains at various frequencies
    #     # For an even better correction, one would need to perform actual calibration
    #     gain_correction = utils.hardware_info.get_gain_correction(
    #         req_tx_id, rx_id, elem[0]
    #     )

    #     start_id = start_corrected_index
    #     stop_id = start_corrected_index + len(averages_list[i])
    #     merged_array[start_id:stop_id] = np.where(
    #         closest_dist_array[start_id:stop_id] > np.abs(elem[1]),
    #         (averages_list[i] + gain_correction),
    #         merged_array[start_id:stop_id],
    #     )
    #     closest_dist_array[start_id:stop_id] = np.minimum(
    #         closest_dist_array[start_id:stop_id],
    #         np.abs(elem[1]),
    #     )
    
    # Fill in and average the data points we have
    averaging_window = utils.math_proc.raised_cosine(loaded_list[0][1]/sample_rate, 0.25)**10
    num_of_corresp_values = np.zeros_like(corrected_bin_array)
    merged_array = np.zeros_like(corrected_bin_array)
    for i, elem in enumerate(loaded_list):
        corresp_first_bin = elem[1][0] + elem[0] - ref_common_freq
        corresp_first_bin = round(corresp_first_bin / bin_width) * bin_width
        start_corrected_index = np.nonzero(corrected_bin_array == corresp_first_bin)[0][
            0
        ]

        # (Try to) correct gain differences between RF chains at various frequencies
        # For an even better correction, one would need to perform actual calibration
        gain_correction = utils.hardware_info.get_gain_correction(
            req_tx_id, rx_id, elem[0]
        )
        merged_array[
            start_corrected_index : start_corrected_index + len(averages_list[i])
        ] += (averages_list[i] + gain_correction)*averaging_window
        num_of_corresp_values[
            start_corrected_index : start_corrected_index + len(averages_list[i])
        ] += averaging_window
    merged_array /= num_of_corresp_values

    # Use this merged array to extract the differences
    diff_list = []
    for i, elem in enumerate(loaded_list):
        corresp_first_bin = elem[1][0] + elem[0] - ref_common_freq
        corresp_first_bin = round(corresp_first_bin / bin_width) * bin_width
        start_corrected_index = np.nonzero(corrected_bin_array == corresp_first_bin)[0][
            0
        ]
        # (Try to) correct gain differences between RF chains at various frequencies
        # For an even better correction, one would need to perform actual calibration
        gain_correction = utils.hardware_info.get_gain_correction(
            req_tx_id, rx_id, elem[0]
        )
        start_id = start_corrected_index
        stop_id = start_corrected_index + len(averages_list[i])
        diff = (averages_list[i] + gain_correction) - merged_array[start_id:stop_id]
        diff_list.append(diff)

    mean_diff = np.mean(np.array(diff_list), axis=0)
    std_diff = np.std(np.array(diff_list), axis=0)

    formatter = EngFormatter(unit="", sep=" ")
    plt.figure(figsize=(10, 5))
    for i, elem in enumerate(diff_list):
        plt.plot(loaded_list[i][1], elem, label=f"Fc={loaded_list[i][0]}")
    ax = plt.gca()
    ax.xaxis.set_major_formatter(formatter)
    plt.grid("both")
    plt.xlabel("Freq bin (Hz)")
    plt.ylabel("Power (dBFS)")
    plt.title(f"Estimated filter shape for Tx: {req_tx_id}, Rx:{rx_id}, {formatter.format_data(sample_rate)}Sps")
    plt.tight_layout()
    plt.close()

    plt.figure(figsize=(10, 5))
    plt.plot(loaded_list[0][1], mean_diff, label = "Averaged")
    plt.fill_between(loaded_list[0][1], mean_diff - std_diff, mean_diff + std_diff, alpha=0.33)
    ax = plt.gca()
    ax.xaxis.set_major_formatter(formatter)
    plt.grid("both")
    plt.xlabel("Freq bin (Hz)")
    plt.ylabel("Power (dBFS)")
    plt.ylim(-6,2)
    plt.title(f"Averaged estimated filter shape for Tx: {req_tx_id}, Rx:{rx_id}, {formatter.format_data(sample_rate)}Sps")
    plt.tight_layout()
    plt.close()

    relative_bins = loaded_list[0][1] / sample_rate

    return mean_diff, relative_bins, rx_id

def argument_parser():
    """
    Creates an argument parser and returns it
    """
    parser = ArgumentParser()
    parser.add_argument(
        "--file-glob",
        dest="file_glob",
        type=str,
        default=".*.sigmf-meta",
        help="Regex pointing to sigmf files to read [default=%(default)r]",
    )
    parser.add_argument(
        "--rx-id",
        dest="rx_id",
        type=int,
        default=-1,
        help="Reciever Id to plot from [default=%(default)r]",
    )
    parser.add_argument(
        "--tx-id",
        dest="tx_id",
        type=int,
        default=-1,
        help="Transmitter Id to plot from [default=%(default)r]",
    )

    return parser


def main(options=None):
    if options is None:
        options = argument_parser().parse_args()

    filenames = glob.glob(options.file_glob, recursive=True)
    print(f"Recieved a file glob (regex) as: {options.file_glob}")
    print(f"Will process the following files: {filenames}")

    diff_list = []
    bins_list = []
    rx_list = []
    for filename in filenames:
        mean_diff, bins, rx_id = estimate_filter_unique(
            [filename],
            options.rx_id,
            options.tx_id,
        )
        if mean_diff is not None:
            diff_list.append(mean_diff)
            bins_list.append(bins)
            rx_list.append(rx_id)

    merge_filter_estimates(diff_list=diff_list, rx_list=rx_list, bins_list=bins_list)
    plt.show()


if __name__ == "__main__":
    main()

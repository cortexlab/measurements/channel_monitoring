import os
import glob
import scipy
from argparse import ArgumentParser

# from functools import partial
import numpy as np

# from tqdm.auto import tqdm
import utils.load
import utils.math_proc
import utils.plot
import utils.filter_comp
import utils.hardware_info
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme(style="whitegrid")
from matplotlib.ticker import EngFormatter


def plot_one_freq(
    loaded_element: list[
        float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]
    ],
    tx_id: int,
    sample_rate: float,
    save_path: str,
    save_format: str = "png",
) -> None:
    """
    Create, save and displays a list of plots for a unique center frequency:
    Frequency response (in Power and phase variation)
    PDP
    Power per frame over time
    PSD evolution over time (MSE compared to average PSD)
    Frequency offsets over time

    Parameters
    ----------
    loaded_element : list[ list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]] ]
        Data element
    tx_id : int
        Index of transmitter (for display purposes)
    sample_rate : float
        Sample rate of captured data, used to compute time resolution
    save_path : str
        Save path for figures. None to not save
    save_format : str, optional
        File format for saved figures, by default "png"
    """

    # Compute and plot full average and std dev for channel response in power
    avg_pow, std_pow = utils.math_proc.compute_avg_power(
        loaded_element[2], compute_sdt=True
    )

    subc_frequencies = loaded_element[1] + loaded_element[0]
    utils.plot.plot_avg_freq(
        avg_data=avg_pow,
        std_data=std_pow,
        freqs=subc_frequencies,
        title=f"Channel gain, Tx: {tx_id}, Rx:{loaded_element[3]}",
        show_plot=True,
        save_path=save_path,
        save_format=save_format,
    )

    # Compute and plot full average and std dev for channel response in phase speed
    avg_ph_speed, std_ph_speed = utils.math_proc.compute_avg_phase_speed(
        loaded_element[2], compute_sdt=True
    )

    utils.plot.plot_avg_phase_speed(
        avg_data=avg_ph_speed,
        std_data=std_ph_speed,
        freqs=subc_frequencies[:-1],
        title=f"Phase variation between neighbouring subcarriers, Tx: {tx_id}, Rx:{loaded_element[3]}",
        show_plot=False,
        save_path=save_path,
        save_format=save_format,
    )

    # Compute and plot PDP from full average channel response
    pdp = utils.math_proc.pdp_from_cmplx_freq_resp(
        loaded_element[2], shifted=False, dB_out=True
    )
    avg_pdp, std_pdp = utils.math_proc.compute_avg_std(pdp, compute_sdt=True)
    time_per_bin = 1 / sample_rate
    rms_spread = utils.math_proc.rms_delay_spread(
        utils.math_proc.pow_dB2lin(avg_pdp), in_scaling=time_per_bin
    )

    utils.plot.plot_avg_pdp(
        avg_pdp,
        std_pdp,
        delays=np.arange(pdp.shape[-1]) * time_per_bin,
        title=f"Power Delay Profile, Tx: {tx_id}, Rx:{loaded_element[3]}, RMS spread = {EngFormatter('s').format_eng(rms_spread)}s",
        show_plot=False,
        save_path=save_path,
        save_format=save_format,
    )

    # Compute and plot temporal variation function (MSE)

    mse = utils.math_proc.compute_frame_mse(loaded_element[2], ref_psd=avg_pow)

    utils.plot.plot_val_v_time_wo_outliers(
        mse,
        loaded_element[-1],
        title=f"MSE compared to average PSD, Tx: {tx_id}, Rx:{loaded_element[3]}",
        show_plot=False,
        save_path=save_path + "v_avg_wo_outliers_",
        save_format=save_format,
    )

    utils.plot.plot_val_v_time_wo_outliers(
        utils.math_proc.compute_avg_power(
            loaded_element[2], frame_avg=False, symb_avg=True, subc_avg=True
        ),
        loaded_element[-1],
        title=f"Average power per frame, Tx: {tx_id}, Rx:{loaded_element[3]}",
        ylabel="Avg Power (dBFS)",
        show_plot=False,
        save_path=save_path + "avg_power_wo_outliers_",
        save_format=save_format,
    )
    pow_mse = np.square(
        utils.math_proc.compute_avg_power(
            loaded_element[2], frame_avg=True, symb_avg=True, subc_avg=True
        )
        - utils.math_proc.compute_avg_power(
            loaded_element[2], frame_avg=False, symb_avg=True, subc_avg=True
        )
    )
    utils.plot.plot_val_v_time_wo_outliers(
        pow_mse,
        loaded_element[-1],
        title=f"MSE between Average power per frame and overall average power, Tx: {tx_id}, Rx:{loaded_element[3]}",
        ylabel="Avg Power MSE",
        show_plot=False,
        save_path=save_path + "avg_power_mse_",
        save_format=save_format,
    )

    utils.plot.plot_val_v_time_wo_outliers(
        loaded_element[-2],
        loaded_element[-1],
        title=f"Frequency Offsets measured by Schmidl&Cox block, Tx: {tx_id}, Rx:{loaded_element[3]}",
        ylabel="Offset (Hz)",
        show_plot=False,
        save_path=save_path + "freq_offsets",
        save_format=save_format,
    )


def plot_over_frequencies(
    data_list: list[
        list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]
    ],
    tx_id: int,
    sample_rate: float,
    save_path: str,
    save_format: str = "png",
) -> None:
    """
    Create, save and displays a list of plots for a frequency sweep:
    Frequency response (in Power only)
    PDP

    Parameters
    ----------
    data_list : list[ list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]] ]
        List of loaded data elements for each frequency
    tx_id : int
        Index of transmitter (for display purposes)
    sample_rate : float
        Sample rate of captured data, used to compute time resolution
    save_path : str
        Save path for figures. None to not save
    save_format : str, optional
        File format for saved figures, by default "png"
    """
    formatter = EngFormatter(unit="", sep=" ")
    # Compute Frequency averages
    # Compensate filtering
    averages_list = []
    # TODO adapt filtering to device and band
    plt.figure(figsize=(10, 5))
        
    for i, elem in enumerate(data_list):
        avg_pow = utils.math_proc.compute_avg_power(elem[2], compute_sdt=False)
        # avg_pow = avg_pow - utils.filter_comp.use_error_func(elem[1] / sample_rate)
        # Use calibrated RRC rolloff
        tx_type = utils.hardware_info.NODE_TYPES[tx_id]
        rx_type = utils.hardware_info.NODE_TYPES[data_list[0][3]]
        rolloff = utils.hardware_info.FILTER_ROLLOFF["TX"][tx_type]["RX"][rx_type][sample_rate]
        avg_pow = avg_pow - utils.filter_comp.rc_error_func(elem[1] / sample_rate, rolloff)

        # Use full averaged filter correction
        # if rx_type == "N2932":
        #     avg_pow = avg_pow - utils.hardware_info.full_filter_25m_2932
        # else:
        #     avg_pow = avg_pow - utils.hardware_info.full_filter_25m_2944
        averages_list.append(avg_pow)
        plt.plot(elem[1] + elem[0], avg_pow, label=f"Fc={elem[0]}")

    # Merge frequency measurements
    # Use the first center frequency as reference point to draw regular bins
    ref_common_freq = data_list[0][0]
    # Get bin width from bin array of first Fc (to avoid needing to forward fft_len)
    bin_width = data_list[0][1][1] - data_list[0][1][0]

    # Create an array of frequency bins to represent all the data points we have
    corrected_bin_list = []
    for i, elem in enumerate(data_list):
        corresp_bin_array = elem[1] + elem[0] - ref_common_freq
        corresp_bin_array = np.round(corresp_bin_array / bin_width) * bin_width
        corrected_bin_list.append(corresp_bin_array)
    corrected_bin_array = np.concatenate(corrected_bin_list, axis=0)
    corrected_bin_array = np.unique(corrected_bin_array)

    num_of_corresp_values = np.zeros_like(corrected_bin_array)
    merged_array = np.zeros_like(corrected_bin_array)

    # Fill in and average the data points we have

    averaging_window = utils.math_proc.raised_cosine(data_list[0][1]/sample_rate, 0.25)**10
    for i, elem in enumerate(data_list):
        corresp_first_bin = elem[1][0] + elem[0] - ref_common_freq
        corresp_first_bin = round(corresp_first_bin / bin_width) * bin_width
        start_corrected_index = np.nonzero(corrected_bin_array == corresp_first_bin)[0][
            0
        ]

        # (Try to) correct gain differences between RF chains at various frequencies
        # For an even better correction, one would need to perform actual calibration
        gain_correction = utils.hardware_info.get_gain_correction(
            tx_id, data_list[0][3], elem[0]
        )
        merged_array[
            start_corrected_index : start_corrected_index + len(averages_list[i])
        ] += (averages_list[i] + gain_correction)*averaging_window
        num_of_corresp_values[
            start_corrected_index : start_corrected_index + len(averages_list[i])
        ] += 1*averaging_window
    merged_array /= num_of_corresp_values
    plt.plot(corrected_bin_array + ref_common_freq, merged_array, label="Averaged", lw=3, ls='--')
    ax = plt.gca()
    ax.xaxis.set_major_formatter(formatter)
    plt.grid("both")
    plt.xlabel("Freq bin (Hz)")
    plt.ylabel("Power (dBFS)")
    plt.title(f"Wideband frequency response, Filter corrected(ish)), Tx: {tx_id}, Rx:{data_list[0][3]}")
    plt.tight_layout()

    # utils.plot.plot_avg_freq(
    #     avg_data=merged_array,
    #     freqs=corrected_bin_array + ref_common_freq,
    #     title=f"Wideband frequency response, Filter corrected(ish)), Tx: {tx_id}, Rx:{data_list[0][3]}",
    #     save_format=save_format,
    #     save_path=save_path + "wideband_",
    #     show_plot=True,
    # )

    # Compute PDP from merged frequency response

    # Compensate natural antenna gain loss with frequency (from reduction of equivalent surface)
    flattened_by_freqs = merged_array - 40 * np.log10(
        scipy.constants.c / (corrected_bin_array + ref_common_freq)
    )

    # Need to compute PDP on contiguous section of information (cannot have holes)
    # Thus, need to find these sections, and get the biggest, for more info
    split_locations = np.flatnonzero(np.diff(corrected_bin_array) != bin_width) + 1
    splitted_arrays = np.split(flattened_by_freqs, split_locations)
    splitted_bin_arrays = np.split(
        corrected_bin_array + ref_common_freq, split_locations
    )
    biggest_split = np.argmax([len(x) for x in splitted_arrays])

    merged_pdp = utils.math_proc.pdp_from_psd(splitted_arrays[biggest_split])
    splitted_bw = (
        splitted_bin_arrays[biggest_split][-1] - splitted_bin_arrays[biggest_split][0]
    )
    time_per_bin = 1 / splitted_bw
    rms_spread = utils.math_proc.rms_delay_spread(
        utils.math_proc.pow_dB2lin(merged_pdp), in_scaling=time_per_bin
    )

    utils.plot.plot_avg_pdp(
        merged_pdp,
        delays=np.arange(merged_pdp.shape[-1]) * time_per_bin,
        title=f"Power Delay Profile, Tx: {tx_id}, Rx:{data_list[0][3]}, RMS spread = {EngFormatter('s').format_eng(rms_spread)}s",
        show_plot=False,
        save_path=save_path + "wideband_",
        save_format=save_format,
    )

    utils.plot.plot_avg_pdp(
        merged_pdp[:len(merged_pdp)//5],
        delays=np.arange(merged_pdp.shape[-1])[:len(merged_pdp)//5] * time_per_bin,
        title=f"Power Delay Profile, Tx: {tx_id}, Rx:{data_list[0][3]}, RMS spread = {EngFormatter('s').format_eng(rms_spread)}s",
        show_plot=False,
        save_path=save_path + "wideband_zoomed",
        save_format=save_format,
    )

    return


def plot_one_file(
    file_path: str,
    save_path: str,
    req_fc: float,
    req_rx_id: int = -1,
    req_tx_id: int = -1,
    save_format: str = "png",
) -> None:
    """
    Plots Channel frequency response (power and phase speed) and Power Delay profile
    As as average value, plus deviation

    Also plots time variation for channel response

    Plots are saved as pngs to the specified folder and prefix

    Parameters
    ----------
    file_path : str
        _description_
    save_path : str
        _description_
    req_fc : float
        _description_
    req_rx_id : int, optional
        _description_, by default -1
    req_tx_id : int, optional
        _description_, by default -1
    """

    loaded_list, sample_rate = utils.load.load_files(
        file_paths=file_path,
        req_tx_id=req_tx_id,
        req_rx_id=req_rx_id,
        req_fc=req_fc,
    )

    if (
        (loaded_list is None)
        or (not isinstance(loaded_list, list))
        or (len(loaded_list) == 0)
    ):
        print(f"Nothing was loaded from {file_path}")
        return

    save_path = save_path + f"_rx{loaded_list[0][3]}_"
    print(f"Saving file to prefix: {save_path}")

    if len(loaded_list) > 1:
        print(
            "Warning: more than one frequency element was loaded, plotting wideband frequency stats"
        )
        plot_over_frequencies(
            loaded_list,
            tx_id=req_tx_id,
            sample_rate=sample_rate,
            save_path=save_path,
            save_format=save_format,
        )
    else:
        plot_one_freq(
            loaded_list[0],
            tx_id=req_tx_id,
            sample_rate=sample_rate,
            save_path=save_path,
            save_format=save_format,
        )


def argument_parser():
    """
    Creates an argument parser and returns it
    """
    parser = ArgumentParser()
    parser.add_argument(
        "--file-glob",
        dest="file_glob",
        type=str,
        default=".*.sigmf-meta",
        help="Regex pointing to sigmf files to read [default=%(default)r]",
    )
    parser.add_argument(
        "--plot-one",
        dest="plot_one",
        action="store_true",
        help="Plot one Rx and Fc [default=%(default)r]",
    )
    parser.add_argument(
        "--fc",
        dest="center_freq",
        type=float,
        default=-1,
        help="Center frequency to plot for [default=%(default)r]",
    )
    parser.add_argument(
        "--rx-id",
        dest="rx_id",
        type=int,
        default=-1,
        help="Reciever Id to plot from [default=%(default)r]",
    )
    parser.add_argument(
        "--tx-id",
        dest="tx_id",
        type=int,
        default=-1,
        help="Transmitter Id to plot from [default=%(default)r]",
    )
    parser.add_argument(
        "--img-save-folder",
        dest="img_folder",
        type=str,
        default=None,
        help="Save plots in the specified folder [default=%(default)r]",
    )
    parser.add_argument(
        "--img-save-format",
        dest="img_format",
        type=str,
        choices=["pdf", "png", "jpg"],
        default="pdf",
        help="Save plots as image of the specified format [default=%(default)r]",
    )

    return parser


def main(options=None):
    if options is None:
        options = argument_parser().parse_args()

    filenames = glob.glob(options.file_glob, recursive=True)
    print(f"Recieved a file glob (regex) as: {options.file_glob}")
    print(f"Will process the following files: {filenames}")
    for filename in filenames:
        if options.img_folder is None:
            file_dir = os.path.dirname(filename)
            options.img_folder = os.path.join(file_dir, "plot_one_file")
        plot_one_file(
            [filename],
            options.img_folder,
            options.center_freq,
            options.rx_id,
            options.tx_id,
            options.img_format,
        )
    plt.show()


if __name__ == "__main__":
    main()

"""
Collection of information about hardware
"""
import numpy as np

NODE_TYPES = {
    1: "PICO_44",
    2: "N2932",
    3: "N2932",
    4: "N2932",
    5: "N2944",
    6: "N2932",
    7: "N2932",
    8: "N2932",
    9: "N2932",
    10: "N2944",
    11: "N2944",
    12: "N2932",
    13: "N2932",
    14: "N2932",
    15: "N2932",
    16: "N2932",
    17: "N2932",
    18: "N2932",
    19: "PICO_44",
    20: "N2932",
    21: "N2932",
    22: "PICO_44",
    23: "N2932",
    24: "N2932",
    25: "N2932",
    26: "N2932",
    27: "N2932",
    28: "N2932",
    29: "N2932",
    30: "N2944",
    31: "N2944",
    32: "N2932",
    33: "N2932",
    34: "N2932",
    35: "N2932",
    36: "N2944",
    37: "N2932",
    38: "N2932",
    39: "PICO_22",
    40: "PICO_44",
}

GAIN_THRESHOLDS = {
    "N2944": {
        "TX": {500e6: 5, 1500e6: 0, 2000e6: 0},
        "RX": {500e6: 7, 1500e6: -4, 2000e6: -10},
    },
    "N2932": {
        "TX": {500e6: 0, 1500e6: 0, 2000e6: 0},
        "RX": {500e6: 0, 1500e6: 0, 2000e6: -10},
    },
}


def get_gain_correction(tx_id: int, rx_id: int, freq: float) -> float:
    """
    Get a gain correction value for a TX-RX pair, at a given frequency
    This takes into account the type of device and is used to correct for diffenrences RF chains

    Parameters
    ----------
    tx_id : int
        TX Id (in CorteXlab)
    rx_id : int
        RX Id (in CorteXlab)
    freq : float
        Center frequency of interest

    Returns
    -------
    float
        gain correction value
    """
    gain_correction = 0
    if freq < 500e6:
        gain_correction = (
            GAIN_THRESHOLDS[NODE_TYPES[tx_id]]["TX"][500e6]
            + GAIN_THRESHOLDS[NODE_TYPES[rx_id]]["RX"][500e6]
        )
    if freq >= 1500e6:
        gain_correction = (
            GAIN_THRESHOLDS[NODE_TYPES[tx_id]]["TX"][1500e6]
            + GAIN_THRESHOLDS[NODE_TYPES[rx_id]]["RX"][1500e6]
        )
    if freq > 2000e6:
        gain_correction = (
            GAIN_THRESHOLDS[NODE_TYPES[tx_id]]["TX"][2000e6]
            + GAIN_THRESHOLDS[NODE_TYPES[rx_id]]["RX"][2000e6]
        )
    return gain_correction


# NEEDs more calibration
FILTER_ROLLOFF = {
    "TX": {
        "N2944": {
            "RX": {"N2944": {25e6: 0.14932971}, "N2932": {25e6: 0.21933804}},
        },
        "N2932": {
            "RX": {"N2944": {25e6: 0.0}, "N2932": {25e6: 0.0}},  # NEEDs measurements
        },
    }
}

full_filter_25m_2944 = np.loadtxt("utils/full_filter_25m_2944.txt")
full_filter_25m_2932 = np.loadtxt("utils/full_filter_25m_2932.txt")

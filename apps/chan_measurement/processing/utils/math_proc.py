import numpy as np
import scipy


def lin2dB(x: int | float | np.ndarray[np.float32]) -> float | np.ndarray[np.float32]:
    """
    Convert value from linear scale to dB

    Parameters
    ----------
    x : int | float | np.ndarray[np.float32]
        Input value

    Returns
    -------
    float|np.ndarray[np.float32]
        Output value, 10log10(x)
    """
    return 10 * np.log10(x)


def pow_lin2dB(
    x: int | float | np.ndarray[np.float32] | np.ndarray[np.complex64],
) -> float | np.ndarray[np.float32]:
    """
    Get power in dB from value (20*log10(abs(x))

    Parameters
    ----------
    x : int | float | np.ndarray[np.float32] | np.ndarray[np.complex64]
        value

    Returns
    -------
    float | np.ndarray[np.float32]
        output
    """
    return 20 * np.log10(np.abs(x))


def dB2lin(x: float | np.ndarray[np.float32]) -> float | np.ndarray[np.float32]:
    """
    Get value from dB scale to linear

    Parameters
    ----------
    x : float | np.ndarray[np.float32]
        input value

    Returns
    -------
    float | np.ndarray[np.float32]
        Output
    """
    return np.power(x / 10, 10)


def pow_dB2lin(x: float | np.ndarray[np.float32]) -> float | np.ndarray[np.float32]:
    """
    Get linear amplitude from dB scale power (10**(x/20))

    Parameters
    ----------
    x : float | np.ndarray[np.float32]
        dB power

    Returns
    -------
    float | np.ndarray[np.float32]
        output
    """
    return np.power(x / 20, 10)


def delay2dist(x):
    """
    Convert a time delay to a distance travelled by light in vacuum

    Parameters
    ----------
    x : _type_
        time delay (s)

    Returns
    -------
    _type_
        Distance  (m)
    """
    return x * scipy.constants.c


def dist2delay(x):
    """
    Convert a distance travelled by light in vacuum to a time delay

    Parameters
    ----------
    x : _type_
        Distance (m)

    Returns
    -------
    _type_
        Time delay (s)
    """
    return x / scipy.constants.c


def raised_cosine(x: np.ndarray[float], roll_off: float) -> np.ndarray[float]:
    output_array = np.where(
        abs(x) < ((1 - roll_off) / 2),
        1,
        0.5 * (1 + np.cos((np.pi / roll_off) * (np.abs(x) - ((1 - roll_off) / 2)))),
    )
    return output_array


def pdp_from_cmplx_freq_resp(
    freq_response: np.ndarray[complex], shifted: bool = False, dB_out: bool = True
) -> np.ndarray[complex]:
    """
    Computes a Power Delay Profile (PDP) from a complex valued channel frequency response

    Parameters
    ----------
    freq_response : np.ndarray[complex]
        Complex valued frequency response

    shifted : bool
        Is the freq response already FFT shifted? Default is False
        Set to True if the zero frequency component is at index 0
        If False, the FFt shift will be performed in this function for the last axis (-1)

    dB_out : bool
        Set to True (default) to get the PDP in dB

    Returns
    -------
    np.ndarray[complex]
        PDP
    """

    if not shifted:
        freq_response = np.fft.ifftshift(freq_response, axes=-1)

    pdp = np.fft.ifft(
        np.conj(freq_response) * freq_response
    )  # [: freq_response.shape[-1] // 2]
    print(f"PDP shape: {pdp.shape},     Freq shape: {freq_response.shape}")
    pdp = pdp[:, :, : pdp.shape[-1] // 2]
    print(f"PDP shape: {pdp.shape},     Freq shape: {freq_response.shape}")
    if dB_out:
        return pow_lin2dB(pdp)

    return pdp


def pdp_from_psd(
    freq_response: np.ndarray[float],
    shifted: bool = False,
    dB_in: bool = True,
    dB_out: bool = True,
) -> np.ndarray[float]:
    """
    Computes a Power Delay Profile (PDP) from a float valued channel frequency response

    Parameters
    ----------
    freq_response : np.ndarray[float]
        Complex valued frequency response

    shifted : bool
        Is the freq response already FFT shifted? Default is False
        Set to True if the zero frequency component is at index 0
        If False, the FFt shift will be performed in this function for the last axis (-1)

    dB_in : bool
        Set to True (default) if the input frequency response is in dB
    dB_out : bool
        Set to True (default) to get the PDP in dB

    Returns
    -------
    np.ndarray[float]
        PDP
    """

    if not shifted:
        freq_response = np.fft.ifftshift(freq_response, axes=-1)

    if dB_in:
        freq_response = pow_dB2lin(freq_response)

    pdp = np.fft.ifft(freq_response)
    print(f"PDP shape: {pdp.shape},     Freq shape: {freq_response.shape}")
    pdp = pdp[: pdp.shape[-1] // 2]
    print(f"PDP shape: {pdp.shape},     Freq shape: {freq_response.shape}")
    if dB_out:
        return pow_lin2dB(pdp)

    return pdp


def compute_avg_std(
    symbol_array: np.ndarray[float],
    frame_avg: bool = True,
    symb_avg: bool = True,
    subc_avg: bool = False,
    compute_sdt=False,
) -> np.ndarray[float] | tuple[np.ndarray[float], np.ndarray[float]]:
    """
    Computes average over a 3d array
    Can optionnaly also compute standard deviation

    Parameters
    ----------
    symbol_array : np.ndarray[complex]
        Array to process Expected to be 3d
    frame_avg : bool, optional
        Set to True to average over the frames (dim 0), by default True
    symb_avg : bool, optional
        Set to True to average over the symbols (dim 1), by default True
    subc_avg : bool, optional
        Set to True to average over the subcarriers (dim 2), by default False
    compute_sdt : bool, optional
        Set to True to also compute standard deviation, by default False

    Returns
    -------
    np.ndarray[float]
        average
    np.ndarray[float]
        std  (if required)
    """

    mean_axes_compute = []
    if frame_avg:
        mean_axes_compute.append(0)
    if symb_avg:
        mean_axes_compute.append(1)
    if subc_avg:
        mean_axes_compute.append(2)
    avg = np.mean(symbol_array, axis=tuple(mean_axes_compute))

    if compute_sdt:
        std = np.std(symbol_array, axis=tuple(mean_axes_compute))
        return avg, std

    return avg


def compute_avg_power(
    symbol_array: np.ndarray[complex],
    frame_avg: bool = True,
    symb_avg: bool = True,
    subc_avg: bool = False,
    compute_sdt=False,
) -> np.ndarray[float] | tuple[np.ndarray[float], np.ndarray[float]]:
    """
    Computes average power over a complex array.
    Input array is expected to have 3 dimensions: [frames, symbols, subcarriers]
    Can be averaged over the one or both first 2 dimensions

    Parameters
    ----------
    symbol_array : np.ndarray[complex]
        Array to process Expected to be 3d
    frame_avg : bool, optional
        Set to True to average over the frames (dim 0), by default True
    symb_avg : bool, optional
        Set to True to average over the symbols (dim 1), by default True
    subc_avg : bool, optional
        Set to True to average over the subcarriers (dim 2), by default False
    compute_sdt : bool, optional
        Set to True to also compute standard deviation, by default False

    Returns
    -------
    np.ndarray[float]
        average power
    np.ndarray[float]
        std of power
    """
    db_pow = pow_lin2dB(symbol_array)

    return compute_avg_std(db_pow, frame_avg, symb_avg, subc_avg, compute_sdt)
    # return pow_lin2dB(compute_avg_std(symbol_array, frame_avg, symb_avg, subc_avg, compute_sdt))


def compute_avg_phase_speed(
    symbol_array: np.ndarray[complex],
    frame_avg: bool = True,
    symb_avg: bool = True,
    compute_sdt=False,
) -> np.ndarray[float] | tuple[np.ndarray[float], np.ndarray[float]]:
    """
    Computes average phase speed over a complex array.
    Input array is expected to have 3 dimensions: [frames, symbols, subcarriers]
    Can be averaged over the one or both first 2 dimensions

    Parameters
    ----------
    symbol_array : np.ndarray[complex]
        Array to process Expected to be 3d
    frame_avg : bool, optional
        Set to True to average over the frames (dim 0), by default True
    symb_avg : bool, optional
        Set to True to average over the symbols (dim 1), by default True
    compute_sdt : bool, optional
        Set to True to also compute standard deviation, by default False

    Returns
    -------
    np.ndarray[float]
        average phase speed
    np.ndarray[float]
        std of phase speed
    """

    phases = np.angle(symbol_array)
    unwrapped = np.unwrap(phases)
    diff = np.diff(unwrapped, axis=-1)

    return compute_avg_std(diff, frame_avg, symb_avg, compute_sdt=compute_sdt)


def mean_excess_delay(values: np.ndarray[float], in_scaling: float = 1) -> float:
    delays = (
        np.arange(
            values.shape[-1],
        )
        * in_scaling
    )
    return np.average(delays, axis=-1, weights=values)


def rms_delay_spread(values: np.ndarray[float], in_scaling: float = 1) -> float:
    delays = (
        np.arange(
            values.shape[-1],
        )
        * in_scaling
    )

    mean = mean_excess_delay(values, in_scaling)
    sec_moment = np.sum(values * np.power(delays, 2)) / np.sum(values)
    return np.sqrt(sec_moment - (mean**2))


def compute_frame_mse(
    values: np.ndarray[complex],
    ref_frame_index: int = 0,
    ref_psd=None,
    already_psd=False,
) -> np.ndarray[float]:
    """
    Computes Mean Square Error between the Power spectral density (PSD) of the reference frame, and each frame in the input array

    Reference can also be a supplied PSD array

    Parameters
    ----------
    values : np.ndarray[complex]
        Input array of complex channel responses. Expected to be 3D: [frames, symbols, subcarriers]
    ref_frame_index : int, optional
        Index of the reference frame (negative to index from the end), by default 0
    ref_psd : int, optional
        Reference PSD array, by default None

    Returns
    -------
    np.ndarray[float]
        Output array of MSE, 1D vector of length [frames]
    """

    if already_psd:
        frame_avg_pwr = values
    else:
        frame_avg_pwr = compute_avg_power(values, frame_avg=False, symb_avg=True)

    if ref_psd is None:
        ref_psd = frame_avg_pwr[ref_frame_index]

    mse = np.mean(np.square(ref_psd - frame_avg_pwr), axis=-1)

    return mse

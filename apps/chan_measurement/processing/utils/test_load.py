import numpy as np
from . import load


def list_is_full_equal(list_1, reference):
    # print(list_1)
    assert isinstance(list_1, list) 
    assert isinstance(reference, list)

    assert len(list_1) == len(reference)

    for i, list_elem in enumerate(list_1):
        # print(i)
        # # print(list_elem.dty)
        assert isinstance(list_elem, list)
        assert isinstance(reference[i], list)
        
        assert len(list_elem) == 5
        assert len(reference[i]) == 5
        
        assert list_elem[0] == reference[i][0]
        
        assert np.all(np.equal(list_elem[1], reference[i][1]))
        

        assert np.all(np.equal(list_elem[2], reference[i][2]))
        assert list_elem[3] == reference[i][3]
        assert np.all(np.equal(list_elem[4], reference[i][4]))
    return True


class TestMergeFilterLists:
    base_list = [
        [
            1e6,
            np.linspace(-10000, 10000, 48),
            np.random.normal(size=(2, 12, 48)),
            5,
            np.arange(
                "2023-10-02T12:00:00.000",
                "2023-10-02T12:00:00.002",
                dtype="datetime64[ms]",
            ),
        ],
        [
            2e6,
            np.linspace(-10000, 10000, 48),
            np.random.normal(size=(10, 12, 48)),
            5,
            np.arange(
                "2023-10-02T12:00:01.000",
                "2023-10-02T12:00:01.010",
                dtype="datetime64[ms]",
            ),
        ],
        [
            1e6,
            np.linspace(-10000, 10000, 48),
            np.random.normal(size=(3, 12, 48)),
            5,
            np.arange(
                "2023-10-02T12:00:03.000",
                "2023-10-02T12:00:03.003",
                dtype="datetime64[ms]",
            ),
        ],
        [
            1e6,
            np.linspace(-10000, 10000, 48),
            np.random.normal(size=(2, 12, 48)),
            7,
            np.arange(
                "2023-10-02T12:00:05.000",
                "2023-10-02T12:00:05.002",
                dtype="datetime64[ms]",
            ),
        ],
    ]

    def test_filter_list_noop(self):
        filtered_list = load.filter_list(self.base_list, req_fc=-1, req_rx_id=-1)

        assert list_is_full_equal(self.base_list, filtered_list)

    
    def test_filter_list_fc(self):
        filtered_list = load.filter_list(self.base_list, req_fc=1e6, req_rx_id=-1)
        expected = [self.base_list[0]] + self.base_list[2:]
        assert list_is_full_equal(filtered_list, expected)

    
    def test_filter_list_rx(self):
        filtered_list = load.filter_list(self.base_list, req_fc=-1, req_rx_id=5)
        expected = self.base_list[:-1]
        assert list_is_full_equal(filtered_list, expected)

    
    def test_filter_list_fc_rx(self):
        filtered_list = load.filter_list(self.base_list, req_fc=1e6, req_rx_id=5)
        expected = [self.base_list[0], self.base_list[2]]
        assert list_is_full_equal(filtered_list, expected)

    def test_merge_list(self):
        expected_list = [self.base_list[0], self.base_list[3], self.base_list[1]]
        expected_list[0][2] = np.concatenate((self.base_list[0][2], self.base_list[2][2]), axis=0)
        expected_list[0][4] = np.concatenate((self.base_list[0][4], self.base_list[2][4]), axis=0)

        out = load.merge_duplicate_fc(self.base_list)

        assert list_is_full_equal(out, expected_list)

            

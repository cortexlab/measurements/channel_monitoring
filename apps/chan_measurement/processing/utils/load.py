# import os
# import glob
# import sigmf
from sigmf import SigMFFile, sigmffile
import numpy as np
from tqdm import tqdm 

def load_file(
    file_path: str, fixed_samps_per_cap: int = -1, req_tx_id: int = 31,
    min_date: np.datetime64 = np.datetime64('2000-01-01')
) -> (
    tuple[list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]], float]
    | None
):
    """
    Load capture data from one sigmf file.
    The file must contain captures

    Parameters
    ----------
    file_path : str
        File path to read from (with or without extension).
        Can be either a meta or a data file
    fixed_samps_per_cap : int
        Amount of samples per capture. If unknown, set to -1
    req_tx_id : int
        Transmitter ID to consider. 
        TODO: extend to read all TXs and sort them out properly 
        (when and if multi TX experiment gets developped)

    Returns
    -------
    list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]] | None
        Read data, as a list of captured data, each element being a list of:
            Center frequency,
            Array of frequency offsets for each subcarrier,
            3d array with channel measurement corresponding to this Fc, 
                with first dimension being frames, 
                then OFDM symbols of that frame, 
                then subcarrier per symbol
            Reciever ID
            List of DateTime for the beginning of each captured frame
    float
        Sample rate of recording
    """

    print(file_path)
    file_to_read = sigmffile.fromfile(file_path)

    file_sample_count = file_to_read.sample_count  # We'll read everything anyways
    samples = file_to_read.read_samples(0, -1)
    glob_info = file_to_read.get_global_info()
    annotations = (
        file_to_read.get_annotations()
    )  # Not expected to contain anything useful
    captures = file_to_read.get_captures()

    if len(captures) < 2:
        print(f"Error: File {file_path} does not contain captures")
        return None, -1

    if fixed_samps_per_cap < 0:
        samps_per_frame = captures[0][
            "cxlb:packet_len"
        ]  # This should always be the same
    else:
        samps_per_frame = fixed_samps_per_cap

    samples = samples[file_to_read.get_capture_start(0) :]

    # Get useful global data
    rx_id = glob_info["cxlb:rx_node"]
    occupied_carrs = glob_info["ofdm:occupied_carriers"]
    samp_rate = glob_info[SigMFFile.SAMPLE_RATE_KEY]
    fft_len = glob_info["ofdm:fft_len"]

    samps_per_symbol = len(occupied_carrs)
    freq_per_carr = (samp_rate / fft_len) * np.array(occupied_carrs)
    samps_per_frame = 12*samps_per_symbol
    # Read captures data
    data_list_per_fc = []

    prev_center_freq = captures[0][SigMFFile.FREQUENCY_KEY]
    reshaped = None
    record_chunk_frame_num = 0
    record_chunk_samp_start = 0
    dates = []
    f_offsets = []
    for cap in tqdm(captures):
        cap_packet_len = cap["cxlb:packet_len"]
        if cap_packet_len != samps_per_frame:
            # print("Warning! we encountered a wrong sized packet, ignoring")
            continue
        cap_tx = cap["cxlb:tx_node"]
        if cap_tx != req_tx_id:
            print("Encountered a packet from wrong Tx, ignoring")
            continue

        cap_center_freq = cap[SigMFFile.FREQUENCY_KEY]
        cap_samp_index = cap[SigMFFile.START_INDEX_KEY]
        cap_datetime = cap[SigMFFile.DATETIME_KEY]
        cap_freq_offset = cap["ofdm:freq_offset"]
        if np.datetime64(cap_datetime) < min_date:
            print("Packet is too early, ignoring")
            continue
            
        if cap_center_freq != prev_center_freq:
            if record_chunk_frame_num > 0:
                # Record chunk of data up to the current capture (excluding it)
                if reshaped is None:
                    reshaped = np.reshape(
                        samples[record_chunk_samp_start : record_chunk_samp_start + record_chunk_frame_num*samps_per_frame],
                        (record_chunk_frame_num, -1, samps_per_symbol),
                    )
                else:
                    reshaped = np.concatenate(
                        (
                            reshaped,
                            np.reshape(
                                samples[record_chunk_samp_start : record_chunk_samp_start + record_chunk_frame_num*samps_per_frame],
                                (record_chunk_frame_num, -1, samps_per_symbol),
                            ),
                        ),
                        axis=0,
                    )
                data_list_per_fc.append(
                    [prev_center_freq, freq_per_carr, np.copy(reshaped), rx_id, np.array(f_offsets), np.array(dates, dtype='datetime64[us]')]
                )
            reshaped = None
            record_chunk_frame_num = 0
            dates = []
            f_offsets = []
            prev_center_freq = cap_center_freq

        if cap_samp_index + cap_packet_len <= len(samples):
            dates.append(cap_datetime)
            f_offsets.append(cap_freq_offset)

            if record_chunk_frame_num == 0:
                record_chunk_samp_start = cap_samp_index
            if record_chunk_samp_start+record_chunk_frame_num*samps_per_frame == cap_samp_index:
                # We have a continguous sample chunk, so we can wait to load it all at once
                record_chunk_frame_num += 1
                continue
            if reshaped is None:
                # reshaped = np.reshape(
                #     samples[cap_samp_index : cap_samp_index + cap_packet_len],
                #     (1, -1, samps_per_symbol),
                # )
                reshaped = np.reshape(
                    samples[record_chunk_samp_start : record_chunk_samp_start + record_chunk_frame_num*samps_per_frame],
                    (record_chunk_frame_num, -1, samps_per_symbol),
                )
            else:
                # reshaped = np.concatenate(
                #     (
                #         reshaped,
                #         np.reshape(
                #             samples[cap_samp_index : cap_samp_index + cap_packet_len],
                #             (1, -1, samps_per_symbol),
                #         ),
                #     ),
                #     axis=0,
                # )
                reshaped = np.concatenate(
                    (
                        reshaped,
                        np.reshape(
                            samples[record_chunk_samp_start : record_chunk_samp_start + record_chunk_frame_num*samps_per_frame],
                            (record_chunk_frame_num, -1, samps_per_symbol),
                        ),
                    ),
                    axis=0,
                )
            record_chunk_samp_start = cap_samp_index
            record_chunk_frame_num = 1

    if record_chunk_frame_num > 0:
        if reshaped is None:
            reshaped = np.reshape(
                samples[record_chunk_samp_start : record_chunk_samp_start + record_chunk_frame_num*samps_per_frame],
                (record_chunk_frame_num, -1, samps_per_symbol),
            )
        else:
            reshaped = np.concatenate(
                (
                    reshaped,
                    np.reshape(
                        samples[record_chunk_samp_start : record_chunk_samp_start + record_chunk_frame_num*samps_per_frame],
                        (record_chunk_frame_num, -1, samps_per_symbol),
                    ),
                ),
                axis=0,
            )
    if reshaped is not None:
        data_list_per_fc.append(
            [prev_center_freq, freq_per_carr, reshaped, rx_id, np.array(f_offsets), np.array(dates, dtype='datetime64[ms]')]
        )

    return data_list_per_fc, samp_rate


def load_files(
    file_paths: list[str],
    fixed_samps_per_cap: int = -1,
    req_tx_id: int = 31,
    req_rx_id: int = -1,
    req_fc: float = -1,
)-> tuple[list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]], float] | None:
    """
    Load capture data from a list of file path strings

    Parameters
    ----------
    file_paths : list[str]
        List of strings read from (with or without extension).
        Can be either a meta or a data file
    fixed_samps_per_cap : int, optional
        Amount of samples per capture. If unknown, set to -1 (default)
    req_tx_id : int, optional
        Transmitter ID to consider, by default 31
    req_rx_id : int, optional
        Reciever ID to consider, by default -1 to keep all

    Returns
    -------
    list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]]
        Read data
    float
        Sample rate of recordings
    """
    data_list_per_fc_per_rx = []
    merged_samp_rate = -1
    for file_path in file_paths:
        loaded_list, samp_rate = load_file(
            file_path, fixed_samps_per_cap=fixed_samps_per_cap, req_tx_id=req_tx_id
        )
        if merged_samp_rate < 0:
            merged_samp_rate = samp_rate
        elif merged_samp_rate != samp_rate:
            print("WARNING!!! Sample rates don't match in loaded files, this could cause issues")
        if loaded_list is not None:
            data_list_per_fc_per_rx += loaded_list
    data_list_per_fc_per_rx = filter_list(data_list_per_fc_per_rx, req_rx_id=req_rx_id, req_fc=req_fc)
    data_list_per_fc_per_rx = merge_duplicate_fc(data_list_per_fc_per_rx)
    return data_list_per_fc_per_rx, merged_samp_rate


def filter_list(
    data_list: list[
        list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]
    ],
    req_fc: float = -1,
    req_rx_id: int = -1
) -> (
    list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]]
    | None
):
    """
    Filters a data list to only keep captures with desired center frequency and/or reciever id

    Parameters
    ----------
    data_list : list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]]
        Data list to filter, same format as load functions
    req_fc : float, optional
        Desired center frequency to keep, -1 (default) to not filter. 
        TODO: Extend to use a list of allowed values
    req_rx_id : int, optional
        Reciever ID to keep, -1 (default) to not filter. 
        TODO: Extend to use a list of allowed values

    Returns
    -------
    list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]] | None
        Filtered data list, same format
    """

    # I choose to list all elements to keep,
    # instead of removing (pop) unwanted elements from the list one after the other.
    # I feel this should reduce the memory management loads
    indeces_to_keep = []
    for i, elem in enumerate(data_list):
        if req_fc >= 0:
            if elem[0] != req_fc:
                continue
        if req_rx_id >= 0:
            if elem[3] != req_rx_id:
                continue
        # If we haven't continued in the ifs above, it means that we want to keep this element
        indeces_to_keep.append(i)

    return [data_list[x] for x in indeces_to_keep]



def merge_duplicate_fc(
    data_list: list[
        list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]
    ]
) -> (
    list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]]
    | None
):
    """
    Merge list elements that have the same reciever and center freq as one bigger element

    Parameters
    ----------
    data_list : list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]]
        Read data, as a list of captured data, each element being a list of:
            Center frequency,
            Array of frequency offsets for each subcarrier,
            3d array with channel measurement corresponding to this Fc, 
                with first dimension being frames, 
                then OFDM symbols of that frame, 
                then subcarrier per symbol
            Reciever ID
            List of DateTime for the beginning of each captured frame
    
    Returns
    -------
    list[list[float, np.ndarray[np.float32], np.ndarray[np.complex64], int, list[int]]] | None
        Merged data list, with elements sorted according to center frequency, then rx id
    """
    fc_rx_extract = [[data_list[x][0], data_list[x][3]] for x in range(len(data_list))]

    # Extract all the unique values. 
    unique_vals, inverses = np.unique(
        np.array(fc_rx_extract), axis=0, return_inverse=True
    )
    merged_list = [0] * len(unique_vals)

    for i, elem in enumerate(inverses):
        if not isinstance(merged_list[elem], list):
            merged_list[elem] = data_list[i]
        else:
            # Check that subcarrier offset match
            if not np.all(np.equal(merged_list[elem][1], data_list[i][1])):
                print(
                    "ERROR, tried to merge channel measurement with different subcarriers"
                )
                continue
            if (not merged_list[elem][0] == data_list[i][0]) or (
                not merged_list[elem][3] == data_list[i][3]
            ):
                print(
                    "ERROR, tried to merge channel measurement with different Center Freq or receiver ID"
                )
                continue
            merged_list[elem][2] = np.concatenate(
                (merged_list[elem][2], data_list[i][2]), axis=0
            )
            merged_list[elem][4] = np.concatenate(
                (merged_list[elem][4], data_list[i][4]), axis=0
            )
            merged_list[elem][5] = np.concatenate(
                (merged_list[elem][5], data_list[i][5]), axis=0
            )

    return merged_list

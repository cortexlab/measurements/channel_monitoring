"""
Collection of functions to correct filter response
"""

import numpy as np


def error_func(x, inner_a, inner_b, inner_c, outer_a, outer_b, outer_c, bias):
    """
    Approximates a filter shape as two polynomials of degree 2
    First poly is used below CUTOFF_POINT (relative, set to 0.41), second is used above

    Parameters
    ----------
    x :  float array
        freq_offset, relative to sample rate (should be in [-0.5,0.5])
    inner_x: float
        Coefficients of the inner polynomial (abs(x) <  CUTOFF_POINT)
    outer_x: float
        Coefficients of the outer polynomial (abs(x) >= CUTOFF_POINT)
    bias: float
        Coefficient to correct imbalance between positive and negative sides
    """

    CUTOFF_POINT = 0.4
    junction_left = inner_a * (CUTOFF_POINT**2) + inner_b * CUTOFF_POINT + inner_c + bias * CUTOFF_POINT
    junction_right = (
        outer_a * (abs(CUTOFF_POINT) ** 3)
        + outer_b * (CUTOFF_POINT**2)
        + outer_c * abs(CUTOFF_POINT)+ bias * CUTOFF_POINT
    )
    offset = junction_left - junction_right

    output_array = np.where(
        abs(x) < CUTOFF_POINT,
        inner_c * (x**2) + inner_b * abs(x) + inner_c + bias * x,
        outer_a * (abs(x) ** 3)
        + outer_b * (x**2)
        + outer_c * abs(x)
        + offset
        + bias * x,
    )
    return output_array

def poly_error_func(x: np.ndarray[float], a, b, c, d, e, bias)->np.ndarray[float]:
    return bias + a * x + b * x**2 + c * x**3 + d * x**4 + e * x**5

def rc_error_func(x: np.ndarray[float], roll_off: float)->np.ndarray[float]:
    output_array = np.where(
        abs(x) < ((1-roll_off)/2),
        1,
        0.5 * (1 + np.cos((np.pi/roll_off)*(np.abs(x)-((1-roll_off)/2))))
    )

    return 30*np.log10(output_array)

def error_func_test(x, inner_a, inner_b, inner_c, outer_a, outer_b, outer_c, bias):
    """
    Approximates a filter shape as two polynomials of degree 2
    First poly is used below CUTOFF_POINT (relative, set to 0.41), second is used above

    Parameters
    ----------
    x :  float array
        freq_offset, relative to sample rate (should be in [-0.5,0.5])
    inner_x: float
        Coefficients of the inner polynomial (abs(x) <  CUTOFF_POINT)
    outer_x: float
        Coefficients of the outer polynomial (abs(x) >= CUTOFF_POINT)
    bias: float
        Coefficient to correct imbalance between positive and negative sides
    """

    CUTOFF_POINT = 0.4
    offset = inner_a * (CUTOFF_POINT**2) + inner_b * CUTOFF_POINT + inner_c + bias * CUTOFF_POINT

    output_array = np.where(
        abs(x) < CUTOFF_POINT,
        inner_c * (x**2) + inner_b * abs(x) + inner_c + bias * x,
        outer_a * ((abs(x)-CUTOFF_POINT) ** 3)
        + outer_b * ((abs(x)-CUTOFF_POINT)**2)
        + outer_c * (abs(x)-CUTOFF_POINT)
        + offset
        + bias * (abs(x)-CUTOFF_POINT)*np.sign(x),
    )
    return output_array

good_ish_params_rel_100M = [
    -1.73562572e-01,
    -2.58647093e00,
    1.08937374e-01,
    -1.74700541e04,
    2.16289830e04,
    -8.93745356e03,
    2.33672166e-01,
]


def use_error_func(bin_vector):
    """
    Approximates the filter shape of the x310 at 100Msps and max bandwidth

    Parameters
    ----------
    x :  array float
        freq_offset, relative to sample rate (should be in [-0.5,0.5])
    """
    return error_func(bin_vector, *good_ish_params_rel_100M)


good_ish_phase_spd_params_rel_100M = [-0.25339675, -0.00296855]


def phase_error_func(x, inner_a, inner_b):
    """
    Approximates a filter shape as a polynomial of degree 2

    Parameters
    ----------
    x :  float array
        freq_offset, relative to sample rate (should be in [-0.5,0.5])
    inner_x: float
        Coefficients of the inner polynomial
    """

    return inner_a * (abs(x) ** 3) + inner_b * (x**2)

def use_phase_spd_error_func(bin_vector):
    """
    Approximates the phase speed filter shape of the x310 at 100Msps and max bandwidth

    Parameters
    ----------
    x :  array float
        freq_offset, relative to sample rate (should be in [-0.5,0.5])
    """
    return phase_error_func(bin_vector, *good_ish_phase_spd_params_rel_100M)

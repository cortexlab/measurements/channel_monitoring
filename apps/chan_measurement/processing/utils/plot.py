import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter

import utils.math_proc


def multiple_formatter(denominator=2, number=np.pi, latex="\pi"):
    def gcd(a, b):
        while b:
            a, b = b, a % b
        return a

    def _multiple_formatter(x, pos):
        den = denominator
        num = int(np.rint(den * x / number))
        com = gcd(num, den)
        (num, den) = (int(num / com), int(den / com))
        if den == 1:
            if num == 0:
                return r"$0$"
            if num == 1:
                return r"$%s$" % latex
            elif num == -1:
                return r"$-%s$" % latex
            else:
                return r"$%s%s$" % (num, latex)
        else:
            if num == 1:
                return r"$\frac{%s}{%s}$" % (latex, den)
            elif num == -1:
                return r"$\frac{-%s}{%s}$" % (latex, den)
            else:
                return r"$\frac{%s%s}{%s}$" % (num, latex, den)

    return _multiple_formatter


class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex="\pi"):
        self.denominator = denominator
        self.number = number
        self.latex = latex

    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)

    def formatter(self):
        return plt.FuncFormatter(
            multiple_formatter(self.denominator, self.number, self.latex)
        )


def base_plot_func(
    data: np.ndarray[float],
    fillin_data: np.ndarray[float] | None = None,
    x_vals: np.ndarray[float] | None = None,
    title: str = "",
    xlabel: str = "",
    ylabel: str = "",
    save_format: str = "png",
    save_path: str | None = None,
    show_plot: bool = False,
    ls="-",
    marker="",
    sec_xaxis = False,
    fig_num = None,
    fig_label = ""
):

    if x_vals is None:
        x_vals = np.arange(data.shape[-1])

    formatter = EngFormatter(unit="", sep=" ")
    plt.figure(figsize=(10, 5), num=fig_num)
    plt.plot(x_vals, data, ls=ls, marker=marker, label=fig_label)
    if fillin_data is not None:
        plt.fill_between(x_vals, data - fillin_data, data + fillin_data, alpha=0.33)
    if x_vals.dtype not in ["datetime64", "datetime64[ms]"]:
        ax = plt.gca()
        ax.xaxis.set_major_formatter(formatter)
    if sec_xaxis:
        secax = ax.secondary_xaxis("top", functions=(utils.math_proc.delay2dist, utils.math_proc.dist2delay))
        secax.set_xlabel("Distance (m)")
    plt.grid("both")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    if fig_num is not None:
        plt.legend()
    plt.tight_layout()

    if save_path is not None:
        plt.savefig(f"{save_path}.{save_format}", dpi=600)

    if show_plot:
        plt.show()
    elif fig_num is not None:
        pass
    else:
        plt.close()


def plot_avg_freq(
    avg_data: np.ndarray[float],
    std_data: np.ndarray[float] | None = None,
    freqs: np.ndarray[float] | None = None,
    title: str = "",
    save_format: str = "png",
    save_path: str | None = None,
    show_plot: bool = False,
    fig_num=None,
    fig_label = ""
):
    if save_path is not None:
        std_string = ""
        if std_data is not None:
            std_string = "_w_std"
        save_path = save_path + f"freq_response{std_string}"

    base_plot_func(
        avg_data,
        std_data,
        freqs,
        title,
        xlabel="Frequency (Hz)",
        ylabel="Power (dBFS)",
        save_format=save_format,
        save_path=save_path,
        show_plot=show_plot,
        fig_num=fig_num,
        fig_label = fig_label
    )


def plot_avg_phase_speed(
    avg_data: np.ndarray[float],
    std_data: np.ndarray[float] | None = None,
    freqs: np.ndarray[float] | None = None,
    title: str = "",
    save_format: str = "png",
    save_path: str | None = None,
    show_plot: bool = False,
):
    if save_path is not None:
        std_string = ""
        if std_data is not None:
            std_string = "_w_std"
        save_path = save_path + f"phase_speed{std_string}"

    base_plot_func(
        avg_data,
        std_data,
        freqs,
        title,
        xlabel="Frequency (Hz)",
        ylabel="Phase variation (rad/subc)",
        save_format=save_format,
        save_path=save_path,
        show_plot=show_plot,
    )


def plot_avg_pdp(
    avg_data: np.ndarray[float],
    std_data: np.ndarray[float] | None = None,
    delays: np.ndarray[float] | None = None,
    title: str = "",
    save_format: str = "png",
    save_path: str | None = None,
    show_plot: bool = False,
    fig_num=None,
    fig_label = ""
):
    if save_path is not None:
        std_string = ""
        if std_data is not None:
            std_string = "_w_std"
        save_path = save_path + f"pdp{std_string}"

    base_plot_func(
        avg_data,
        std_data,
        x_vals=delays,
        title=title,
        xlabel="Delay (s)",
        ylabel="Power (dBFS)",
        save_format=save_format,
        save_path=save_path,
        show_plot=show_plot,
        sec_xaxis=True,
        fig_num=fig_num,
        fig_label = fig_label
    )


def plot_mse_v_time(
    mse_data: np.ndarray[float],
    dates: np.ndarray[np.datetime64],
    title: str = "",
    ylabel: str = "MSE",
    save_format: str = "png",
    save_path: str | None = None,
    show_plot: bool = False,
):

    if save_path is not None:
        save_path = save_path + "mse"

    base_plot_func(
        mse_data,
        fillin_data=None,
        x_vals=dates,
        title=title,
        xlabel="Time",
        ylabel=ylabel,
        save_format=save_format,
        save_path=save_path,
        show_plot=show_plot,
        ls="",
        marker=".",
    )


def is_outlier(points, thresh=3.5):
    """
    Returns a boolean array with True if points are outliers and False
    otherwise.

    Parameters:
    -----------
        points : An numobservations by numdimensions array of observations
        thresh : The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.

    Returns:
    --------
        mask : A numobservations-length boolean array.

    References:
    ----------
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """
    if len(points.shape) == 1:
        points = points[:, None]
    median = np.median(points, axis=0)
    diff = np.sum((points - median) ** 2, axis=-1)
    diff = np.sqrt(diff)
    med_abs_deviation = np.median(diff)

    modified_z_score = 0.6745 * diff / med_abs_deviation

    return modified_z_score > thresh


def plot_val_v_time_wo_outliers(
    mse_data: np.ndarray[float],
    dates: np.ndarray[np.datetime64],
    title: str = "",
    ylabel: str = "MSE",
    save_format: str = "png",
    save_path: str | None = None,
    show_plot: bool = False,
):
    outliers = is_outlier(mse_data, thresh=30)

    base_plot_func(
        mse_data[~outliers],
        fillin_data=None,
        x_vals=dates[~outliers],
        title=title,
        xlabel="Time",
        ylabel=ylabel,
        save_format=save_format,
        save_path=save_path,
        show_plot=show_plot,
        ls="",
        marker=".",
    )

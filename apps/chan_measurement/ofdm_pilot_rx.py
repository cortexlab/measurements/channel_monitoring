#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: OFDM Pilot RX
# Author: Cyrille Morin
# GNU Radio version: 3.10.9.2

from gnuradio import analog
from gnuradio import blocks
from gnuradio import channel_monitoring
from gnuradio import digital
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
import datetime
import numpy as np
import threading


def snipfcn_snippet_freq_change(self):
    def freq_sweep():
        local_time_ref = self.uhd_usrp_source_0.get_time_last_pps().get_real_secs()
        if local_time_ref < 1:
            self.my_log.info(f"Time at last PPS below 1, implying that PPS signal is not available. Setting time reference to CPU time")
            local_time_ref = self.uhd_usrp_sink_0.get_time_now().get_real_secs()
        self.my_log.info(f"time ref is {local_time_ref}, read as {datetime.datetime.fromtimestamp(local_time_ref).strftime('%Y-%m-%dT%H:%M:%S.%f')}.")

        # Trigger the re-emission of a rx_time tag after what was skipped by the Head block
        time.sleep(self.skip_time)
        tune_res = self.uhd_usrp_source_0.set_center_freq(self.freq, 0)
        self.my_log.info(f"Curr time is {time.ctime()}. Initial tuning of RX USRP to {tune_res}")
        cmd_time_float = local_time_ref
        changed_gain = False
        time.sleep(self.step_duration*0.8)
        for step_id, freq in enumerate(range(self.freq, self.f_max, self.f_step)):
            curr_tme = self.uhd_usrp_source_0.get_time_now().get_real_secs()
            cmd_time_float = local_time_ref + ((step_id+1) * self.step_duration) + self.start_time_offset
            self.uhd_usrp_source_0.set_command_time(uhd.time_spec(cmd_time_float))
            tune_res = self.uhd_usrp_source_0.set_center_freq(freq+self.f_step, 0)
            if not changed_gain and freq+self.f_step > self.gain_up_freq:
                self.my_log.info(f"Changing gain from {self.gain} to {self.gain + self.gain_up}")
                self.set_gain(self.gain + self.gain_up)
                changed_gain = True
            self.uhd_usrp_source_0.clear_command_time()
            self.my_log.info(f"Curr time is {datetime.datetime.fromtimestamp(curr_tme).strftime('%Y-%m-%dT%H:%M:%S.%f')}. Tuned request of TX USRP to {tune_res}Request set at time: {datetime.datetime.fromtimestamp(cmd_time_float).strftime('%Y-%m-%dT%H:%M:%S.%f')}")
            self.sigmf_record_chan_blk.set_freq_change(tune_res.actual_rf_freq-tune_res.actual_dsp_freq, cmd_time_float-(0.25*self.start_time_offset))
            time.sleep((cmd_time_float - curr_tme) + self.step_duration*0.8)
        curr_tme = self.uhd_usrp_source_0.get_time_now().get_real_secs()
        time.sleep((cmd_time_float - curr_tme) + self.step_duration)
        self.set_gain(0)
        self.sigmf_record_chan_blk.record_current_data()
        self.my_log.info(f"Curr time is {datetime.datetime.fromtimestamp(self.uhd_usrp_source_0.get_time_now().get_real_secs()).strftime('%Y-%m-%dT%H:%M:%S.%f')}. Reducing gain to 0")

    self.my_log = gr.logger(self.alias())
    freq_sweep_thread = threading.Thread(target=freq_sweep)
    freq_sweep_thread.daemon = True
    freq_sweep_thread.start()

def snipfcn_snippet_time_sync(self):
    if self.sync_on == 0:
        return

    import socket
    import struct
    self.log = gr.logger(self.alias())

    self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    self.socket.bind((self.ip_addr,self.port))
    self.log.info("Socket created")

    # Wait for incoming packets (query and start)
    pack_size = struct.calcsize("!BBdd")
    while 1: # Reception loop
        data, addr = self.socket.recvfrom(1024)
        self.log.info(f"Recieved addr variable: {addr}")
        pack_type, lead_id, lead_time_last_pps, lead_curr_time = struct.unpack("!BBdd",data)[:pack_size]
        curr_time = self.uhd_usrp_source_0.get_time_now().get_real_secs()
        time_last_pps = self.uhd_usrp_source_0.get_time_last_pps().get_real_secs()
        if pack_type == 3:
            self.log.info(f"Start packet recieved from addr {addr}, id {lead_id}. Starting!")
            break
        if pack_type == 1:
            self.log.info(f"Query packet recieved from addr {addr}, id {lead_id}. Will respond")
            poll_msg_bytes = struct.pack("!BBdd", 2, self.rx_id, time_last_pps, curr_time)#Type (2 for response), id, time_last_pps, curr_time
            self.socket.sendto(poll_msg_bytes, addr)
            local_clock_diff = lead_time_last_pps - time_last_pps
            pack_latency = (lead_curr_time - lead_time_last_pps) - (curr_time - time_last_pps)
            self.log.info(f"Responded to a Query packet from Lead. Measured a PPS time diff of {local_clock_diff}s and a packet delivery time of {pack_latency}")
        if pack_type not in [1,3]:
            self.log.warn(f"Wrong packet recieved from addr {addr}, id {lead_id}. Will NOT respond")


    self.socket.close()
    self.log.info("Sync process finished")


def snippets_main_after_init(tb):
    snipfcn_snippet_time_sync(tb)

def snippets_main_after_start(tb):
    snipfcn_snippet_freq_change(tb)


class ofdm_pilot_rx(gr.top_block):

    def __init__(self, carrier_num=64, f_max=2450000000, f_step=25000000, fft_len=128, freq=2450000000, gain=50, gain_up=6, gain_up_freq=2e9, port=3580, rx_id=4, samp_rate=5000000, seed=42, skip_time=0.5, start_time_offset=0.1, step_duration=60, sync_on=1, use_zc=0):
        gr.top_block.__init__(self, "OFDM Pilot RX", catch_exceptions=True)

        ##################################################
        # Parameters
        ##################################################
        self.carrier_num = carrier_num
        self.f_max = f_max
        self.f_step = f_step
        self.fft_len = fft_len
        self.freq = freq
        self.gain = gain
        self.gain_up = gain_up
        self.gain_up_freq = gain_up_freq
        self.port = port
        self.rx_id = rx_id
        self.samp_rate = samp_rate
        self.seed = seed
        self.skip_time = skip_time
        self.start_time_offset = start_time_offset
        self.step_duration = step_duration
        self.sync_on = sync_on
        self.use_zc = use_zc

        ##################################################
        # Variables
        ##################################################
        self.occupied_carriers = occupied_carriers = (list(range(-carrier_num//2, carrier_num//2)),)
        self.N = N = len(occupied_carriers[0])
        self.symbols_amount = symbols_amount = 12
        self.pilot_carriers = pilot_carriers = ((),)
        self.M = M = N-1
        self.pilot_symbols = pilot_symbols = ((),)
        self.pilot_seq_zf = pilot_seq_zf = [np.exp(np.pi*1j*x*(x+1)*(M/N)) for x in range(N)]*symbols_amount
        self.pilot_seq_rand = pilot_seq_rand = np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(occupied_carriers[0]*symbols_amount))) if np.random.seed(seed) is None else 0
        self.packet_length_tag_key = packet_length_tag_key = "packet_len"
        self.length_tag_key = length_tag_key = "frame_len"
        self.header_mod = header_mod = digital.constellation_bpsk()
        self.frame_len = frame_len = symbols_amount * len(occupied_carriers[0])
        self.bpsk_sqrt = bpsk_sqrt = {0: np.sqrt(2), 1: -np.sqrt(2)}
        self.bpsk = bpsk = {0: 1, 1: -1}
        self.active_carriers = active_carriers = list(np.array(list(occupied_carriers[0]) + list(pilot_carriers[0])) % fft_len)
        self.sync_word2 = sync_word2 = np.fft.fftshift([0j] + [bpsk[np.random.randint(2)] if x in active_carriers else 0 for x in range(1, fft_len)]) if np.random.seed(42) is None else 0
        self.sync_word1 = sync_word1 = np.fft.fftshift([bpsk_sqrt[np.random.randint(2)] if x in active_carriers and x % 2 else 0 for x in range(fft_len)]) if np.random.seed(42) is None else 0
        self.sym_scramble_seq = sym_scramble_seq = (1, 1, 1, 1, -1, -1, -1, 1, -1, -1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, -1, 1, 1, -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 1, 1, -1, -1, 1, 1, 1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, 1, -1, -1, 1, 1, 1, 1, 1, -1, -1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1,-1, -1, -1, -1, -1, 1, -1, 1, 1, -1, 1, -1, 1, 1, 1, -1, -1, 1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1)
        self.qpsk = qpsk = {0: 1+1j, 1: -1+1j, 2:1-1j, 3:-1-1j}
        self.pilot_seq = pilot_seq = pilot_seq_zf if use_zc else pilot_seq_rand
        self.ip_addr = ip_addr = "0.0.0.0"
        self.header_formatter_id = header_formatter_id = channel_monitoring.packet_header_ofdm_id( occupied_carriers, n_syms=1, len_tag_key=packet_length_tag_key, frame_len_tag_key=length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23, max_allowed_id=40, expected_len=frame_len )
        self.header_equalizer = header_equalizer = digital.ofdm_equalizer_simpledfe(fft_len,header_mod.base(),occupied_carriers,pilot_carriers,pilot_symbols)
        self.cp_len = cp_len = fft_len//8

        ##################################################
        # Blocks
        ##################################################

        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        _last_pps_time = self.uhd_usrp_source_0.get_time_last_pps().get_real_secs()
        # Poll get_time_last_pps() every 50 ms until a change is seen
        while(self.uhd_usrp_source_0.get_time_last_pps().get_real_secs() == _last_pps_time):
            time.sleep(0.05)
        # Set the time to PC time on next PPS
        self.uhd_usrp_source_0.set_time_next_pps(uhd.time_spec(int(time.time()) + 1.0))
        # Sleep 1 second to ensure next PPS has come
        time.sleep(1)

        self.uhd_usrp_source_0.set_center_freq(freq, 0)
        self.uhd_usrp_source_0.set_antenna("RX2", 0)
        self.uhd_usrp_source_0.set_bandwidth(160e6, 0)
        self.uhd_usrp_source_0.set_gain(gain, 0)
        self.sigmf_record_chan_blk = channel_monitoring.sigmf_record_chan(f"./channel_{rx_id:02d}", samp_rate, fft_len, freq, occupied_carriers[0], 'rx_time', 'freq_frac_offset', 'ofdm_sync_carr_offset', rx_id, 2)
        self.fft_vxx_1 = fft.fft_vcc(fft_len, True, (), True, 1)
        self.fft_vxx_1.set_min_output_buffer((2*(symbols_amount)))
        self.fft_vxx_0_0 = fft.fft_vcc(fft_len, True, (), True, 1)
        self.digital_packet_headerparser_b_0 = digital.packet_headerparser_b(header_formatter_id)
        self.digital_ofdm_sync_sc_cfb_0 = digital.ofdm_sync_sc_cfb(fft_len, cp_len, False, 0.985)
        self.digital_ofdm_serializer_vcc_payload = digital.ofdm_serializer_vcc(fft_len, occupied_carriers, length_tag_key, packet_length_tag_key, 0, '', True)
        self.digital_ofdm_serializer_vcc_payload.set_min_output_buffer((2*frame_len))
        self.digital_ofdm_serializer_vcc_header = digital.ofdm_serializer_vcc(fft_len, occupied_carriers, "header_len", '', 0, '', True)
        self.digital_ofdm_frame_equalizer_vcvc_0 = digital.ofdm_frame_equalizer_vcvc(header_equalizer.base(), cp_len, "header_len", True, 1)
        self.digital_ofdm_chanest_vcvc_0 = digital.ofdm_chanest_vcvc(sync_word1, sync_word2, 1, 0, 1, False)
        self.digital_header_payload_demux_0 = digital.header_payload_demux(
            3,
            fft_len,
            cp_len,
            length_tag_key,
            "",
            True,
            gr.sizeof_gr_complex,
            "rx_time",
            int(samp_rate),
            [],
            0)
        self.digital_header_payload_demux_0.set_min_output_buffer((2*(symbols_amount)))
        self.digital_constellation_decoder_cb_0 = digital.constellation_decoder_cb(header_mod.base())
        self.channel_monitoring_freq_offset_tagger_0 = channel_monitoring.freq_offset_tagger((samp_rate/fft_len) / np.pi, 'freq_frac_offset')
        self.blocks_vector_source_x_0_0 = blocks.vector_source_c(pilot_seq, True, 1, [])
        self.blocks_skiphead_0 = blocks.skiphead(gr.sizeof_gr_complex*1, (int(samp_rate*skip_time)))
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, f"./channel_{rx_id:02d}.sigmf-data", False)
        self.blocks_file_sink_0.set_unbuffered(False)
        self.blocks_divide_xx_0 = blocks.divide_cc(1)
        self.blocks_delay_0 = blocks.delay(gr.sizeof_gr_complex*1, (fft_len+cp_len))
        self.analog_frequency_modulator_fc_0 = analog.frequency_modulator_fc((-2.0/fft_len))


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_packet_headerparser_b_0, 'header_data'), (self.digital_header_payload_demux_0, 'header_data'))
        self.connect((self.analog_frequency_modulator_fc_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_delay_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_divide_xx_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.blocks_divide_xx_0, 0), (self.sigmf_record_chan_blk, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.digital_header_payload_demux_0, 0))
        self.connect((self.blocks_skiphead_0, 0), (self.blocks_delay_0, 0))
        self.connect((self.blocks_skiphead_0, 0), (self.digital_ofdm_sync_sc_cfb_0, 0))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.blocks_divide_xx_0, 1))
        self.connect((self.channel_monitoring_freq_offset_tagger_0, 0), (self.analog_frequency_modulator_fc_0, 0))
        self.connect((self.digital_constellation_decoder_cb_0, 0), (self.digital_packet_headerparser_b_0, 0))
        self.connect((self.digital_header_payload_demux_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.digital_header_payload_demux_0, 1), (self.fft_vxx_1, 0))
        self.connect((self.digital_ofdm_chanest_vcvc_0, 0), (self.digital_ofdm_frame_equalizer_vcvc_0, 0))
        self.connect((self.digital_ofdm_frame_equalizer_vcvc_0, 0), (self.digital_ofdm_serializer_vcc_header, 0))
        self.connect((self.digital_ofdm_serializer_vcc_header, 0), (self.digital_constellation_decoder_cb_0, 0))
        self.connect((self.digital_ofdm_serializer_vcc_payload, 0), (self.blocks_divide_xx_0, 0))
        self.connect((self.digital_ofdm_sync_sc_cfb_0, 1), (self.channel_monitoring_freq_offset_tagger_0, 1))
        self.connect((self.digital_ofdm_sync_sc_cfb_0, 0), (self.channel_monitoring_freq_offset_tagger_0, 0))
        self.connect((self.digital_ofdm_sync_sc_cfb_0, 1), (self.digital_header_payload_demux_0, 1))
        self.connect((self.fft_vxx_0_0, 0), (self.digital_ofdm_chanest_vcvc_0, 0))
        self.connect((self.fft_vxx_1, 0), (self.digital_ofdm_serializer_vcc_payload, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_skiphead_0, 0))


    def get_carrier_num(self):
        return self.carrier_num

    def set_carrier_num(self, carrier_num):
        self.carrier_num = carrier_num
        self.set_occupied_carriers((list(range(-self.carrier_num//2, self.carrier_num//2)),))

    def get_f_max(self):
        return self.f_max

    def set_f_max(self, f_max):
        self.f_max = f_max

    def get_f_step(self):
        return self.f_step

    def set_f_step(self, f_step):
        self.f_step = f_step

    def get_fft_len(self):
        return self.fft_len

    def set_fft_len(self, fft_len):
        self.fft_len = fft_len
        self.set_active_carriers(list(np.array(list(self.occupied_carriers[0]) + list(self.pilot_carriers[0])) % self.fft_len))
        self.set_cp_len(self.fft_len//8)
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))
        self.set_sync_word1(np.fft.fftshift([self.bpsk_sqrt[np.random.randint(2)] if x in self.active_carriers and x % 2 else 0 for x in range(self.fft_len)]) if np.random.seed(42) is None else 0)
        self.set_sync_word2(np.fft.fftshift([0j] + [self.bpsk[np.random.randint(2)] if x in self.active_carriers else 0 for x in range(1, self.fft_len)]) if np.random.seed(42) is None else 0)
        self.analog_frequency_modulator_fc_0.set_sensitivity((-2.0/self.fft_len))
        self.blocks_delay_0.set_dly(int((self.fft_len+self.cp_len)))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_source_0.set_center_freq(self.freq, 0)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_source_0.set_gain(self.gain, 0)

    def get_gain_up(self):
        return self.gain_up

    def set_gain_up(self, gain_up):
        self.gain_up = gain_up

    def get_gain_up_freq(self):
        return self.gain_up_freq

    def set_gain_up_freq(self, gain_up_freq):
        self.gain_up_freq = gain_up_freq

    def get_port(self):
        return self.port

    def set_port(self, port):
        self.port = port

    def get_rx_id(self):
        return self.rx_id

    def set_rx_id(self, rx_id):
        self.rx_id = rx_id

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_seed(self):
        return self.seed

    def set_seed(self, seed):
        self.seed = seed
        self.set_pilot_seq_rand(np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(self.occupied_carriers[0]*self.symbols_amount))) if np.random.seed(self.seed) is None else 0)

    def get_skip_time(self):
        return self.skip_time

    def set_skip_time(self, skip_time):
        self.skip_time = skip_time

    def get_start_time_offset(self):
        return self.start_time_offset

    def set_start_time_offset(self, start_time_offset):
        self.start_time_offset = start_time_offset

    def get_step_duration(self):
        return self.step_duration

    def set_step_duration(self, step_duration):
        self.step_duration = step_duration

    def get_sync_on(self):
        return self.sync_on

    def set_sync_on(self, sync_on):
        self.sync_on = sync_on

    def get_use_zc(self):
        return self.use_zc

    def set_use_zc(self, use_zc):
        self.use_zc = use_zc
        self.set_pilot_seq(self.pilot_seq_zf if self.use_zc else self.pilot_seq_rand)

    def get_occupied_carriers(self):
        return self.occupied_carriers

    def set_occupied_carriers(self, occupied_carriers):
        self.occupied_carriers = occupied_carriers
        self.set_N(len(self.occupied_carriers[0]))
        self.set_active_carriers(list(np.array(list(self.occupied_carriers[0]) + list(self.pilot_carriers[0])) % self.fft_len))
        self.set_frame_len(self.symbols_amount * len(self.occupied_carriers[0]))
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23, max_allowed_id=40, expected_len=self.frame_len ))
        self.set_pilot_seq_rand(np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(self.occupied_carriers[0]*self.symbols_amount))) if np.random.seed(self.seed) is None else 0)

    def get_N(self):
        return self.N

    def set_N(self, N):
        self.N = N
        self.set_M(self.N-1)
        self.set_pilot_seq_zf([np.exp(np.pi*1j*x*(x+1)*(self.M/self.N)) for x in range(self.N)]*self.symbols_amount)

    def get_symbols_amount(self):
        return self.symbols_amount

    def set_symbols_amount(self, symbols_amount):
        self.symbols_amount = symbols_amount
        self.set_frame_len(self.symbols_amount * len(self.occupied_carriers[0]))
        self.set_pilot_seq_rand(np.exp(1j*np.random.uniform(-np.pi, np.pi, size=len(self.occupied_carriers[0]*self.symbols_amount))) if np.random.seed(self.seed) is None else 0)
        self.set_pilot_seq_zf([np.exp(np.pi*1j*x*(x+1)*(self.M/self.N)) for x in range(self.N)]*self.symbols_amount)

    def get_pilot_carriers(self):
        return self.pilot_carriers

    def set_pilot_carriers(self, pilot_carriers):
        self.pilot_carriers = pilot_carriers
        self.set_active_carriers(list(np.array(list(self.occupied_carriers[0]) + list(self.pilot_carriers[0])) % self.fft_len))
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))

    def get_M(self):
        return self.M

    def set_M(self, M):
        self.M = M
        self.set_pilot_seq_zf([np.exp(np.pi*1j*x*(x+1)*(self.M/self.N)) for x in range(self.N)]*self.symbols_amount)

    def get_pilot_symbols(self):
        return self.pilot_symbols

    def set_pilot_symbols(self, pilot_symbols):
        self.pilot_symbols = pilot_symbols
        self.set_header_equalizer(digital.ofdm_equalizer_simpledfe(self.fft_len,header_mod.base(),self.occupied_carriers,self.pilot_carriers,self.pilot_symbols))

    def get_pilot_seq_zf(self):
        return self.pilot_seq_zf

    def set_pilot_seq_zf(self, pilot_seq_zf):
        self.pilot_seq_zf = pilot_seq_zf
        self.set_pilot_seq(self.pilot_seq_zf if self.use_zc else self.pilot_seq_rand)

    def get_pilot_seq_rand(self):
        return self.pilot_seq_rand

    def set_pilot_seq_rand(self, pilot_seq_rand):
        self.pilot_seq_rand = pilot_seq_rand
        self.set_pilot_seq(self.pilot_seq_zf if self.use_zc else self.pilot_seq_rand)

    def get_packet_length_tag_key(self):
        return self.packet_length_tag_key

    def set_packet_length_tag_key(self, packet_length_tag_key):
        self.packet_length_tag_key = packet_length_tag_key
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23, max_allowed_id=40, expected_len=self.frame_len ))

    def get_length_tag_key(self):
        return self.length_tag_key

    def set_length_tag_key(self, length_tag_key):
        self.length_tag_key = length_tag_key
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23, max_allowed_id=40, expected_len=self.frame_len ))

    def get_header_mod(self):
        return self.header_mod

    def set_header_mod(self, header_mod):
        self.header_mod = header_mod

    def get_frame_len(self):
        return self.frame_len

    def set_frame_len(self, frame_len):
        self.frame_len = frame_len
        self.set_header_formatter_id(channel_monitoring.packet_header_ofdm_id( self.occupied_carriers, n_syms=1, len_tag_key=self.packet_length_tag_key, frame_len_tag_key=self.length_tag_key,bits_per_header_sym=1,bits_per_payload_sym=8,scramble_header=True, tx_id=23, max_allowed_id=40, expected_len=self.frame_len ))

    def get_bpsk_sqrt(self):
        return self.bpsk_sqrt

    def set_bpsk_sqrt(self, bpsk_sqrt):
        self.bpsk_sqrt = bpsk_sqrt
        self.set_sync_word1(np.fft.fftshift([self.bpsk_sqrt[np.random.randint(2)] if x in self.active_carriers and x % 2 else 0 for x in range(self.fft_len)]) if np.random.seed(42) is None else 0)

    def get_bpsk(self):
        return self.bpsk

    def set_bpsk(self, bpsk):
        self.bpsk = bpsk
        self.set_sync_word2(np.fft.fftshift([0j] + [self.bpsk[np.random.randint(2)] if x in self.active_carriers else 0 for x in range(1, self.fft_len)]) if np.random.seed(42) is None else 0)

    def get_active_carriers(self):
        return self.active_carriers

    def set_active_carriers(self, active_carriers):
        self.active_carriers = active_carriers
        self.set_sync_word1(np.fft.fftshift([self.bpsk_sqrt[np.random.randint(2)] if x in self.active_carriers and x % 2 else 0 for x in range(self.fft_len)]) if np.random.seed(42) is None else 0)
        self.set_sync_word2(np.fft.fftshift([0j] + [self.bpsk[np.random.randint(2)] if x in self.active_carriers else 0 for x in range(1, self.fft_len)]) if np.random.seed(42) is None else 0)

    def get_sync_word2(self):
        return self.sync_word2

    def set_sync_word2(self, sync_word2):
        self.sync_word2 = sync_word2

    def get_sync_word1(self):
        return self.sync_word1

    def set_sync_word1(self, sync_word1):
        self.sync_word1 = sync_word1

    def get_sym_scramble_seq(self):
        return self.sym_scramble_seq

    def set_sym_scramble_seq(self, sym_scramble_seq):
        self.sym_scramble_seq = sym_scramble_seq

    def get_qpsk(self):
        return self.qpsk

    def set_qpsk(self, qpsk):
        self.qpsk = qpsk

    def get_pilot_seq(self):
        return self.pilot_seq

    def set_pilot_seq(self, pilot_seq):
        self.pilot_seq = pilot_seq
        self.blocks_vector_source_x_0_0.set_data(self.pilot_seq, [])

    def get_ip_addr(self):
        return self.ip_addr

    def set_ip_addr(self, ip_addr):
        self.ip_addr = ip_addr

    def get_header_formatter_id(self):
        return self.header_formatter_id

    def set_header_formatter_id(self, header_formatter_id):
        self.header_formatter_id = header_formatter_id

    def get_header_equalizer(self):
        return self.header_equalizer

    def set_header_equalizer(self, header_equalizer):
        self.header_equalizer = header_equalizer

    def get_cp_len(self):
        return self.cp_len

    def set_cp_len(self, cp_len):
        self.cp_len = cp_len
        self.blocks_delay_0.set_dly(int((self.fft_len+self.cp_len)))



def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-O", "--carrier-num", dest="carrier_num", type=intx, default=64,
        help="Set Occupied Carrier Number [default=%(default)r]")
    parser.add_argument(
        "--f-max", dest="f_max", type=intx, default=2450000000,
        help="Set Max frequency [default=%(default)r]")
    parser.add_argument(
        "--f-step", dest="f_step", type=intx, default=25000000,
        help="Set Frequency Step [default=%(default)r]")
    parser.add_argument(
        "-l", "--fft-len", dest="fft_len", type=intx, default=128,
        help="Set FFT Length [default=%(default)r]")
    parser.add_argument(
        "-f", "--freq", dest="freq", type=intx, default=2450000000,
        help="Set Center frequency [default=%(default)r]")
    parser.add_argument(
        "-g", "--gain", dest="gain", type=eng_float, default=eng_notation.num_to_str(float(50)),
        help="Set RX Gain [default=%(default)r]")
    parser.add_argument(
        "--gain-up", dest="gain_up", type=eng_float, default=eng_notation.num_to_str(float(6)),
        help="Set Gain Increase [default=%(default)r]")
    parser.add_argument(
        "--gain-up-freq", dest="gain_up_freq", type=eng_float, default=eng_notation.num_to_str(float(2e9)),
        help="Set Gain Increase Frequency [default=%(default)r]")
    parser.add_argument(
        "-P", "--port", dest="port", type=intx, default=3580,
        help="Set port_number [default=%(default)r]")
    parser.add_argument(
        "-i", "--rx-id", dest="rx_id", type=intx, default=4,
        help="Set RX ID [default=%(default)r]")
    parser.add_argument(
        "-r", "--samp-rate", dest="samp_rate", type=intx, default=5000000,
        help="Set Sample rate [default=%(default)r]")
    parser.add_argument(
        "--seed", dest="seed", type=intx, default=42,
        help="Set Pilot Sequence Seed [default=%(default)r]")
    parser.add_argument(
        "-s", "--skip-time", dest="skip_time", type=eng_float, default=eng_notation.num_to_str(float(0.5)),
        help="Set skip stime [default=%(default)r]")
    parser.add_argument(
        "-o", "--start-time-offset", dest="start_time_offset", type=eng_float, default=eng_notation.num_to_str(float(0.1)),
        help="Set Start Time Offset [default=%(default)r]")
    parser.add_argument(
        "-d", "--step-duration", dest="step_duration", type=eng_float, default=eng_notation.num_to_str(float(60)),
        help="Set Frequency Step [default=%(default)r]")
    parser.add_argument(
        "--sync-on", dest="sync_on", type=intx, default=1,
        help="Set Synchronise startup [default=%(default)r]")
    parser.add_argument(
        "-u", "--use-zc", dest="use_zc", type=intx, default=0,
        help="Set Use ZC sequence [default=%(default)r]")
    return parser


def main(top_block_cls=ofdm_pilot_rx, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(carrier_num=options.carrier_num, f_max=options.f_max, f_step=options.f_step, fft_len=options.fft_len, freq=options.freq, gain=options.gain, gain_up=options.gain_up, gain_up_freq=options.gain_up_freq, port=options.port, rx_id=options.rx_id, samp_rate=options.samp_rate, seed=options.seed, skip_time=options.skip_time, start_time_offset=options.start_time_offset, step_duration=options.step_duration, sync_on=options.sync_on, use_zc=options.use_zc)
    snippets_main_after_init(tb)
    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    snippets_main_after_start(tb)
    tb.wait()


if __name__ == '__main__':
    main()

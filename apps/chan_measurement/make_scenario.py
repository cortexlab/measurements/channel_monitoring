import os
import time
from argparse import ArgumentParser

import subprocess

NUM_NODES = 40

# Lookup table to find rx gain values depending on transmitter
# In form of a dict of dict, with tx_id first, then rx_id.
# Needs to be filled in over time when playing with different transmitters
RX_POWER_LUT = {
    36: {
        3: 25,
        4: 25,
        5: 25,
        6: 25,
        7: 25,
        8: 25,
        9: 22,
        10: 20,
        11: 20,
        12: 20,
        13: 20,
        14: 20,
        15: 20,
        16: 20,
        17: 20,
        18: 20,
        23: 25,
        24: 25,
        25: 25,
        26: 25,
        27: 25,
        28: 20,
        29: 20,
        30: 20,
        31: 20,
        32: 20,
        33: 15,
        34: 15,
        35: 15,
        37: 15,
        38: 15,
    }
}


def argument_parser():
    """
    Creates an argument parser and returns it
    """
    parser = ArgumentParser()
    parser.add_argument(
        "--folder-location",
        dest="folder_location",
        type=str,
        default=".",
        help="Location of task folder to create [default=%(default)r]",
    )
    parser.add_argument(
        "--docker-image",
        dest="docker_image",
        type=str,
        default="registry.gitlab.inria.fr/cortexlab/measurements/channel_monitoring/base_img:latest",
        help="Location of docker image to run from [default=%(default)r]",
    )
    parser.add_argument(
        "--tx-command",
        dest="tx_command",
        type=str,
        default="/cortexlab/homes/cmorin/utils/channel_monitoring/ofdm_pilot_tx.py",
        help="Location of the python file to run on transmitter [default=%(default)r]",
    )
    parser.add_argument(
        "--rx-command",
        dest="rx_command",
        type=str,
        default="/cortexlab/homes/cmorin/utils/channel_monitoring/ofdm_pilot_rx.py",
        help="Location of the python file to run on reciever [default=%(default)r]",
    )
    parser.add_argument(
        "-s",
        dest="submit",
        action="store_true",
        help="Submit task [default=%(default)r]",
    )
    parser.add_argument(
        "-w",
        dest="waitup",
        action="store_true",
        help="Wait for submitted task to finish and gather results into new dir with start date. Implies -s [default=%(default)r]",
    )
    parser.add_argument(
        "-z",
        dest="use_zc",
        action="store_true",
        help="Use Zadoff-Chu sequence as pilots (Good thing to use) [default=%(default)r]",
    )
    parser.add_argument(
        "-c",
        dest="cleanup",
        action="store_true",
        help="Cleanup created files and folders [default=%(default)r]",
    )
    parser.add_argument(
        "--fft",
        dest="fft_len",
        type=int,
        default=128,
        help="FFT length [default=%(default)r]",
    )
    parser.add_argument(
        "-O",
        dest="carrier_num",
        type=int,
        default=127,
        help="Number of occupied (pilot) subcarriers per symbol. When a Zadoff-Cho sequence is used, this should ideally be prime (and less than fft_len) [default=%(default)r]",
    )
    parser.add_argument(
        "-b",
        dest="bw",
        type=float,
        default=25e6,
        help="Sample rate to operate on (defines the measured Bandwidth) [default=%(default)r]",
    )
    parser.add_argument(
        "-d",
        dest="duration",
        type=float,
        default=2,
        help="Duration of a measurement at a single center frequency [default=%(default)r]",
    )
    parser.add_argument(
        "--fc",
        dest="center_freq",
        type=float,
        default=-1,
        help="Center frequency to measure at (or starting frequency for a sweep) [default=%(default)r]",
    )
    parser.add_argument(
        "--f-max",
        dest="max_freq",
        type=float,
        default=-1,
        help=(
            "Maximum frequency bound for sweep, excluded"
            " (Works as a max value for range() in Python) [default=%(default)r]"
        ),
    )
    parser.add_argument(
        "--f-step",
        dest="step_freq",
        type=float,
        default=-1,
        help="Step size of frequency sweep (Works as a max value for range() in Python) [default=%(default)r]",
    )
    parser.add_argument(
        "-r",
        "--rx_nodes",
        help="List of recieving nodes (ex: -g 1,32,28 )",
        type=lambda s: [int(item) for item in s.split(",")],
        default=[1000],
    )
    parser.add_argument(
        "-g",
        "--rx_nodes_gains",
        help="List of recieving nodes gains (ex: -g 1,32,28 )",
        type=lambda s: [int(item) for item in s.split(",")],
    )
    parser.add_argument(
        "--tx-id",
        dest="tx_id",
        type=int,
        default=36,
        help="Node to transmit from [default=%(default)r]",
    )
    parser.add_argument(
        "--tx-gain",
        dest="tx_gain",
        type=int,
        default=32,
        help="Transmitter gain [default=%(default)r]",
    )
    parser.add_argument(
        "-p",
        dest="tx_period",
        type=float,
        default=0.01,
        help="Period (s) at which to emit frames [default=%(default)r]",
    )
    group = parser.add_argument_group("Advanced arguments")
    group.add_argument(
        "--sweep-offset",
        dest="sweep_offset",
        type=float,
        default=0.1,
        help="Offset (s) between Tx and Rx changing frequency when sweeping [default=%(default)r]",
    )
    group.add_argument(
        "--skip-time",
        dest="skip_time",
        type=float,
        default=0.1,
        help="Time (s) to skip at Rx startup to avoid transient effects [default=%(default)r]",
    )
    group.add_argument(
        "--port",
        dest="port",
        type=int,
        default=3580,
        help="Port number for startup synchronisation [default=%(default)r]",
    )
    group.add_argument(
        "--gain-up-freq",
        dest="gain_up_freq",
        type=float,
        default=2e9,
        help="Frequency at which to increase Rx gain [default=%(default)r]",
    )
    group.add_argument(
        "--gain-up",
        dest="gain_up",
        type=float,
        default=10,
        help="How much to increase Rx gain at gain_up_freq [default=%(default)r]",
    )
    group.add_argument(
        "--record-margin",
        dest="record_margin",
        type=float,
        default=20,
        help="Time (s) margin to ensure proper recording before end of task [default=%(default)r]",
    )

    return parser


def main(options=None):
    if options is None:
        options = argument_parser().parse_args()


    task_folder = os.path.join(options.folder_location, f"node{options.tx_id}")
    os.makedirs(task_folder, exist_ok=True)

    if options.carrier_num > options.fft_len:
        print(f"Provided number of used carriers ({options.carrier_num}) above FFT length ({options.fft_len}). This is not feasable. Clipping to {options.fft_len}")
        options.carrier_num = options.fft_len

    if options.max_freq > 0 and options.step_freq > 0:
        # We are performing a sweep
        description_line = "Channel response sweep"
        full_duration = (
            len(
                range(
                    int(options.center_freq),
                    int(options.max_freq),
                    int(options.step_freq),
                )
            )
            + 1
        ) * options.duration + options.record_margin
    else:
        description_line = "Channel response measurement"
        full_duration = options.duration + options.record_margin
        options.max_freq = options.center_freq

    common_args = (
        f"-r {int(options.bw)} -l {int(options.fft_len)}"
        f" -d {options.duration} -f {int(options.center_freq)}"
        f" --f-max {int(options.max_freq)} --f-step {int(options.step_freq)}"
        f" -P {options.port} -u {int(options.use_zc)} -O {options.carrier_num}"
    )
    rx_args = (
        f"-o {options.sweep_offset} -s {options.skip_time}"
        f" --gain-up-freq {options.gain_up_freq} --gain-up {options.gain_up}"
    )

    rx_node_string = ""
    for rx_node in options.rx_nodes:
        rx_node_string += f"{rx_node},"
    rx_node_string = rx_node_string[:-1]
    tx_args = f"-g {options.tx_gain} -t {options.tx_period} -n {rx_node_string}"

    with open(
        os.path.join(task_folder, "scenario.yaml"), "w", encoding="utf-8"
    ) as yaml_file:
        print(f"description: {description_line}", file=yaml_file)
        print(f"duration: {full_duration}", file=yaml_file)

        print("nodes:", file=yaml_file)

        for node_id in range(1, NUM_NODES):
            if node_id == options.tx_id:
                print(f"  node{node_id}:", file=yaml_file)
                print("    container:", file=yaml_file)
                print(f"    - image: {options.docker_image}", file=yaml_file)
                print(
                    (
                        f'      command: bash -lc "{options.tx_command}'
                        f' -i {node_id} {common_args} {tx_args}"'
                    ),
                    file=yaml_file,
                )
            elif node_id in options.rx_nodes:
                rx_index = options.rx_nodes.index(node_id)
                if options.rx_nodes_gains is None:
                    rx_gain = RX_POWER_LUT[options.tx_id][node_id]
                else:
                    rx_gain = options.rx_nodes_gains[rx_index]
                print(f"  node{node_id}:", file=yaml_file)
                print("    container:", file=yaml_file)
                print(f"    - image: {options.docker_image}", file=yaml_file)
                print(
                    (
                        f'      command: bash -lc "{options.rx_command}'
                        f' -i {node_id} -g {rx_gain} {common_args} {rx_args}"'
                    ),
                    file=yaml_file,
                )

    if options.submit or options.waitup:
        print("Task creation and submission")
        subprocess.run(["minus", "task", "create", "-f", f"{task_folder}"], check=True)
        ret_val = subprocess.run(
            ["minus", "task", "submit", f"{task_folder}.task"],
            check=True,
            capture_output=True,
        )
        num_task = int("".join(i for i in str(ret_val.stdout) if i.isdigit()))

    if options.waitup:
        date = time.strftime("%Y%m%d_%Hh%M")
        print(
            f"Waiting for tasks to finish. Expected duration: {full_duration//60}mn{full_duration%60}s, Current date: {time.strftime('%Y%m%d_%Hh%M')}",
            end="",
            flush=True,
        )
        while True:
            ret_out = subprocess.run(
                ["minus", "testbed", "status"],
                check=True,
                capture_output=True,
            ).stdout
            none_waiting = str(ret_out).find("waiting: 0") > 0
            none_running = str(ret_out).find("(none)") > 0
            if str(ret_out).find("<Task(id=") < 0:
                running_id = 0
            else:
                running_id = int(
                    str(ret_out)[
                        str(ret_out).find("<Task(id=")
                        + len("<Task(id=") : str(ret_out).find(", state=")
                    ]
                )

            if (none_waiting and none_running) or running_id > num_task:
                print("")
                break

            time.sleep(10)
            print("...Task still running...", end="", flush=True)
        print(f"Tasks finished at time: {time.strftime('%Y%m%d_%Hh%M')}")

        GATHERING_WAIT_TIME = 30
        print(f"Gathering result files in {GATHERING_WAIT_TIME}s")
        time.sleep(GATHERING_WAIT_TIME)

        os.chdir(os.path.expanduser("~/results"))
        print("Creating result folder")

        os.makedirs(f"{date}", exist_ok=False)

        for rx in options.rx_nodes:
            try:
                os.rename(
                    f"task_{num_task}/node{rx}/task_{num_task}_container_0/root/channel_{rx:02d}.sigmf-meta",
                    os.path.expanduser(f"~/results/{date}/channel_{rx:02d}.sigmf-meta"),
                )
                os.rename(
                    f"task_{num_task}/node{rx}/task_{num_task}_container_0/root/channel_{rx:02d}.sigmf-data",
                    os.path.expanduser(f"~/results/{date}/channel_{rx:02d}.sigmf-data"),
                )
            except FileNotFoundError as e:
                print(e)

    if options.cleanup:
        # Delete folders and tasks
        print(f"Deleting folder {task_folder} and/or {task_folder}.task")
        subprocess.run([f"rm -r {task_folder}*"], check=False, shell=True)


if __name__ == "__main__":
    main()

"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import pmt


class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self, occupied_carriers=[[1.0]], fft_len=64):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.sync_block.__init__(
            self,
            name='Chan est extractor',   # will show up in GRC
            in_sig=[np.complex64],
            out_sig=[np.complex64]
        )
        # if an attribute with the same name as a parameter is found,
        # a callback is registered (properties work, too).
        self.occupied_carriers = np.array(occupied_carriers[0]) + fft_len//2
        self.fft_len = fft_len
        print(occupied_carriers)
        print(self.occupied_carriers)
        self.occupied_taps = np.zeros(len(self.occupied_carriers), dtype=np.complex64)
        self.logger = gr.logger(self.alias())
        self.set_output_multiple(len(self.occupied_carriers))

    def work(self, input_items, output_items):
        """example: multiply with constant"""
        in0 = input_items[0]
        out0 = output_items[0]
        tags = self.get_tags_in_window(0, 0, len(out0))
        for tag in tags:

            if pmt.to_python(tag.key) == "ofdm_sync_chan_taps":
                if tag.offset > self.nitems_read(0):
                    # occupied_taps = in0[:tag.offset-self.nitems_read(0)]
                    break
                taps = pmt.to_python(tag.value)
                # self.logger.warn(f"Chan taps recieved: \n {taps}")
                self.occupied_taps = taps[self.occupied_carriers]
                # self.logger.warn(f"Recieved occupied taps: {occupied_taps}")


        output_items[0][:len(self.occupied_taps)] = self.occupied_taps

        return len(self.occupied_taps)

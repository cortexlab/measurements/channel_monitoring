# Channel Monitoring

This holds modules and flowgraphs to measure some propagation channel statistics with GNU Radio and Software Defined Radios (SDRs)

The main part of the file tree is a GNU Radio OOT (Out of Tree) module, with a few custom blocks and objects,
and the `apps` directory contains flowgraphs to do some measurements

A recent GNU Radio version is expected (> 3.10.x). It might unexpectedly work with 3.9.x, but it's not supported


## OOT module

### Cloning and build

The `Record Channel Meta (SigMF)` block requires the [sigmf python library](https://github.com/sigmf/sigmf-python), that can be installed with:
```
pip install sigmf
```

Standard GNU Radio building instruction applies:
```
git clone https://gitlab.inria.fr/cortexlab/measurements/channel_monitoring.git
cd channel_monitoring
mkdir build
cd build
cmake ..
make install
```
If GNU radio is installed in a custom location, or via radioconda for instance, follow the specific build instructions for that. (For radioconda, you can look [here](https://wiki.gnuradio.org/index.php?title=CondaInstall#Building_OOT_modules_to_use_with_conda-installed_GNU_Radio))

### Description of blocks
#### Freq Offset Tagger
This block is made to operate after a `Schmidl & Cox OFDM sync` block.
It uses both of its output stream. So it expects a float and a byte stream, operating at the same rate.

It continuously copies the contents of the `in` stream to its output, without modification.

When a frame is detected (a.k.a. when a 1 is present in the `trigger` stream), the corresponding sample from the `in` stream is read, and its value is added as a tag (of `Tag Key` key) to the output stream, on the same sample.
The value is first multiplied by `Norm Factor`. (That can be 1, if no modification is desired)
A good `Norm Factor` value is `(samp_rate/fft_len) / np.pi` to get back the frequency offset in Hz.

This allow the value of the sample of interest to be conserved downstream, even if some blocks change the value of the original sample.

#### Record Channel Meta

This block records a lot of information in a SigMF meta file. (The raw data is expected to be recorded separately, for instance with a `File Sink` block)

As global information, it records:
- Author info
- A human readable description
- The sample rate
- An index/number for the reciever used
- The OFDM FFT length
- The list of occupied subcarriers

And for each frame (defined as samples between tags of key `"tx_id"`):
- A reception time for first sample (Gathered from a tag of key `Time Tag Key`)
- Center frequency of reciever (Gathered from a tag of key `"rx_freq"`)
- The index of the frist corresponding sample in the data file
- The packet length (Gathered from a tag of key `"packet_len"`)
- The packet number (Gathered from a tag of key `"packet_num"`)
- The transmitter (Gathered from the tag of key `"tx_id"`)
- The frequency offset measured by a Schmidl&Cox block (Combined from a tag of key `Frac Offset Tag Key`, and another of key `Carrier Offset Tag Key`)
All these tags are expected to be on the exact same sample.
The transmitter id tag is optional. If not found, the `TX Note ID` parameter is used.

#### packet_header_ofdm_id

This object is an extension of the stock GNU Radio `packet_header_ofdm` header formatter.
It extends it by adding a transmitter ID field. And a parameter to reject transmitter ID above a maximum.


## Power loopback

The `apps/pow_loopback` contains a GRC flowgraph to record loopback power across a range of frequencies.
It contains an embedded python block operate the frequency sweep, and record measured power values in a csv file.

## Channel measurement

The `apps/chan_measurement` contains three GRC flowgraphs for measuring and recording propagation channel state.

#### ofdm_pilot_loopback
`ofdm_pilot_loopback.grc` is intended for debugging and prototyping purposes.
It contains both a TX and RX chain that can be either used with real SDR hardware, or with a synthetic channel.
See following for description of the chains

#### ofdm_pilot_tx
It is the TX side of the transmission chain, intended for production use, so without plotting and with real hardware (of course, one can modify it).

Every `Packet Wait Time`, a frame is sent, full of pilot symbols, with the specified OFDM parameters (FFT Length, coccupied carriers, Cyclic Prefix, ...), and the desired hardware parameters (Sample rate, Center Freq, gain,...)

A pseudo random sequence of frame length qpsk symbols is generated (known sequence) at the beginning, to serve as pilots symbols. And a `Message Strobe` block sends it at regular intervals. This frame is read to generate a packet header (with the `packet_header_ofdm_id` object), and the two are concatenated.
It is then sent through the standard OFDM blocks to place the symbols on subcarriers, add sync words, do the IFFT, and add the Cyclic prefix. before being sent to the hardware.

A length of zeros can be prepended to the frame to give a bit of time to the radio device to stabilise befor each start of frame.

#### ofdm_pilot_rx
It is the RX side of the transmission chain, intended for production use, so without plotting and with real hardware (of course, one can modify it).

The radio chain is set to listen to the desired hardware parameters (Sample rate, Center Freq, gain,...), and expected OFDM parameters (FFT Length, coccupied carriers, Cyclic Prefix, ...). 
The first few samples are skipped to ignore transient effects at power on.
A Schmidl&Cox block detects the presence of OFDM sync words, and estimates the incoming signal frequency offset, that can be then corrected.
The Header/Payload Demux block uses the start of frame detection to send the header to be decoded, and uses its output to forward the payload.

The payload is then extracted with FFT and serializing.
Since pilot sequence is generated in a known way, we can directly divide the recieved samples by it to get subcarrier channel measurements, and finally record those.

Note that this operation rests on the fact that all recieved frames have the same length.
A strong enough interference could cause frame of a wrong size to be recieved, and the pilot sequence would get out of sync with the recieved symbol sequence. Only a restart could solve that issue.

#### make_scenario
For operation inside of the [CorteXlab](www.cortexlab.fr) experiment room, this script can facilitate the creation of scenario files and running of experiments.
Feel free to look at all its cli options by running `python3 make_scenario.py -h`
As an overview of the possibilities, one can:
- Choose transmission parameters (FFT Length, sample rate, occupied carriers, transmission period)
- Frequency sweep parameter (Stat, stop, step, duration, ...)
- Select participating TX and Rx nodes (only one TX at a time), with their respective gains
- And even select the docker image, and the commands to run on the nodes

One can use it to just generate a scenario file that can be later manually deployed.
But the script can also directly use it to create a task from the scenario, submit it (`-s`), and even wait for the end of experiment to gather all channel recording files into one folder (`-w`).
